// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:0,bsrc:0,bdst:7,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:33844,y:32830,varname:node_1873,prsc:2|normal-8075-RGB,custl-4074-OUT;n:type:ShaderForge.SFN_LightVector,id:4801,x:31779,y:32520,varname:node_4801,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:2592,x:31779,y:32656,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:2345,x:31971,y:32576,varname:node_2345,prsc:2,dt:1|A-4801-OUT,B-2592-OUT;n:type:ShaderForge.SFN_Multiply,id:2528,x:32178,y:32576,varname:node_2528,prsc:2|A-2345-OUT,B-8922-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8922,x:31971,y:32742,varname:node_8922,prsc:2;n:type:ShaderForge.SFN_Vector1,id:1284,x:32178,y:32742,varname:node_1284,prsc:2,v1:0;n:type:ShaderForge.SFN_Append,id:9906,x:32367,y:32639,varname:node_9906,prsc:2|A-2528-OUT,B-1284-OUT;n:type:ShaderForge.SFN_Tex2d,id:5738,x:32552,y:32639,ptovrint:False,ptlb:Toon Ramp,ptin:_ToonRamp,varname:node_5738,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:93c0524696550094e8522d634df9564e,ntxv:0,isnm:False|UVIN-9906-OUT;n:type:ShaderForge.SFN_Slider,id:2472,x:32395,y:32525,ptovrint:False,ptlb:Toon Ramp Intensity,ptin:_ToonRampIntensity,varname:node_2472,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Blend,id:5842,x:32743,y:32639,varname:node_5842,prsc:2,blmd:1,clmp:True|SRC-2472-OUT,DST-5738-RGB;n:type:ShaderForge.SFN_Slider,id:2168,x:32365,y:33134,ptovrint:False,ptlb:Min Light,ptin:_MinLight,varname:node_2168,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Slider,id:4836,x:32365,y:32944,ptovrint:False,ptlb:Max Light,ptin:_MaxLight,varname:node_4836,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Subtract,id:9240,x:32739,y:32946,varname:node_9240,prsc:2|A-4836-OUT,B-2168-OUT;n:type:ShaderForge.SFN_Multiply,id:5382,x:32936,y:32946,varname:node_5382,prsc:2|A-5842-OUT,B-9240-OUT;n:type:ShaderForge.SFN_Add,id:6520,x:33117,y:33092,varname:node_6520,prsc:2|A-5382-OUT,B-2168-OUT;n:type:ShaderForge.SFN_Tex2d,id:1788,x:33117,y:32910,ptovrint:False,ptlb:Albedo,ptin:_Albedo,varname:node_1788,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2239,x:33333,y:33016,varname:node_2239,prsc:2|A-1788-RGB,B-6520-OUT;n:type:ShaderForge.SFN_Tex2d,id:8075,x:33333,y:32849,ptovrint:False,ptlb:Normal Map,ptin:_NormalMap,varname:node_8075,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Fresnel,id:2708,x:33117,y:33311,varname:node_2708,prsc:2|NRM-2933-OUT,EXP-8310-OUT;n:type:ShaderForge.SFN_NormalVector,id:2933,x:32928,y:33256,prsc:2,pt:True;n:type:ShaderForge.SFN_ValueProperty,id:8310,x:32928,y:33439,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_8310,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:4074,x:33594,y:33080,varname:node_4074,prsc:2|A-2239-OUT,B-2005-OUT;n:type:ShaderForge.SFN_OneMinus,id:2005,x:33330,y:33311,varname:node_2005,prsc:2|IN-2708-OUT;proporder:5738-2472-2168-4836-1788-8075-8310;pass:END;sub:END;*/

Shader "Shader Forge/Character" {
    Properties {
        _ToonRamp ("Toon Ramp", 2D) = "white" {}
        _ToonRampIntensity ("Toon Ramp Intensity", Range(0, 1)) = 1
        _MinLight ("Min Light", Range(0, 1)) = 0.2
        _MaxLight ("Max Light", Range(0, 1)) = 1
        _Albedo ("Albedo", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _Fresnel ("Fresnel", Float ) = 1
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "RenderType"="Opaque"
            "CanUseSpriteAtlas"="True"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcAlpha
            
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _ToonRamp; uniform float4 _ToonRamp_ST;
            uniform float _ToonRampIntensity;
            uniform float _MinLight;
            uniform float _MaxLight;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = 1;
                float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(i.uv0, _Albedo));
                float2 node_9906 = float2((max(0,dot(lightDirection,normalDirection))*attenuation),0.0);
                float4 _ToonRamp_var = tex2D(_ToonRamp,TRANSFORM_TEX(node_9906, _ToonRamp));
                float3 finalColor = ((_Albedo_var.rgb*((saturate((_ToonRampIntensity*_ToonRamp_var.rgb))*(_MaxLight-_MinLight))+_MinLight))*(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)));
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _ToonRamp; uniform float4 _ToonRamp_ST;
            uniform float _ToonRampIntensity;
            uniform float _MinLight;
            uniform float _MaxLight;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation,i, i.posWorld.xyz);
                float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(i.uv0, _Albedo));
                float2 node_9906 = float2((max(0,dot(lightDirection,normalDirection))*attenuation),0.0);
                float4 _ToonRamp_var = tex2D(_ToonRamp,TRANSFORM_TEX(node_9906, _ToonRamp));
                float3 finalColor = ((_Albedo_var.rgb*((saturate((_ToonRampIntensity*_ToonRamp_var.rgb))*(_MaxLight-_MinLight))+_MinLight))*(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)));
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

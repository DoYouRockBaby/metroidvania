﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Physic
{
    public class Controller2D : MonoBehaviour
    {
        public Vector2 velocity = Vector2.zero;
        public LayerMask collisionMask;
        public LayerMask platformMask;
        public float maxSlopeAngle = 40f;
        public bool facingRight = true;

        public struct RaycastOrigins
        {
            public Vector2 topLeft, topRight;
            public Vector2 bottomLeft, bottomRight;
        }

        public struct CollisionInfo
        {
            public bool above, below;
            public bool left, right;

            public bool nothingAbove, nothingBelow;
            public bool nothingLeft, nothingRight;

            public bool climbingSlope;
            public bool descendingSlope;
            public bool slidingDownMaxSlope;

            public float horizontalCollisionAngle;
            public float verticalCollisionAngle;

            public float slopeAngle, slopeAngleOld;
            public Vector2 slopeNormal;
            public Vector2 moveAmountOld;
            public int faceDir;
            public bool fallingThroughPlatform;

            public void Reset()
            {
                above = below = false;
                left = right = false;
                nothingAbove = nothingBelow = false;
                nothingLeft = nothingRight = false;
                climbingSlope = false;
                descendingSlope = false;
                slidingDownMaxSlope = false;
                slopeNormal = Vector2.zero;

                slopeAngleOld = slopeAngle;
                slopeAngle = 0;
            }
        }

        [HideInInspector]
        public int horizontalRayCount;
        [HideInInspector]
        public int verticalRayCount;
        [HideInInspector]
        public float horizontalRaySpacing;
        [HideInInspector]
        public float verticalRaySpacing;

        [HideInInspector]
        public RaycastOrigins raycastOrigins;
        [HideInInspector]
        public CollisionInfo collisions;

        public List<Collider2D> collisionColliders = new List<Collider2D>();

        private new BoxCollider2D collider;
        private Animator animator;

        private const float skinWidth = .015f;
        private const float dstBetweenRays = .25f;

        private void Awake()
        {
            collider = GetComponent<BoxCollider2D>();
            animator = GetComponent<Animator>();

            CalculateRaySpacing();
            collisions.faceDir = 1;
        }

        protected void Update()
        {
            var previousCollisionColliders = collisionColliders;
            collisionColliders = new List<Collider2D>();

            var movements = Move();
            velocity.y = movements.y / Time.deltaTime;

            NotifyCollisions(previousCollisionColliders);

            if(animator != null)
            {
                animator.SetBool("Ground", collisions.below);
                animator.SetFloat("Speed", Mathf.Abs(velocity.x));
                animator.SetFloat("vSpeed", velocity.y);
                animator.SetFloat("Direction", facingRight ? 1f : -1f);
            }
        }

        protected void UpdateRaycastOrigins()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);

            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        protected void CalculateRaySpacing()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);

            float boundsWidth = bounds.size.x;
            float boundsHeight = bounds.size.y;

            horizontalRayCount = Mathf.RoundToInt(boundsHeight / dstBetweenRays);
            verticalRayCount = Mathf.RoundToInt(boundsWidth / dstBetweenRays);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }

        //Return the real move done
        public Vector2 Move()
        {
            UpdateRaycastOrigins();

            var moveAmount = velocity * Time.deltaTime;

            collisions.Reset();
            collisions.moveAmountOld = moveAmount;

            if (moveAmount.y < 0)
            {
                DescendSlope(ref moveAmount);
            }

            if (moveAmount.x != 0)
            {
                collisions.faceDir = (int)Mathf.Sign(moveAmount.x);
            }

            HorizontalCollisions(ref moveAmount, Mathf.Sign(moveAmount.x));
            if (moveAmount.y != 0)
            {
                VerticalCollisions(ref moveAmount, Mathf.Sign(moveAmount.y));
            }

            transform.Translate(moveAmount, Space.World);

            return moveAmount;
        }

        void HorizontalCollisions(ref Vector2 moveAmount, float directionX, bool onlyForCollisionHandler = false)
        {
            float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
            collisions.horizontalCollisionAngle = 0.0f;

            if (Mathf.Abs(moveAmount.x) < skinWidth)
            {
                rayLength = 2 * skinWidth;
            }

            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX, onlyForCollisionHandler ? Color.yellow : Color.red);

                if (hit)
                {
                    if (hit.distance == 0)
                    {
                        continue;
                    }

                    collisionColliders.Add(hit.collider);

                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    collisions.horizontalCollisionAngle = 90.0f - slopeAngle;

                    if (onlyForCollisionHandler)
                    {
                        continue;
                    }

                    if (i == 0 && slopeAngle <= maxSlopeAngle)
                    {
                        if (collisions.descendingSlope)
                        {
                            collisions.descendingSlope = false;
                            moveAmount = collisions.moveAmountOld;
                        }
                        float distanceToSlopeStart = 0;
                        if (slopeAngle != collisions.slopeAngleOld)
                        {
                            distanceToSlopeStart = hit.distance - skinWidth;
                            moveAmount.x -= distanceToSlopeStart * directionX;
                        }
                        ClimbSlope(ref moveAmount, slopeAngle, hit.normal);
                        moveAmount.x += distanceToSlopeStart * directionX;
                    }

                    if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle)
                    {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        rayLength = hit.distance;

                        if (collisions.climbingSlope)
                        {
                            moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
                        }

                        collisions.left = directionX == -1;
                        collisions.right = directionX == 1;
                    }
                }
                else if (i == 0 && !onlyForCollisionHandler)
                {
                    collisions.nothingBelow = true;
                }
                else if (i == horizontalRayCount - 1 && !onlyForCollisionHandler)
                {
                    collisions.nothingAbove = true;
                }
            }
        }

        void VerticalCollisions(ref Vector2 moveAmount, float directionY, bool onlyForCollisionHandler = false)
        {
            float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;
            collisions.verticalCollisionAngle = 0.0f;

            for (int i = 0; i < verticalRayCount; i++)
            {

                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY, onlyForCollisionHandler ? Color.yellow : Color.red);

                if (hit)
                {
                    if (hit.collider.tag == "Through")
                    {
                        if (directionY == 1 || hit.distance == 0)
                        {
                            continue;
                        }

                        if (collisions.fallingThroughPlatform)
                        {
                            continue;
                        }

                        /*if (playerInput.y == -1)
                        {
                            collisions.fallingThroughPlatform = true;
                            Invoke("ResetFallingThroughPlatform", .5f);
                            continue;
                        }*/
                    }

                    collisionColliders.Add(hit.collider);

                    float angle = Vector2.Angle(hit.normal, Vector2.up);
                    collisions.verticalCollisionAngle = angle;

                    if (onlyForCollisionHandler)
                    {
                        continue;
                    }

                    moveAmount.y = (hit.distance - skinWidth) * directionY;
                    rayLength = hit.distance;

                    if (collisions.climbingSlope)
                    {
                        moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                    }

                    collisions.below = directionY == -1;
                    collisions.above = directionY == 1;
                }
                else if (i == 0 && !onlyForCollisionHandler)
                {
                    collisions.nothingLeft = true;
                }
                else if (i == verticalRayCount - 1 && !onlyForCollisionHandler)
                {
                    collisions.nothingRight = true;
                }
            }

            if (collisions.climbingSlope)
            {
                float directionX = Mathf.Sign(moveAmount.x);
                rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
                Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * moveAmount.y;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                if (hit)
                {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (slopeAngle != collisions.slopeAngle)
                    {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        collisions.slopeAngle = slopeAngle;
                        collisions.slopeNormal = hit.normal;
                    }
                }
            }
        }

        void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal)
        {
            float moveDistance = Mathf.Abs(moveAmount.x);
            float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

            if (moveAmount.y <= climbmoveAmountY)
            {
                moveAmount.y = climbmoveAmountY;
                moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                collisions.below = true;
                collisions.climbingSlope = true;
                collisions.slopeAngle = slopeAngle;
                collisions.slopeNormal = slopeNormal;
            }
        }

        void DescendSlope(ref Vector2 moveAmount)
        {

            RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(raycastOrigins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
            RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(raycastOrigins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
            if (maxSlopeHitLeft ^ maxSlopeHitRight)
            {
                SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
                SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);
            }

            if (!collisions.slidingDownMaxSlope)
            {
                float directionX = Mathf.Sign(moveAmount.x);
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

                if (hit)
                {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle)
                    {
                        if (Mathf.Sign(hit.normal.x) == directionX)
                        {
                            if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))
                            {
                                float moveDistance = Mathf.Abs(moveAmount.x);
                                float descendmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                                moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                                moveAmount.y -= descendmoveAmountY;

                                collisions.slopeAngle = slopeAngle;
                                collisions.descendingSlope = true;
                                collisions.below = true;
                                collisions.slopeNormal = hit.normal;
                            }
                        }
                    }
                }
            }
        }

        void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount)
        {

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle > maxSlopeAngle)
                {
                    moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

                    collisions.slopeAngle = slopeAngle;
                    collisions.slidingDownMaxSlope = true;
                    collisions.slopeNormal = hit.normal;
                }
            }
        }

        void ResetFallingThroughPlatform()
        {
            collisions.fallingThroughPlatform = false;
        }

        void NotifyCollisions(List<Collider2D> previousCollisionColliders)
        {
            collisionColliders = collisionColliders.Distinct().ToList();

            foreach (var collider in collisionColliders)
            {
                collider.gameObject.BroadcastMessage("OnColliderStay2D", collider, SendMessageOptions.DontRequireReceiver);

                if (!previousCollisionColliders.Contains(collider))
                {
                    collider.gameObject.BroadcastMessage("OnColliderEnter2D", collider, SendMessageOptions.DontRequireReceiver);
                }
            }

            foreach (var collider in previousCollisionColliders)
            {
                if (!collisionColliders.Contains(collider))
                {
                    collider.gameObject.BroadcastMessage("OnColliderExit2D", collider, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}

﻿using UnityEngine;

namespace Physic
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class Gravity : MonoBehaviour
    {
        public float gravity = -50f;
        public float maxGravitySpeeds = 20;

        private Physic.Controller2D controller;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
        }

        private void Update()
        {
            var newVelocityY = controller.velocity.y + Time.deltaTime * gravity;

            if (Mathf.Abs(newVelocityY) > maxGravitySpeeds)
            {
                newVelocityY = Mathf.Sign(newVelocityY) * maxGravitySpeeds;
            }

            controller.velocity = new Vector2(
                controller.velocity.x,
                newVelocityY
            );
        }
    }
}
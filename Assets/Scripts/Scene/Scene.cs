﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Scene
{
    [RequireComponent(typeof(Collider2D))]
    public class Scene : MonoBehaviour
    {
        public string[] scenes = new string[0];

        static private string[] loadedScenes = new string[0];

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag == "Player")
            {

                //Load desired scenes
                foreach (var scene in scenes)
                {
                    if (!loadedScenes.Contains(scene))
                    {
                        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
                        asyncOperation.allowSceneActivation = false;

                        StartCoroutine(ActivateSceneWhenLoaded(asyncOperation));
                    }
                }

                //Unload undesired scenes
                foreach (var scene in loadedScenes)
                {
                    if (!scenes.Contains(scene))
                    {
                        SceneManager.UnloadSceneAsync(scene);
                    }
                }

                //Update loaded scene list
                loadedScenes = scenes.ToArray();
            }
        }

        IEnumerator ActivateSceneWhenLoaded(AsyncOperation asyncOperation)
        {
            while (asyncOperation.progress < 0.9f)
            {
                yield return new WaitForEndOfFrame();
            }

            asyncOperation.allowSceneActivation = true;
        }
    }
}

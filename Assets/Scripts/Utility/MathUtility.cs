﻿using System;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    public static class MathUtility
    {
        [Serializable]
        public enum Interpolation
        {
            Linear,
            BezierSimple,
            BezierFree
        }

        [Serializable]
        public class InterpolationPoint
        {
            public Vector3 position;
            public Interpolation interpolation = Interpolation.Linear;
            public Vector3 tangent1;
            public Vector3 tangent2;

            public InterpolationPoint Transform(Matrix4x4 matrix)
            {
                return new InterpolationPoint()
                {
                    position = matrix.MultiplyPoint(position),
                    interpolation = interpolation,
                    tangent1 = matrix.MultiplyPoint(tangent1),
                    tangent2 = matrix.MultiplyPoint(tangent2)
                };
            }
        }

        public static Vector3 CubeBezier3(Vector3 start, Vector3 end, Vector3 startTangent, Vector3 endTangent, float t)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 p = uuu * start;
            p += 3 * uu * t * startTangent;
            p += 3 * u * tt * endTangent;
            p += ttt * end;

            return p;
        }

        public static Vector3 CubeBezier3Tangent(Vector3 start, Vector3 end, Vector3 startTangent, Vector3 endTangent, float t)
        {
            float u = -t;
            float tt = 2 * t;
            float uu = 2 - 2 * t;
            float uuu = 3 + 3 * t * t - 12 * t;
            float ttt = tt * 3;

            Vector3 p = uuu * start;
            p += 3 * (uu * t + u * u) * startTangent;
            p += 3 * ((1 - t) * tt + u * t * t) * endTangent;
            p += ttt * end;

            return p;
        }

        public static Vector3 Interpolate(InterpolationPoint start, InterpolationPoint end, float t)
        {
            if (start.interpolation == Interpolation.Linear && end.interpolation == Interpolation.Linear)
            {
                return Vector3.Lerp(start.position, end.position, t);
            }
            else
            {
                Vector3 tangent1 = Vector3.zero;
                switch (start.interpolation)
                {
                    case Interpolation.Linear:
                        tangent1 = (end.position + start.position) / 2f;
                        break;
                    case Interpolation.BezierSimple:
                        tangent1 = start.tangent1;
                        break;
                    case Interpolation.BezierFree:
                        tangent1 = start.tangent1;
                        break;
                }

                Vector3 tangent2 = Vector3.zero;
                switch (end.interpolation)
                {
                    case Interpolation.Linear:
                        tangent2 = (end.position + start.position) / 2f;
                        break;
                    case Interpolation.BezierSimple:
                        tangent2 = 2f * end.position - end.tangent1;
                        break;
                    case Interpolation.BezierFree:
                        tangent2 = end.tangent2;
                        break;
                }

                return CubeBezier3(start.position, end.position, tangent1, tangent2, t).normalized;
            }
        }

        public static Vector3 InterpolateTangent(InterpolationPoint start, InterpolationPoint end, float t)
        {
            if (start.interpolation == Interpolation.Linear && end.interpolation == Interpolation.Linear)
            {
                return (end.position - start.position).normalized;
            }
            else
            {
                Vector3 tangent1 = Vector3.zero;
                switch (start.interpolation)
                {
                    case Interpolation.Linear:
                        tangent1 = (end.position + start.position) / 2f;
                        break;
                    case Interpolation.BezierSimple:
                        tangent1 = start.tangent1;
                        break;
                    case Interpolation.BezierFree:
                        tangent1 = start.tangent1;
                        break;
                }

                Vector3 tangent2 = Vector3.zero;
                switch (end.interpolation)
                {
                    case Interpolation.Linear:
                        tangent2 = (end.position + start.position) / 2f;
                        break;
                    case Interpolation.BezierSimple:
                        tangent2 = 2f * end.position - end.tangent1;
                        break;
                    case Interpolation.BezierFree:
                        tangent2 = end.tangent2;
                        break;
                }

                return CubeBezier3Tangent(start.position, end.position, tangent1, tangent2, t);
            }
        }
    }
}

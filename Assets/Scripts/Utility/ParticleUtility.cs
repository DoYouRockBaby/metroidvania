﻿using UnityEngine;

namespace Assets.Scripts.Utility
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleUtility : MonoBehaviour
    {
        private new ParticleSystem particleSystem;

        private void Awake()
        {
            particleSystem = GetComponent<ParticleSystem>();
        }

        public void PlayParticleSystem()
        {
            particleSystem.Play();
        }
    }
}

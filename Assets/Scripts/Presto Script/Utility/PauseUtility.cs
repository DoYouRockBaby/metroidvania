﻿using System;
using System.Linq;
using UnityEngine;

namespace Presto_Script.Utility
{
    class UnFreezableAttribute : Attribute
    {
    }

    static class PauseUtility
    {
        private static MonoBehaviour[] lastFreezed = null;

        private static bool freezed = false;
        public static bool Freezed
        {
            set
            {
                if(value == freezed)
                {
                    return;
                }

                freezed = value;

                if (freezed)
                {
                    lastFreezed = UnityEngine.Object.FindObjectsOfType<MonoBehaviour>().Where((e) =>
                    {
                        var namespacee = e.GetType().Namespace?.Split('.')?.FirstOrDefault();

                        return e.enabled &&
                        namespacee != "Unity" &&
                        namespacee != "UnityEngine" &&
                        !e.GetType().CustomAttributes.Where((f) => f.AttributeType == typeof(UnFreezableAttribute)).Any();
                    }).ToArray();

                    foreach(var behaviour in lastFreezed)
                    {
                        behaviour.enabled = false;
                    }
                }
                else
                {
                    if(lastFreezed != null)
                    {
                        foreach (var behaviour in lastFreezed)
                        {
                            if(behaviour != null)
                            {
                                behaviour.enabled = true;
                            }
                        }

                        lastFreezed = null;
                    }
                }
            }
            get
            {
                return freezed;
            }
        }
    }
}

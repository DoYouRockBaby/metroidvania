﻿using Presto_Script.Core;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    class InstructionNode : IDrawable, IEventHandler
    {
        public IInstruction Instruction { get; private set; }
        public Rect Area { get; private set; }
        private float NodeHeight { get; set; } = float.MaxValue;

        public bool IsDragged { get; private set; }
        public bool IsSelected { get; private set; }

        private Action<IInstruction> onRemoveInstruction;

        static IInputReference selectedInputPoint;
        static IOutputReference selectedOutputPoint;

        static Dictionary<IOutputReference, Vector2> lastOutputPositions = new Dictionary<IOutputReference, Vector2>();

        //Style
        static float nodeWidth = 300f;
        static GUIStyle defaultNodeStyle;
        static GUIStyle selectedNodeStyle;
        static GUIStyle nodeHeader;
        static GUIStyle alignRight;

        struct DrawnInput
        {
            public IInputReference input;
            public Vector2 center;
        }

        struct DrawnOutput
        {
            public IOutputReference output;
            public Vector2 center;
        }

        public InstructionNode(IInstruction instruction, Action<IInstruction> onRemoveInstruction)
        {
            Instruction = instruction;
            this.onRemoveInstruction = onRemoveInstruction;
        }

        public void Draw(Rect area)
        {
            var nodeStyle = IsSelected ? selectedNodeStyle : defaultNodeStyle;

            var areaRect = new Rect(Instruction.GridPosition.x, Instruction.GridPosition.y, nodeWidth, NodeHeight);

            GUILayout.BeginArea(Area);
            GUILayout.BeginVertical(nodeStyle);

            GUILayout.Label(Instruction.Name, nodeHeader, GUILayout.Height(30));

            GUILayout.BeginHorizontal();

            GUILayout.BeginVertical();
            List<DrawnInput> drawnInputs;
            DrawInputs(Instruction, out drawnInputs);
            GUILayout.EndVertical();

            GUILayout.BeginVertical(alignRight);
            List<DrawnOutput> drawnOutputs;
            DrawOutputs(Instruction, out drawnOutputs);
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            List<DrawnInput> drawnNextInstructions;
            DrawInstructions(Instruction, out drawnNextInstructions);
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            var instruction = Instruction;

            var newRect = GUILayoutUtility.GetLastRect();
            areaRect.height = newRect.height;
            Area = areaRect;

            GUILayout.EndArea();

            DrawConnectionPoints(drawnInputs, drawnOutputs, drawnNextInstructions);
        }

        void DrawInputs(IInstruction instruction, out List<DrawnInput> drawn)
        {
            drawn = new List<DrawnInput>();

            foreach (var input in instruction.Inputs)
            {
                if (input.OutputPipe != null)
                {
                    //If we have a link, we only display the field name
                    EditorGUILayout.LabelField(new GUIContent(input.Name));

                    drawn.Add(new DrawnInput()
                    {
                        center = GUILayoutUtility.GetLastRect().center,
                        input = input
                    });
                }
                else
                {
                    //We display the property form otherwise
                    InputField.DrawFieldForInput(input);

                    drawn.Add(new DrawnInput()
                    {
                        center = GUILayoutUtility.GetLastRect().center,
                        input = input
                    });
                }
            }
        }

        void DrawOutputs(IInstruction instruction, out List<DrawnOutput> drawn)
        {
            drawn = new List<DrawnOutput>();

            foreach (var output in instruction.Outputs)
            {
                EditorGUILayout.LabelField(output.Name);

                drawn.Add(new DrawnOutput()
                {
                    center = GUILayoutUtility.GetLastRect().center,
                    output = output
                });
            }
        }

        void DrawInstructions(IInstruction instruction, out List<DrawnInput> drawn)
        {
            drawn = new List<DrawnInput>();

            foreach (var next in instruction.NextInstructions)
            {
                EditorGUILayout.LabelField(next.Name);

                drawn.Add(new DrawnInput()
                {
                    center = GUILayoutUtility.GetLastRect().center,
                    input = next
                });
            }
        }

        void DrawConnectionPoints(IEnumerable<DrawnInput> drawnInputs, IEnumerable<DrawnOutput> drawnOutputs, IEnumerable<DrawnInput> drawnNextInstructions)
        {
            if (Instruction.CanBeReferenced)
            {
                var position = OutputPointDrawer.DrawPoint(Instruction.This, Area, Area.center, OnSelectOutputPoint);

                if(lastOutputPositions.ContainsKey(Instruction.This))
                {
                    lastOutputPositions[Instruction.This] = position;
                }
                else
                {
                    lastOutputPositions.Add(Instruction.This, position);
                }

                if (selectedOutputPoint != null && selectedOutputPoint == Instruction.This)
                {
                    DrawConnectionLine(position, Event.current.mousePosition, Instruction.This);
                }
            }

            foreach (var output in drawnOutputs)
            {
                var position = OutputPointDrawer.DrawPoint(output.output, Area, output.center, OnSelectOutputPoint);

                if (lastOutputPositions.ContainsKey(output.output))
                {
                    lastOutputPositions[output.output] = position;
                }
                else
                {
                    lastOutputPositions.Add(output.output, position);
                }

                if (selectedOutputPoint != null && selectedOutputPoint == output.output)
                {
                    DrawConnectionLine(position, Event.current.mousePosition, output.output);
                }
            }

            foreach (var input in drawnInputs)
            {
                var position = InputPointDrawer.DrawPoint(input.input, Area, input.center, OnSelectInputPoint);

                if (input.input.OutputPipe != null && lastOutputPositions.ContainsKey(input.input.OutputPipe))
                {
                    DrawConnectionLine(position, lastOutputPositions[input.input.OutputPipe], input.input);
                }

                if (selectedInputPoint != null && selectedInputPoint == input.input)
                {
                    DrawConnectionLine(position, Event.current.mousePosition, input.input);
                }
            }

            foreach (var input in drawnNextInstructions)
            {
                var position = InputPointDrawer.DrawPoint(input.input, Area, input.center, OnSelectInputPoint);

                if (input.input.OutputPipe != null && lastOutputPositions.ContainsKey(input.input.OutputPipe))
                {
                    DrawConnectionLine(position, lastOutputPositions[input.input.OutputPipe], input.input);
                }

                if (selectedInputPoint != null && selectedInputPoint == input.input)
                {
                    DrawConnectionLine(position, Event.current.mousePosition, input.input);
                }
            }
        }

        private void DrawConnectionLine(Vector2 center, Vector2 target, IInputReference input)
        {
            Handles.DrawBezier(
                center,
                target,
                ConnectionPointUtility.GetConnectionPointTangent(input, center, target),
                ConnectionPointUtility.GetConnectionOpositePointTangent(input, center, target),
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        private void DrawConnectionLine(Vector2 center, Vector2 target, IOutputReference output)
        {
            Handles.DrawBezier(
                center,
                target,
                ConnectionPointUtility.GetConnectionPointTangent(output, center, target),
                ConnectionPointUtility.GetConnectionOpositePointTangent(output, center, target),
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        public void OnSelectInputPoint(IInputReference input)
        {
            selectedInputPoint = input;
            CreateNewPipeIfNeeded();
            selectedOutputPoint = null;
        }

        public void OnSelectOutputPoint(IOutputReference output)
        {
            selectedOutputPoint = output;
            CreateNewPipeIfNeeded();
            selectedInputPoint = null;
        }

        public void CreateNewPipeIfNeeded()
        {
            if(selectedInputPoint != null && selectedOutputPoint != null && selectedInputPoint.Type == selectedOutputPoint.Type)
            {
                selectedInputPoint.OutputPipe = selectedOutputPoint;
                selectedInputPoint = null;
                selectedOutputPoint = null;
            }
        }

        public void HandleEvent(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (Area.Contains(e.mousePosition))
                        {
                            GUI.changed = true;
                            IsDragged = true;
                            IsSelected = true;
                            selectedInputPoint = null;
                            selectedOutputPoint = null;
                        }
                        else
                        {
                            GUI.changed = true;
                            IsSelected = false;
                            selectedInputPoint = null;
                            selectedOutputPoint = null;
                        }

                        //selectedConnectionPoint = null;
                    }

                    if (e.button == 1 && IsSelected && Area.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:

                    if(IsDragged)
                    {
                        IsDragged = false;
                    }

                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && IsDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                    }
                    break;
            }
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, () => onRemoveInstruction(Instruction));
            genericMenu.ShowAsContext();
        }

        public void Drag(Vector2 delta)
        {
            Instruction.GridPosition += delta;
        }

        static InstructionNode()
        {
            defaultNodeStyle = new GUIStyle();
            defaultNodeStyle.border = new RectOffset(12, 12, 12, 12);
            defaultNodeStyle.alignment = TextAnchor.MiddleCenter;
            defaultNodeStyle.padding = new RectOffset(15, 15, 5, 15);
            defaultNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            defaultNodeStyle.normal.textColor = Color.white;

            selectedNodeStyle = new GUIStyle();
            selectedNodeStyle.border = new RectOffset(12, 12, 12, 12);
            selectedNodeStyle.alignment = TextAnchor.MiddleCenter;
            selectedNodeStyle.padding = new RectOffset(15, 15, 5, 15);
            selectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
            selectedNodeStyle.normal.textColor = Color.white;

            nodeHeader = new GUIStyle();
            nodeHeader.alignment = TextAnchor.MiddleCenter;
            nodeHeader.fontStyle = FontStyle.Bold;
            nodeHeader.normal.textColor = Color.white;

            alignRight = new GUIStyle();
            alignRight.alignment = TextAnchor.MiddleRight;
        }
    }
}

﻿using Presto_Script.Instruction.Persistence;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor.Instruction.Persistence
{
    [CustomEditor(typeof(SaveManager))]
    public class SaveManagerEditor : UnityEditor.Editor
    {
        class SaveEditorStatus
        {
            public string newEntryKey = "";
            public string newEntryValue = "";
            public bool showEntry = false;
        }

        Dictionary<Save, SaveEditorStatus> saveStatus = new Dictionary<Save, SaveEditorStatus>();

        public override void OnInspectorGUI()
        {
            var saveManager = target as SaveManager;

            //Draw "add save" button
            if (GUILayout.Button(new GUIContent("New Save")))
            {
                saveManager.New();
            }

            //Draw "reload" button
            if (GUILayout.Button(new GUIContent("Reload")))
            {
                saveManager.Reload();
            }

            saveStatus = saveStatus.Where(p => saveManager.Saves.Contains(p.Key)).ToDictionary(pair => pair.Key, pair => pair.Value);

            //Draw the saves
            var savesToRemove = new List<Save>();
            foreach (var save in saveManager.Saves)
            {
                if(save != null)
                {
                    //Add status if not aldready
                    SaveEditorStatus status = null;
                    if (!saveStatus.ContainsKey(save))
                    {
                        saveStatus.Add(save, status = new SaveEditorStatus());
                    }
                    else
                    {
                        status = saveStatus[save];
                    }

                    GUILayout.BeginHorizontal();

                    status.showEntry = EditorGUILayout.Foldout(status.showEntry, save.Name);

                    if(status.showEntry)
                    {
                        //Save button
                        if (GUILayout.Button(new GUIContent("Save"), GUILayout.ExpandWidth(false)))
                        {
                            save.Write();
                        }
                    }
                    else
                    {
                        //Remove button
                        if (GUILayout.Button(new GUIContent("X"), GUILayout.ExpandWidth(false)))
                        {
                            savesToRemove.Add(save);
                        }
                    }

                    GUILayout.EndHorizontal();

                    if (status.showEntry)
                    {
                        //Draw new entry form
                        GUILayout.BeginHorizontal();

                        status.newEntryKey = EditorGUILayout.TextField(status.newEntryKey);
                        status.newEntryValue = EditorGUILayout.TextField(status.newEntryValue);

                        if (GUILayout.Button(new GUIContent("+")) && status.newEntryKey != "")
                        {
                            save[status.newEntryKey] = status.newEntryValue;
                        }

                        GUILayout.EndHorizontal();

                        //Draw list of entries
                        var entriesToRemove = new List<string>();
                        foreach (var entry in save.Entries)
                        {
                            GUILayout.BeginHorizontal();

                            //Display informations
                            EditorGUILayout.LabelField(entry.Key, GUILayout.MinWidth(25));
                            EditorGUILayout.LabelField(entry.Value.GetType().Name, GUILayout.MinWidth(25));
                            EditorGUILayout.LabelField(entry.Value.ToString(), GUILayout.MinWidth(50));

                            //Handle remove actions
                            if (GUILayout.Button(new GUIContent("x")))
                            {
                                entriesToRemove.Add(entry.Key);
                            }

                            GUILayout.EndHorizontal();
                        }

                        GUILayout.Space(12);

                        //Remove entries that need to be removed
                        foreach (var key in entriesToRemove)
                        {
                            save.Remove(key);
                        }
                    }
                }
            }

            //Remove the entries that need to be
            foreach (var save in savesToRemove)
            {
                saveManager.Remove(save);
            }
        }
    }
}

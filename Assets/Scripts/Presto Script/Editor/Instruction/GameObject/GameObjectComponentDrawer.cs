﻿using Presto_Script.Instruction.GameObject;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor.Instruction.GameObject
{
    [CustomPropertyDrawer(typeof(GameObjectComponent))]
    public class GameObjectComponentDrawer : PropertyDrawer
    {
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var gameObjectRect = new Rect(position.x, position.y, position.width, position.height / 2);
            var componentRect = new Rect(position.x, position.y + 16, position.width, position.height / 2);

            //Draw GameObject
            var gameObjectProperty = property.FindPropertyRelative("gameObject");
            var gameObject = gameObjectProperty.objectReferenceValue as UnityEngine.GameObject;
            EditorGUI.PropertyField(gameObjectRect, gameObjectProperty, GUIContent.none);

            //Draw Components
            if(gameObject != null)
            {
                var componentProperty = property.FindPropertyRelative("component");
                var component = componentProperty.objectReferenceValue as Component;
                var components = gameObject.GetComponents(typeof(Component));
                var currentIndex = ArrayUtility.IndexOf(components, component);
                var result = EditorGUI.Popup(componentRect, currentIndex, components.Select(e => new GUIContent(e.GetType().Name)).ToArray());
                if(result >= 0 && result < components.Length)
                {
                    componentProperty.objectReferenceValue = components[result];
                }
            }

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 2;
        }
    }
}

﻿using Presto_Script.Core;
using System;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    public static class InputPointDrawer
    {
        private static GUIStyle inPointStyle;
        private static GUIStyle nextPointStyle;

        //return point position
        public static Vector2 DrawPoint(IInputReference input, Rect nodeRect, Vector2 desiredCenter, Action<IInputReference> onClickInputPoint)
        {
            Rect rect = Rect.zero;
            GUIStyle style;

            if (input.Type == typeof(IInstruction))
            {
                rect = new Rect(
                    nodeRect.x + desiredCenter.x -100f,
                    nodeRect.y + nodeRect.height - 8f,
                    20f,
                    10f
                );

                style = nextPointStyle;
            }
            else
            {
                rect = new Rect(
                    nodeRect.x - 5f,
                    nodeRect.y + desiredCenter.y - 10f,
                    10f,
                    20f
                );

                style = inPointStyle;
            }

            if (GUI.Button(rect, "", style))
            {
                onClickInputPoint?.Invoke(input);
            }

            return rect.center;
        }

        static InputPointDrawer()
        {
            inPointStyle = new GUIStyle();
            inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
            inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
            inPointStyle.border = new RectOffset(4, 4, 12, 12);

            nextPointStyle = new GUIStyle();
            nextPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn mid.png") as Texture2D;
            nextPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn mid on.png") as Texture2D;
            nextPointStyle.border = new RectOffset(12, 12, 4, 4);
        }
    }
}
﻿using Presto_Script.Core;
using UnityEngine;

namespace Presto_Script.Editor
{
    class ConnectionPointUtility
    {
        static public Vector3 GetConnectionPointTangent(IInputReference input, Vector2 from, Vector2 to)
        {
            if (input.Type == typeof(IInstruction))
            {
                return from + Vector2.up * Vector2.Distance(from, to) / 2f;
            }
            else
            {
                return from + Vector2.left * Vector2.Distance(from, to) / 2f;
            }
        }

        static public Vector3 GetConnectionPointTangent(IOutputReference output, Vector2 from, Vector2 to)
        {
            if (output.Type == typeof(IInstruction))
            {
                return from + Vector2.down * Vector2.Distance(from, to) / 2f;
            }
            else
            {
                return from + Vector2.right * Vector2.Distance(from, to) / 2f;
            }
        }

        static public Vector3 GetConnectionOpositePointTangent(IInputReference input, Vector2 from, Vector2 to)
        {
            if (input.Type == typeof(IInstruction))
            {
                return to + Vector2.down * Vector2.Distance(from, to) / 2f;
            }
            else
            {
                return to + Vector2.right * Vector2.Distance(from, to) / 2f;
            }
        }

        static public Vector3 GetConnectionOpositePointTangent(IOutputReference output, Vector2 from, Vector2 to)
        {
            if (output.Type == typeof(IInstruction))
            {
                return to + Vector2.up * Vector2.Distance(from, to) / 2f;
            }
            else
            {
                return to + Vector2.left * Vector2.Distance(from, to) / 2f;
            }
        }
    }
}

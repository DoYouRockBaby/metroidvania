﻿using System;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    public class Grid : IDrawable
    {
        public Vector2 Offset { get; set; }
        public float Spacing { get; set; }
        public float Opacity { get; set; }
        public Color Color { get; set; }

        public void Draw(Rect area)
        {
            int widthDivs = Mathf.CeilToInt(area.width / Spacing);
            int heightDivs = Mathf.CeilToInt(area.height / Spacing);

            Handles.BeginGUI();
            Handles.color = new Color(Color.r, Color.g, Color.b, Opacity);

            Vector3 newOffset = new Vector3(Offset.x % Spacing, Offset.y % Spacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(Spacing * i, -Spacing, 0) + newOffset, new Vector3(Spacing * i, area.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-Spacing, Spacing * j, 0) + newOffset, new Vector3(area.width, Spacing * j, 0f) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }
    }
}

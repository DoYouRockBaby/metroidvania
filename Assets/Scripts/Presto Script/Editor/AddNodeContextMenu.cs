﻿using Presto_Script.Core;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    public class AddNodeContextMenu : IEventHandler
    {
        public delegate void AddFunctionDelegate(Vector2 mousePosition, Type blocType);
        public delegate void AddFunctionInstructionDelegate(Vector2 mousePosition, Script func);

        AddFunctionDelegate onClickAddInstruction;
        AddFunctionInstructionDelegate onClickAddFunctionInstruction;

        public AddNodeContextMenu(AddFunctionDelegate onClickAddInstruction, AddFunctionInstructionDelegate onClickAddFunctionInstruction)
        {
            this.onClickAddInstruction = onClickAddInstruction;
            this.onClickAddFunctionInstruction = onClickAddFunctionInstruction;
        }

        public void HandleEvent(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;
            }
        }

        public void ProcessContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();

            foreach (var instructionType in InstructionStore.Instructions)
            {
                if (!instructionType.InstantiableInEditor)
                {
                    continue;
                }

                genericMenu.AddItem(new GUIContent(instructionType.Label), false, () => onClickAddInstruction(mousePosition, instructionType.Type));
            }

            //Get asset functions
            foreach (var function in FindAssetsByType<Script>())
            {
                genericMenu.AddItem(new GUIContent("Function/" + Path.GetFileNameWithoutExtension(function.Key)), false, () => onClickAddFunctionInstruction(mousePosition, function.Value));
            }

            genericMenu.ShowAsContext();
        }

        private static Dictionary<string, T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
            Dictionary<string, T> assets = new Dictionary<string, T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if (asset != null)
                {
                    assets.Add(assetPath, asset);
                }
            }
            return assets;
        }
    }
}

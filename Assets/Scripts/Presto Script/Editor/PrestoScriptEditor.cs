﻿using Presto_Script.Core;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    [CustomEditor(typeof(PrestoScript))]
    class PrestoScriptEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var t = target as PrestoScript;
            if (t.script == null)
            {
                t.script = new Script();
            }

            if (GUILayout.Button("Edit Script"))
            {
                GraphScriptEditor.currentScript = t.script;
                EditorWindow.GetWindow(typeof(GraphScriptEditor));
                GUIUtility.ExitGUI();
            }

            DrawDefaultInspector();
        }
    }
}

﻿using Presto_Script.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Presto_Script.Editor
{
    static class InputField
    {
        delegate void FieldHandler(IInputReference input);
        static Dictionary<Type, FieldHandler> handlers = null;

        private static string[] layerNames = null;
        private static int[] layerMasks = null;

        static InputField()
        {
            handlers = new Dictionary<Type, FieldHandler>()
            {
                { typeof(Bounds), (i) => i.Value = EditorGUILayout.BoundsField(i.Name, (Bounds)i.Value) },
                { typeof(BoundsInt), (i) => i.Value = EditorGUILayout.BoundsIntField(i.Name, (BoundsInt)i.Value) },
                { typeof(Color), (i) => i.Value = EditorGUILayout.ColorField(i.Name, (Color)i.Value) },
                { typeof(AnimationCurve), (i) => i.Value = EditorGUILayout.CurveField(i.Name, (AnimationCurve)i.Value) },
                { typeof(double), (i) => i.Value = EditorGUILayout.DoubleField(i.Name, (double)i.Value) },
                { typeof(float), (i) => i.Value = EditorGUILayout.FloatField(i.Name, (float)i.Value) },
                { typeof(int), (i) => i.Value = EditorGUILayout.IntField(i.Name, (int)i.Value) },
                { typeof(long), (i) => i.Value = EditorGUILayout.LongField(i.Name, (long)i.Value) },
                { typeof(Rect), (i) => i.Value = EditorGUILayout.RectField(i.Name, (Rect)i.Value) },
                { typeof(RectInt), (i) => i.Value = EditorGUILayout.RectIntField(i.Name, (RectInt)i.Value) },
                { typeof(string), (i) => i.Value = EditorGUILayout.TextField(i.Name, (string)i.Value) },
                { typeof(bool), (i) => i.Value = EditorGUILayout.Toggle(i.Name, (bool)i.Value) },
                { typeof(Vector2), (i) => i.Value = EditorGUILayout.Vector2Field(i.Name, (Vector2)i.Value) },
                { typeof(Vector2Int), (i) => i.Value = EditorGUILayout.Vector2IntField(i.Name, (Vector2Int)i.Value) },
                { typeof(Vector3), (i) => i.Value = EditorGUILayout.Vector3Field(i.Name, (Vector3)i.Value) },
                { typeof(Vector3Int), (i) => i.Value = EditorGUILayout.Vector3IntField(i.Name, (Vector3Int)i.Value) },
                { typeof(Vector4), (i) => i.Value = EditorGUILayout.Vector4Field(i.Name, (Vector4)i.Value) },
                { typeof(Quaternion), (i) => i.Value = Quaternion.Euler(EditorGUILayout.Vector3Field(i.Name, ((Quaternion)i.Value).eulerAngles)) },
                { typeof(Enum), (i) => i.Value = EditorGUILayout.EnumPopup(i.Name, (Enum)i.Value) },
                { typeof(LayerMask), (i) => i.Value = LayerMaskField(i.Name, (LayerMask)i.Value) },
                { typeof(UnityEngine.Object), (i) => i.Value = EditorGUILayout.ObjectField(i.Name, (UnityEngine.Object)i.Value, i.Type, false) },
                { typeof(System.Object), (i) => EditorGUILayout.LabelField(i.Value + " (" + i.Type.Name + ") : Unsuported type") },
            };

            var tmpNames = new List<string>();
            var tmpMasks = new List<int>();
            for (int i = 0; i < 32; i++)
            {
                try
                {
                    var name = LayerMask.LayerToName(i);
                    if (name != "")
                    {
                        tmpNames.Add(name);
                        tmpMasks.Add(1 << i);
                    }
                }
                catch { }
            }

            layerNames = tmpNames.ToArray();
            layerMasks = tmpMasks.ToArray();
        }

        static public void DrawFieldForInput(IInputReference input)
        {
            if(handlers.ContainsKey(input.Type))
            {
                handlers[input.Type].Invoke(input);
            }
            else
            {
                var handler = handlers.Where(e => e.Key.IsAssignableFrom(input.Type)).Select(e => e.Value).First();
                handler.Invoke(input);
            }
        }

        private static LayerMask LayerMaskField(string label, LayerMask selected)
        {
            return LayerMaskField(label, selected, true);
        }

        private static LayerMask LayerMaskField(string label, LayerMask selected, bool showSpecial)
        {

            List<string> layers = new List<string>();
            List<int> layerNumbers = new List<int>();

            string selectedLayers = "";

            for (int i = 0; i < 32; i++)
            {

                string layerName = LayerMask.LayerToName(i);

                if (layerName != "")
                {
                    if (selected == (selected | (1 << i)))
                    {

                        if (selectedLayers == "")
                        {
                            selectedLayers = layerName;
                        }
                        else
                        {
                            selectedLayers = "Mixed";
                        }
                    }
                }
            }

            EventType lastEvent = Event.current.type;

            if (Event.current.type != EventType.MouseDown && Event.current.type != EventType.ExecuteCommand)
            {
                if (selected.value == 0)
                {
                    layers.Add("Nothing");
                }
                else if (selected.value == -1)
                {
                    layers.Add("Everything");
                }
                else
                {
                    layers.Add(selectedLayers);
                }
                layerNumbers.Add(-1);
            }

            if (showSpecial)
            {
                layers.Add((selected.value == 0 ? "[X] " : "     ") + "Nothing");
                layerNumbers.Add(-2);

                layers.Add((selected.value == -1 ? "[X] " : "     ") + "Everything");
                layerNumbers.Add(-3);
            }

            for (int i = 0; i < 32; i++)
            {

                string layerName = LayerMask.LayerToName(i);

                if (layerName != "")
                {
                    if (selected == (selected | (1 << i)))
                    {
                        layers.Add("[X] " + layerName);
                    }
                    else
                    {
                        layers.Add("     " + layerName);
                    }
                    layerNumbers.Add(i);
                }
            }

            bool preChange = GUI.changed;

            GUI.changed = false;

            int newSelected = 0;

            if (Event.current.type == EventType.MouseDown)
            {
                newSelected = -1;
            }

            newSelected = EditorGUILayout.Popup(label, newSelected, layers.ToArray(), EditorStyles.layerMaskField);

            if (GUI.changed && newSelected >= 0)
            {
                if (showSpecial && newSelected == 0)
                {
                    selected = 0;
                }
                else if (showSpecial && newSelected == 1)
                {
                    selected = -1;
                }
                else
                {

                    if (selected == (selected | (1 << layerNumbers[newSelected])))
                    {
                        selected &= ~(1 << layerNumbers[newSelected]);
                    }
                    else
                    {
                        selected = selected | (1 << layerNumbers[newSelected]);
                    }
                }
            }
            else
            {
                GUI.changed = preChange;
            }

            return selected;
        }
    }
}

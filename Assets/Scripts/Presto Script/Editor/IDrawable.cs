﻿using UnityEngine;

namespace Presto_Script.Editor
{
    interface IDrawable
    {
        void Draw(Rect area);
    }
}

﻿using Presto_Script.Core;
using System;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    public static class OutputPointDrawer
    {
        private static GUIStyle outPointStyle;
        private static GUIStyle selfPointStyle;

        //return point position
        public static Vector2 DrawPoint(IOutputReference output, Rect nodeRect, Vector2 desiredCenter, Action<IOutputReference> onClickOutputPoint)
        {
            Rect rect = Rect.zero;
            GUIStyle style;

            if (output.Type == typeof(IInstruction))
            {
                rect = new Rect(
                    nodeRect.x + nodeRect.width / 2f - 10f,
                    nodeRect.y,
                    20f,
                    10f
                );

                style = selfPointStyle;
            }
            else
            {
                rect = new Rect(
                    nodeRect.max.x - 5f,
                    nodeRect.y + desiredCenter.y - 10f,
                    10f,
                    20f
                );

                style = outPointStyle;
            }

            if (GUI.Button(rect, "", style))
            {
                onClickOutputPoint?.Invoke(output);
            }

            return rect.center;
        }

        static OutputPointDrawer()
        {
            selfPointStyle = new GUIStyle();
            selfPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn mid.png") as Texture2D;
            selfPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn mid on.png") as Texture2D;
            selfPointStyle.border = new RectOffset(12, 12, 4, 4);

            outPointStyle = new GUIStyle();
            outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
            outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
            outPointStyle.border = new RectOffset(4, 4, 12, 12);
        }
    }
}
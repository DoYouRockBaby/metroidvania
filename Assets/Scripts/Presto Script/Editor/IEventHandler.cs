﻿using UnityEngine;

namespace Presto_Script.Editor
{
    interface IEventHandler
    {
        void HandleEvent(Event e);
    }
}

﻿using Presto_Script.Core;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    [CustomEditor(typeof(Script))]
    class EasyScriptEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var script = target as Script;

            if (GUILayout.Button("Edit Script"))
            {
                GraphScriptEditor.currentScript = script;
                EditorWindow.GetWindow(typeof(GraphScriptEditor));
                GUIUtility.ExitGUI();
            }

            DrawDefaultInspector();
        }
    }
}
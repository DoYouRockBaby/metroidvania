﻿using Presto_Script.Core;
using Presto_Script.Instruction.Function;
using Presto_Script.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Editor
{
    public class GraphScriptEditor : EditorWindow
    {
        public static Script currentScript;
        private List<IDrawable> Drawables = new List<IDrawable>();
        private List<IEventHandler> EventHandlers = new List<IEventHandler>();
        private List<InstructionNode> Nodes = new List<InstructionNode>();

        private Grid grid1;
        private Grid grid2;

        public static float zoomScale { get; private set; } = 1f;

        [MenuItem("Window/Graph Script Editor")]
        private static void OpenWindow()
        {
            GraphScriptEditor window = GetWindow<GraphScriptEditor>();
            window.titleContent = new GUIContent("Graph Script Editor");
        }

        private void OnEnable()
        {
            Drawables.Add(grid1 = new Grid()
            {
                Spacing = 20,
                Opacity = .2f,
                Color = Color.gray
            });

            Drawables.Add(grid2 = new Grid()
            {
                Spacing = 100,
                Opacity = .4f,
                Color = Color.gray
            });

            EventHandlers.Add(new AddNodeContextMenu(AddInstruction, AddFunctionInstruction));

            UpdateScript();
        }

        private void OnSelectionChange()
        {
            UpdateScript();
        }

        private void OnGUI()
        {
            if (currentScript == null)
            {
                return;
            }

            //Save button
            GUILayout.BeginHorizontal(EditorStyles.toolbar);

            if (GUILayout.Button("Save", EditorStyles.toolbarButton))
            {
                EditorUtility.SetDirty(currentScript);
                AssetDatabase.SaveAssets();
            }

            if (GUILayout.Button("Load", EditorStyles.toolbarButton))
            {
                string guid;
                long localId;

                if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(currentScript, out guid, out localId))
                {
                    AssetDatabase.ForceReserializeAssets(new string[] { AssetDatabase.GUIDToAssetPath(guid) });
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            //Zoom-dezoom
            /*Matrix4x4 before = GUI.matrix;
            Matrix4x4 Translation = Matrix4x4.TRS(new Vector3(0, 25, 0), Quaternion.identity, Vector3.one);
            Matrix4x4 Scale = Matrix4x4.Scale(new Vector3(zoomScale, zoomScale, zoomScale));
            GUI.matrix = Translation * Scale * Translation.inverse;*/

            //Update node to fit the script contents
            UpdateInstructionNodes();

            //Draw
            foreach (var drawable in Drawables)
            {
                drawable.Draw(position);
            }

            //Event
            foreach (var handler in EventHandlers)
            {
                handler.HandleEvent(Event.current);
            }

            ProcessEvents(Event.current);

            //Repaint
            if (GUI.changed)
            {
                Repaint();
            }

            //Restore zoom
            //GUI.matrix = before;
        }

        private void UpdateScript()
        {
            /*if (Selection.activeObject is GameObject && (Selection.activeObject as GameObject).GetComponent<PrestoScript>() != null)
            {
                currentScript = (Selection.activeObject as GameObject).GetComponent<PrestoScript>().script;
            }
            else if (Selection.activeObject is Script)
            {
                currentScript = Selection.activeObject as Script;
            }
            else
            {
                currentScript = null;
            }*/
        }

        private void UpdateInstructionNodes()
        {
            var newInstructions = currentScript.instructions.Where((e) => Nodes.FindAll((f) => f.Instruction == e).Count == 0);

            foreach(var instruction in newInstructions)
            {
                AddInstructionNodes(instruction);
            }

            var oldInstructions = Nodes.Where((e) => currentScript.instructions.Where((f) => e.Instruction == f).Count() == 0).ToList();

            foreach (var instruction in oldInstructions)
            {
                RemoveInstructionNode(instruction);
            }
        }

        private void AddInstructionNodes(IInstruction instruction)
        {
            var node = new InstructionNode(instruction, RemoveInstruction);

            Nodes.Add(node);
            Drawables.Add(node);
            EventHandlers.Add(node);

            GUI.changed = true;
        }

        private void RemoveInstructionNode(InstructionNode node)
        {
            Nodes.Remove(node);
            Drawables.Remove(node);
            EventHandlers.Remove(node);

            GUI.changed = true;
        }

        private void AddInstruction(Vector2 mousePosition, Type blocType)
        {
            var instruction = ObjectUtility.CreateDefaultConstructor(blocType).Invoke() as IInstruction;

            //var instruction = CreateInstance(blocType) as IInstruction;
            instruction.GridPosition = mousePosition;

            currentScript.instructions.Add(instruction);
        }

        private void AddFunctionInstruction(Vector2 mousePosition, Script func)
        {
            var instruction = new Function();
            //var instruction = CreateInstance(typeof(Function)) as Function;
            instruction.GridPosition = mousePosition;
            instruction.script = func;

            currentScript.instructions.Add(instruction);
        }

        public void RemoveInstruction(IInstruction instruction)
        {
            currentScript.instructions.Remove(instruction);
        }

        public void ProcessEvents(Event e)
        {
            var hasSelectedNode = Nodes.Where((n) => n.IsSelected).Any();

            switch (e.type)
            {
                case EventType.MouseDrag:
                    if (e.button == 0)
                    {
                        grid1.Offset += e.delta;
                        grid2.Offset += e.delta;

                        if (!hasSelectedNode)
                        {
                            foreach (var node in Nodes)
                            {
                                node.Drag(e.delta);
                            }
                        }

                        e.Use();
                    }

                    break;
                
                case EventType.ScrollWheel:
                    zoomScale += e.delta.magnitude;
                    e.Use();
                    break;
            }
        }
    }
}
﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Utility
{
    [Instruction("Utility/Debug"), Serializable]
    public class Debug : BaseInstruction
    {
        [Input("Value")]
        public UnityEngine.Object value;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            UnityEngine.Debug.Log(value);
        }
    }
}

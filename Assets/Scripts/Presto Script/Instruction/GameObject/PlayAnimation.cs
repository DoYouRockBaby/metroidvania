﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Play Animation"), Serializable]
    public class PlayAnimation : BaseInstruction
    {
        [Input("GameObject")]
        public UnityEngine.GameObject gameObject;

        [Input("Animation Name")]
        public string animationName;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            if (gameObject == null)
            {
                return;
            }

            var animator = gameObject.GetComponent<Animator>();

            if (animator != null)
            {
                animator.Rebind();
                animator.Play(Animator.StringToHash(animationName));
            }
        }
    }
}
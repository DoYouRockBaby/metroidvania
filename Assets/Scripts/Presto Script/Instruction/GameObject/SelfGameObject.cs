﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Self", false), Serializable]
    public class SelfGameObject : BaseInstruction
    {
        [Output("GameObject")]
        public UnityEngine.GameObject GameObject
        {
            get
            {
                if (ScriptExecuter.Last != null)
                {
                    return ScriptExecuter.Last.GameObject;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

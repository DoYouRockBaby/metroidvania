﻿using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Serializable]
    public struct GameObjectComponent
    {
        public UnityEngine.GameObject gameObject;
        public Component component;
    }
}

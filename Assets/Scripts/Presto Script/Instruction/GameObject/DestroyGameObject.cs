﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Destroy"), Serializable]
    public class DestroyGameObject : BaseInstruction
    {
        [Input("GameObject")]
        public UnityEngine.GameObject gameObject;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            Destroy(gameObject);
        }
    }
}
﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Set Trigger"), Serializable]
    public class SetTrigger : BaseInstruction
    {
        [Input("GameObject")]
        public UnityEngine.GameObject gameObject;

        [Input("Animation Name")]
        public string trigger;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            if (gameObject == null)
            {
                return;
            }

            gameObject.GetComponent<Animator>()?.SetTrigger(trigger);
        }
    }
}

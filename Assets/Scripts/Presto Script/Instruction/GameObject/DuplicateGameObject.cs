﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Duplicate"), Serializable]
    public class DuplicateGameObject : BaseInstruction
    {
        [Input("GameObject")]
        public UnityEngine.GameObject gameObject;

        [Input("Position")]
        public Vector3 position;

        [Input("Rotation")]
        public Quaternion rotation = Quaternion.identity;

        [Output("Copy")]
        public UnityEngine.GameObject Copy
        {
            get
            {
                if (_copy == null)
                {
                    _copy = Instantiate(gameObject, position, rotation);
                }

                return _copy;
            }
        }

        [NextInstruction("Next")]
        public IInstruction next;

        private UnityEngine.GameObject _copy = null;
    }
}

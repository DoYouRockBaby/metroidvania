﻿using Presto_Script.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Component Properties", false), Serializable]
    public class ComponentProperties : BaseInstruction
    {
        [Input("GameObject Component")]
        public GameObjectComponent gameObjectComponent;

        protected class OutputMemberReference : IOutputReference
        {
            private UnityEngine.Object instance;
            private string propertyName;

            public OutputMemberReference(UnityEngine.Object instance, string propertyName, string label, Type type)
            {
                this.instance = instance;
                this.propertyName = propertyName;
                Name = label;
                Type = type;
            }

            override public string Name { get; }

            override public object Value
            {
                get
                {
                    if (instance.GetType().GetProperty(propertyName) != null)
                    {
                        return instance.GetType().GetProperty(propertyName).GetValue(instance, null);
                    }
                    else
                    {
                        return instance.GetType().GetField(propertyName).GetValue(instance);
                    }
                }
            }

            override public Type Type { get; }

            public override IInstruction[] Dependencies => new IInstruction[] { };
        }

        private Component lastComponent = null;
        private IOutputReference[] outputs = new IOutputReference[0];
        public override IOutputReference[] Outputs
        {
            get
            {
                if(gameObjectComponent.component != lastComponent)
                {
                    var newOutputs = new List<IOutputReference>();

                    var properties = gameObjectComponent.component.GetType().GetProperties();
                    foreach(var property in properties)
                    {
                        newOutputs.Add(new OutputMemberReference(gameObjectComponent.component, property.Name, property.Name, property.PropertyType));
                    }

                    outputs = newOutputs.ToArray();
                    lastComponent = gameObjectComponent.component;
                }

                return outputs;
            }
        }
    }
}
﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.GameObject
{
    [Instruction("GameObject/Bounds", false), Serializable]
    class GameObjectBounds : BaseInstruction
    {
        [Input("GameObject")]
        public UnityEngine.GameObject gameObject;

        [Output("Center")]
        public Vector3 Center
        {
            get
            {
                return Bounds.center;
            }
        }

        [Output("Min")]
        public Vector3 Min
        {
            get
            {
                return Bounds.min;
            }
        }

        [Output("Max")]
        public Vector3 Max
        {
            get
            {
                return Bounds.max;
            }
        }

        private Bounds Bounds
        {
            get
            {
                if (gameObject != null && gameObject.GetComponent(typeof(Collider2D)) != null)
                {
                    return (Bounds)gameObject.GetComponent<Collider2D>()?.bounds;
                }

                if (gameObject != null && gameObject.GetComponent(typeof(Renderer)) != null)
                {
                    return (Bounds)gameObject.GetComponent<Renderer>()?.bounds;
                }

                if (gameObject != null && gameObject.GetComponent(typeof(Collider)) != null)
                {
                    return (Bounds)gameObject.GetComponent<Collider>()?.bounds;
                }

                return default(Bounds);
            }
        }
    }
}

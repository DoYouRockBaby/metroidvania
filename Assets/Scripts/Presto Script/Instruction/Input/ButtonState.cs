﻿using Presto_Script.Core;
using System;
using UnityStandardAssets.CrossPlatformInput;

namespace Presto_Script.Instruction.Input
{
    [Instruction("Input/Button State", false), Serializable]
    public class ButtonState : BaseInstruction
    {
        [Serializable]
        public enum InputType
        {
            Down,
            Up,
            Stay
        };

        [Input("Input Name")]
        public string inputName;

        [Input("Type")]
        public InputType type = InputType.Stay;

        [Output("State")]
        public bool State
        {
            get
            {
                switch (type)
                {
                    case InputType.Stay:
                        return CrossPlatformInputManager.GetButton(inputName);
                    case InputType.Down:
                        return CrossPlatformInputManager.GetButtonDown(inputName);
                    case InputType.Up:
                        return CrossPlatformInputManager.GetButtonUp(inputName);
                    default:
                        return CrossPlatformInputManager.GetButton(inputName);
                }
            }
        }
    }
}

﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Persistence
{
    [Instruction("Persistence/Set"), Serializable]
    class LoadEntry : BaseInstruction
    {
        [Input("Key")]
        public string key;

        [Input("Value")]
        public object value;

        public override void Action(ScriptExecuter exec)
        {
            if (key != "" && SaveManager.Instance.CurrentSave != null)
            {
                SaveManager.Instance.CurrentSave[key] = value;
            }
        }
    }
}

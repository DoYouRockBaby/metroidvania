﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Persistence
{
    [Instruction("Persistence/Interruptor"), Serializable]
    class Interruptor : BaseInstruction
    {
        [NextInstruction("First")]
        public IInstruction first = null;

        [NextInstruction("Others")]
        public IInstruction others;

        private bool alreadyUsed = false;

        public override void Action(ScriptExecuter exec)
        {
            if (SaveManager.Instance.CurrentSave != null)
            {
                var script = exec.GameObject?.GetComponent<PrestoScript>()?.script;
                if (script != null)
                {
                    var thisIndex = script.instructions.IndexOf(this);

                    if (thisIndex != -1)
                    {
                        var identifier = "_____INTERRUPTOR_____" + exec.GameObject.transform.GetSiblingIndex() + "_____" + thisIndex;
                        alreadyUsed = SaveManager.Instance.CurrentSave.ContainsKey(identifier);
                        SaveManager.Instance.CurrentSave[identifier] = true;
                    }
                }
            }
        }

        public override IOutputReference[] NextExecutableInstructions
        {
            get
            {
                if (alreadyUsed)
                {
                    return new IOutputReference[] { new ThisReference(others) };
                }
                else
                {
                    return new IOutputReference[] { new ThisReference(first) };
                }
            }
        }
    }
}

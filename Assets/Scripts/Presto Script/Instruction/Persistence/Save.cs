﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using UnityEngine;

namespace Presto_Script.Instruction.Persistence
{
    public class Save : ScriptableObject
    {
        Dictionary<string, object> entries = new Dictionary<string, object>();

        public Save(string path)
        {
            Path = path;
        }

        public string Path { get; private set; }
        public string Name => System.IO.Path.GetFileNameWithoutExtension(Path);
        public bool IsLoaded { get; private set; } = false;
        public IEnumerable<KeyValuePair<string, object>> Entries
        {
            get
            {
                if (!IsLoaded)
                {
                    Load();
                }

                return entries;
            }
        }

        public object this[string key]
        {
            get
            {
                if(!IsLoaded)
                {
                    Load();
                }

                return entries[key];
            }
            set
            {
                if (!IsLoaded)
                {
                    Load();
                }

                if(entries.ContainsKey(key))
                {
                    entries[key] = value;
                }
                else
                {
                    entries.Add(key, value);
                }
            }
        }

        public bool ContainsKey(string key)
        {
            return entries.ContainsKey(key);
        }

        public void Remove(string key)
        {
            if (!IsLoaded)
            {
                Load();
            }

            entries.Remove(key);
        }

        public void Load()
        {
            /*BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream file = File.OpenRead(Path))
            {
                entries = formatter.Deserialize(file) as Dictionary<string, object>;
            }*/

            var serializer = new DataContractSerializer(typeof(Dictionary<string, object>));
            using (FileStream file = File.OpenRead(Path))
            {
                entries = serializer.ReadObject(file) as Dictionary<string, object>;
                file.Close();
            }

            if(entries == null)
            {
                entries = new Dictionary<string, object>();
            }
        }

        public void Write()
        {
            /*BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream file = File.Create(Path))
            {
                formatter.Serialize(file, entries);
            }*/

            var serializer = new DataContractSerializer(typeof(Dictionary<string, object>));
            using (FileStream file = File.Create(Path))
            {
                serializer.WriteObject(file, entries);
                file.Close();
            }
        }
    }
}

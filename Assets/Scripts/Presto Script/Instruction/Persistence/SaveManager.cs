﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Presto_Script.Instruction.Persistence
{
    public class SaveManager : MonoBehaviour
    {
        static SaveManager instance = null;
        public static SaveManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<SaveManager>();
                    if(instance == null)
                    {
                        instance = new SaveManager();
                    }
                }

                return instance;
            }
        }

        private Save currentSave = null;
        public Save CurrentSave
        {
            get
            {
                if(currentSave == null)
                {
                    currentSave = New();
                }

                return currentSave;
            }
            set
            {
                currentSave = value;
            }
        }

        List<Save> saves = new List<Save>();

        public Save New()
        {
            var filepath = Application.persistentDataPath + "/save";
            filepath = GenerateUniqueFilepath(filepath, "gd");

            var save = new Save(filepath);
            save.Write();

            saves.Add(save);

            return save;
        }

        public void Remove(Save save)
        {
            saves.Remove(save);
            File.Delete(save.Path);
        }

        public void Reload()
        {
            saves.Clear();
            foreach (var filepath in Directory.GetFiles(Application.persistentDataPath, "*.gd"))
            {
                saves.Add(new Save(filepath));
            }
        }

        public IEnumerable<Save> Saves
        {
            get
            {
                if(saves == null)
                {
                    Reload();
                }

                return saves;
            }
        }

        private string GenerateUniqueFilepath(string filepath, string extension)
        {
            if(File.Exists(filepath + "." + extension))
            {
                return GenerateUniqueFilepath(filepath, extension, 1);
            }
            else
            {
                return filepath + "." + extension;
            }
        }

        private string GenerateUniqueFilepath(string filepath, string extension, int number)
        {
            if (File.Exists(filepath + " " + number + "." + extension))
            {
                return GenerateUniqueFilepath(filepath, extension, number + 1);
            }
            else
            {
                return filepath + " " + number + "." + extension;
            }
        }
    }
}

﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Persistence
{
    [Instruction("Persistence/Save"), Serializable]
    class SaveAction : BaseInstruction
    {
        public override void Action(ScriptExecuter exec)
        {
            if (SaveManager.Instance.CurrentSave != null)
            {
                SaveManager.Instance.CurrentSave.Write();
            }
        }
    }
}

﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Persistence
{
    [Instruction("Persistence/Get", false), Serializable]
    class GetEntry : BaseInstruction
    {
        [Input("Key")]
        public string key;

        [Output("Value")]
        public object Value
        {
            get
            {
                if(key == "")
                {
                    return null;
                }
                else if(SaveManager.Instance.CurrentSave == null)
                {
                    return null;
                }
                else
                {
                    return SaveManager.Instance.CurrentSave[key];
                }
            }
        }
    }
}

﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Base
{
    [Instruction("Base/Condition"), Serializable]
    public class Condition : BaseInstruction
    {
        [Input("?")]
        public bool condition;

        [NextInstruction("Then")]
        public IInstruction then = null;

        [NextInstruction("Else")]
        public IInstruction eelse;

        public override IOutputReference[] NextExecutableInstructions
        {
            get
            {
                if(condition)
                {
                    return new IOutputReference[] { new ThisReference(then) };
                }
                else
                {
                    return new IOutputReference[] { new ThisReference(eelse) };
                }
            }
        }
    }
}

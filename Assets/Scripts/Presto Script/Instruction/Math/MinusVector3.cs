﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Math
{
    [Instruction("Math/Minus Vector3", false), Serializable]
    public class MinusVector3 : BaseInstruction
    {
        [Input("A")]
        public Vector3 a;

        [Input("B")]
        public Vector3 b;

        [Output("Result")]
        public Vector3 Result
        {
            get
            {
                return a - b;
            }
        }
    }
}
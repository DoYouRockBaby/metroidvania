﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Math
{
    [Instruction("Math/New Vector3", false), Serializable]
    public class NewVector3 : BaseInstruction
    {
        [Input("x")]
        public float x;

        [Input("y")]
        public float y;

        [Input("z")]
        public float z;

        [Output("Vector")]
        public Vector3 Vector
        {
            get
            {
                return new Vector3(x, y, z);
            }
        }
    }
}
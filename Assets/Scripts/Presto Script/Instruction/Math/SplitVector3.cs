﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Math
{
    [Instruction("Math/Split Vector3", false), Serializable]
    public class SplitVector3 : BaseInstruction
    {
        [Input("Vector")]
        public Vector3 vector;

        [Output("X")]
        public float X
        {
            get
            {
                return vector.x;
            }
        }

        [Output("Y")]
        public float Y
        {
            get
            {
                return vector.y;
            }
        }

        [Output("Z")]
        public float Z
        {
            get
            {
                return vector.z;
            }
        }
    }
}
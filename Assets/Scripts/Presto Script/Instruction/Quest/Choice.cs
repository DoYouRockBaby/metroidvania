﻿using Presto_Script.Core;
using UnityEngine;

namespace Presto_Script.Instruction.Quest
{
    [Instruction("Quest/Choice"), System.Serializable]
    public class Choice : BaseInstruction
    {
        [Input("Name")]
        public string speakerName;

        [Input("Text"), TextArea(5, 20)]
        public string text;

        [Input("Image")]
        public Sprite image;

        [Input("Option 1")]
        public string option1;

        [Input("Option 2")]
        public string option2;

        [NextInstruction("Next 1")]
        public IInstruction next1;

        [NextInstruction("Next 2")]
        public IInstruction next2;

        static ChoiceManager choiceManager = null;
        static ChoiceManager ChoiceManager
        {
            get
            {
                if (choiceManager == null)
                {
                    var foundObjects = Object.FindObjectsOfType<ChoiceManager>();

                    if (foundObjects.Length > 0)
                    {
                        choiceManager = foundObjects[0];
                    }
                }

                return choiceManager;
            }
        }

        public override void Action(ScriptExecuter exec)
        {
            if (ChoiceManager == null)
            {
                return;
            }

            ChoiceManager.PushChoice(new ChoiceRequest()
            {
                speakerName = speakerName,
                text = text,
                image = image,
                callback1 = () => exec.ExecuteInstruction(next1),
                callback2 = () => exec.ExecuteInstruction(next2),
                option1 = option1,
                option2 = option2,
            });
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[0];
    }
}
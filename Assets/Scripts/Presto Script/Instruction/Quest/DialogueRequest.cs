﻿using System;
using UnityEngine;

namespace Presto_Script.Instruction.Quest
{
    public class DialogueRequest : ScriptableObject
    {
        public string speakerName;
        public string text;
        public Sprite image;
        public Action callback;
    }
}
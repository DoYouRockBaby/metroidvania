﻿using Gameplay.Environment;
using Presto_Script.Core;

namespace Presto_Script.Instruction.Quest
{
    [Instruction("Quest/Run Trajectory"), System.Serializable]
    public class RunTrajectory : BaseInstruction
    {
        [Input("Trajectory")]
        public Trajectory trajectory;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            trajectory.enabled = true;
        }
    }
}
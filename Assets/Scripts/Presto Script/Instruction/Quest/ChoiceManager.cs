﻿using Presto_Script.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Presto_Script.Instruction.Quest
{
    [UnFreezable]
    public class ChoiceManager : MonoBehaviour
    {
        public UnityEngine.GameObject choicePanel;
        public Text speakerNameText;
        public Text textBox;
        public Image image;
        public Button option1Button;
        public Button option2Button;
        public Text option1Text;
        public Text option2Text;
        public float displayTime = 0.2f;

        Queue<ChoiceRequest> _choices = new Queue<ChoiceRequest>();
        bool _enabled = false;
        Animator _animator = null;

        Action _nextCallback1 = null;
        Action _nextCallback2 = null;

        private void Awake()
        {
            _animator = choicePanel.GetComponent<Animator>();
            option1Button.onClick.AddListener(TaskOnClick1);
            option2Button.onClick.AddListener(TaskOnClick2);
        }

        public void PushChoice(ChoiceRequest choice)
        {
            _choices.Enqueue(choice);
            ContinueChoice();
        }

        void TaskOnClick1()
        {
            if (_enabled)
            {
                _enabled = false;
                PauseUtility.Freezed = false;
                _animator.SetBool("Hidden", true);
                _nextCallback1();
            }
        }

        void TaskOnClick2()
        {
            if (_enabled)
            {
                _enabled = false;
                PauseUtility.Freezed = false;
                _animator.SetBool("Hidden", true);
                _nextCallback2();
            }
        }

        public void ContinueChoice()
        {
            var choice = _choices.Dequeue();

            textBox.text = "";
            speakerNameText.text = choice.speakerName;
            image.sprite = choice.image;
            _nextCallback1 = choice.callback1;
            _nextCallback2 = choice.callback2;

            option1Text.text = choice.option1;
            option2Text.text = choice.option2;

            if (!_enabled)
            {
                PauseUtility.Freezed = false;
            }

            _enabled = true;
            PauseUtility.Freezed = true;

            _animator.SetBool("Hidden", false);

            if (displayTime <= 0f)
            {
                textBox.text = choice.text;
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(TypeText(choice.text));
            }
        }

        IEnumerator<WaitForSecondsRealtime> TypeText(string text)
        {
            var timePerChar = displayTime / text.ToCharArray().Length;

            foreach (var character in text.ToCharArray())
            {
                textBox.text += character;
                yield return new WaitForSecondsRealtime(timePerChar);
            }
        }
    }
}
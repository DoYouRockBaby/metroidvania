﻿using Presto_Script.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Presto_Script.Instruction.Quest
{
    [UnFreezable]
    public class DialogueManager : MonoBehaviour
    {
        public UnityEngine.GameObject dialogPanel;
        public Text speakerNameText;
        public Text textBox;
        public Image image;
        public float displayTime = 1f;

        Queue<DialogueRequest> _dialogues = new Queue<DialogueRequest>();
        bool _enabled = false;
        Animator _animator = null;
        Action _nextCallback = null;
        bool _dialogOver = false;

        private void Awake()
        {
            _animator = dialogPanel.GetComponent<Animator>();
        }

        public void PushDialogue(DialogueRequest dialogue)
        {
            _dialogues.Enqueue(dialogue);
            ContinueDialogue();
        }

        void Update()
        {
            if (_enabled)
            {
                if (UnityEngine.Input.GetButtonDown("Enter") && _dialogOver)
                {
                    if(_dialogues.Count > 0)
                    {
                        ContinueDialogue();
                    }
                    else
                    {

                        _enabled = false;
                        PauseUtility.Freezed = false;
                        _animator.SetBool("Hidden", true);
                        _nextCallback();
                    }
                }
            }
        }

        public void ContinueDialogue()
        {
            var dialogue = _dialogues.Dequeue();

            textBox.text = "";
            speakerNameText.text = dialogue.speakerName;
            image.sprite = dialogue.image;
            _dialogOver = false;
            _nextCallback = dialogue.callback;

            _enabled = true;
            PauseUtility.Freezed = true;

            _animator.SetBool("Hidden", false);

            if (displayTime <= 0f)
            {
                textBox.text = dialogue.text;
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(TypeText(dialogue.text));
            }
        }

        IEnumerator<WaitForSecondsRealtime> TypeText(string text)
        {
            var timePerChar = displayTime / text.ToCharArray().Length;

            foreach (var character in text.ToCharArray())
            {
                textBox.text += character;
                yield return new WaitForSecondsRealtime(timePerChar);
            }

            _dialogOver = true;
        }
    }
}
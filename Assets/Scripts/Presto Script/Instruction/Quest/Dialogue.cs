﻿using Presto_Script.Core;
using UnityEngine;

namespace Presto_Script.Instruction.Quest
{
    [Instruction("Quest/Dialogue"), System.Serializable]
    public class DialogueBloc : BaseInstruction
    {
        [Input("Name")]
        public string speakerName;

        [Input("Text"), TextArea(5, 20)]
        public string text;

        [Input("Image")]
        public Sprite image;

        [NextInstruction("Next")]
        public IInstruction next;

        static DialogueManager dialogueManager = null;
        static DialogueManager DialogueManager
        {
            get
            {
                if (dialogueManager == null)
                {
                    var foundObjects = FindObjectsOfType<DialogueManager>();

                    if (foundObjects.Length > 0)
                    {
                        dialogueManager = foundObjects[0];
                    }
                }

                return dialogueManager;
            }
        }

        public override void Action(ScriptExecuter exec)
        {
            if (DialogueManager == null)
            {
                return;
            }

            var dialog = new DialogueRequest
            {
                speakerName = speakerName,
                text = text,
                image = image,
                callback = () => exec.ExecuteInstruction(next)
            };

            DialogueManager.PushDialogue(dialog);
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[0];
    }
}
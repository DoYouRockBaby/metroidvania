﻿using System;
using UnityEngine;

namespace Presto_Script.Instruction.Quest
{
    public class ChoiceRequest : ScriptableObject
    {
        public string speakerName;
        public string text;
        public Sprite image;

        public string option1;
        public string option2;
        public Action callback1;
        public Action callback2;
    }
}
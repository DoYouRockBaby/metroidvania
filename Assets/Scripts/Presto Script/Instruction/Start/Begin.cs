﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Start
{
    [Instruction("Start/Begin", false), Serializable]
    public class Begin : BaseInstruction
    {
        [NextInstruction("Next")]
        public IInstruction next;
    }
}

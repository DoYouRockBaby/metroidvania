﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Start
{
    [Instruction("Start/Trigger", false), Serializable]
    public class Trigger : Begin
    {
        public enum TriggerType
        {
            Enter,
            Stay,
            Exit
        }

        [Input("Type")]
        public TriggerType type;

        [Input("Mask")]
        public LayerMask mask;

        public override void Action(ScriptExecuter exec)
        {
            switch (type)
            {
                case TriggerType.Enter:
                    exec.TriggerEntered += (collider) =>
                    {
                        if (mask == (mask | (1 << collider.gameObject.layer)))
                        {
                            var test = this;
                            exec.ExecuteInstruction(next);
                        }
                    };

                    break;
                case TriggerType.Stay:
                    exec.TriggerStayed += (collider) =>
                    {
                        if (mask == (mask | (1 << collider.gameObject.layer)))
                        {
                            exec.ExecuteInstruction(next);
                        }
                    };

                    break;
                case TriggerType.Exit:
                    exec.TriggerExited += (collider) =>
                    {
                        if (mask == (mask | (1 << collider.gameObject.layer)))
                        {
                            exec.ExecuteInstruction(next);
                        }
                    };

                    break;
            }
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[0];
    }
}
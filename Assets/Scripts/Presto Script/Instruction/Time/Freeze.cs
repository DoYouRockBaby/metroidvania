﻿using Presto_Script.Core;
using Presto_Script.Utility;
using System;

namespace Presto_Script.Instruction.Time
{
    [Instruction("Time/Freeze"), Serializable]
    public class Freeze : BaseInstruction
    {
        [Input("Defreeze")]
        public bool defreeze = false;

        [NextInstruction("Next")]
        public IInstruction next;

        float _previousTimeScale = 1f;

        public override void Action(ScriptExecuter exec)
        {
            PauseUtility.Freezed = !defreeze;
        }
    }
}
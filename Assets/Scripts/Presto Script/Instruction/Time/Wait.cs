﻿using Presto_Script.Core;
using System;
using System.Collections;
using UnityEngine;

namespace Presto_Script.Instruction.Time
{
    [Instruction("Time/Wait"), Serializable]
    public class Wait : BaseInstruction
    {
        [Input("Seconds")]
        public float seconds;

        [NextInstruction("Next")]
        public IInstruction next;

        public override void Action(ScriptExecuter exec)
        {
            exec.GameObject.GetComponent<MonoBehaviour>().StartCoroutine(Coroutine(exec));
        }

        public IEnumerator Coroutine(ScriptExecuter exec)
        {
            yield return new WaitForSeconds(seconds);
            exec.ExecuteInstruction(next);
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[0];
    }
}
﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Function
{
    public abstract class Output : BaseInstruction
    {
        public abstract Type Type { get; }

        [Input("Name")]
        public string outputName;
    }

    public abstract class Output<T> : Output
    {
        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        [Input("Value")]
        public T Value
        {
            get;
            set;
        }
    }

    [Instruction("Output/Vector 3", false, true), Serializable]
    public class Vector3Output : Output<Vector3>
    {
    }

    [Instruction("Output/Game Object", false, true), Serializable]
    public class GameObjectOutput : Output<UnityEngine.GameObject>
    {
    }

    [Instruction("Output/Float", false, true), Serializable]
    public class DoubleOutput : Output<Double>
    {
    }

    [Instruction("Output/Int", false, true), Serializable]
    public class IntOutput : Output<Int32>
    {
    }
}
﻿using Presto_Script.Core;
using System;
using UnityEngine;

namespace Presto_Script.Instruction.Function
{
    public abstract class Input : BaseInstruction
    {
        public abstract Type Type { get; }

        [Input("Name")]
        public string inputName;
    }

    public abstract class Input<T> : Input
    {
        public override Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        [Output("Value")]
        public T Value
        {
            get;
            set;
        }
    }

    [Instruction("Input/Vector 3", false, true), Serializable]
    public class Vector3Input : Input<Vector3>
    {
    }

    [Instruction("Input/Game Object", false, true), Serializable]
    public class GameObjectInput : Input<UnityEngine.GameObject>
    {
    }

    [Instruction("Input/Float", false, true), Serializable]
    public class DoubleInput : Input<Double>
    {
    }

    [Instruction("Input/Int", false, true), Serializable]
    public class IntInput : Input<Int32>
    {
    }
}
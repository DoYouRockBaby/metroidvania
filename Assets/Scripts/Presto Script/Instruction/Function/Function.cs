﻿using Presto_Script.Core;
using System;
using System.Collections.Generic;

namespace Presto_Script.Instruction.Function
{
    [Instruction("Function/Function"), Serializable]
    public class Function : BaseInstruction
    {
        [Input("Script")]
        public Script script;
        private Script previousScript;

        IInputReference scriptInput = null;

        private IInputReference[] inputs;
        public override IInputReference[] Inputs
        {
            get
            {
                CheckFunctionChange();

                if(scriptInput == null)
                {
                    scriptInput = new InputMemberReference(this, "script", "Script", typeof(Script));
                }

                if (inputs == null)
                {
                    if (script == null)
                    {
                        inputs = new IInputReference[] { scriptInput };
                    }
                    else
                    {
                        var inputList = new List<IInputReference>
                        {
                            scriptInput
                        };

                        foreach (var instruction in script.instructions)
                        {
                            if (instruction == null)
                            {
                                continue;
                            }

                            if (instruction.GetType().IsInstanceOfType(typeof(Input)) || instruction.GetType() == typeof(Input))
                            {
                                var inputInstruction = instruction as Input;
                                inputList.Add(new InputMemberReference(inputInstruction, "next", inputInstruction.inputName, typeof(IInstruction)));
                            }
                        }

                        inputs = inputList.ToArray();
                    }
                }

                return inputs;
            }
        }

        private IOutputReference[] outputs;
        public override IOutputReference[] Outputs
        {
            get
            {

                CheckFunctionChange();

                if (outputs == null)
                {
                    if (script == null)
                    {
                        return new IOutputReference[0];
                    }
                    else
                    {
                        var outputList = new List<IOutputReference>();

                        foreach (var instruction in script.instructions)
                        {
                            if (instruction == null)
                            {
                                continue;
                            }

                            if (instruction.GetType().IsInstanceOfType(typeof(Output)) || instruction.GetType() == typeof(Output))
                            {
                                var outputInstruction = instruction as Output;
                                outputList.Add(new OutputMemberReference(outputInstruction, "next", outputInstruction.outputName, typeof(IInstruction)));
                            }
                        }

                        outputs = outputList.ToArray();
                    }
                }

                return outputs;
            }
        }

        private IInputReference[] nextInstructions = null;
        public override IInputReference[] NextInstructions
        {
            get
            {
                CheckFunctionChange();

                if (nextInstructions == null)
                {
                    if (script == null)
                    {
                        nextInstructions = new IInputReference[0];
                    }
                    else
                    {
                        var nextInstructionList = new List<IInputReference>();

                        foreach (var instruction in script.instructions)
                        {
                            if (instruction == null)
                            {
                                continue;
                            }

                            if (instruction.GetType().IsInstanceOfType(typeof(Continue)) || instruction.GetType() == typeof(Continue))
                            {
                                var continueInstruction = instruction as Continue;
                                nextInstructionList.Add(new ContinueReference(continueInstruction));
                            }
                        }

                        nextInstructions = nextInstructionList.ToArray();
                    }
                }

                return nextInstructions;
            }
        }

        public void CheckFunctionChange()
        {
            if(script == null
                || (previousScript != null
                && script.GetHashCode() != previousScript.GetHashCode()))
            {
                nextInstructions = null;
            }

            previousScript = script;
        }

        public override void Action(ScriptExecuter exec)
        {
            //Inject the next blocs into its continue blocs
            /*foreach (var instruction in script.Instructions)
            {
                if (instruction == null)
                {
                    continue;
                }

                if (instruction.GetType().IsSubclassOf(typeof(Continue)) || instruction.GetType() == typeof(Continue))
                {
                    (instruction as Continue).next = this;
                }
            }*/

            //Execute function
            exec.Execute(script);
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[0];
    }
}

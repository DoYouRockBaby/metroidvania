﻿using Presto_Script.Core;
using System;
using UnityEditor;

namespace Presto_Script.Instruction.Function
{
    public class ContinueReference : IInputReference
    {
        private Continue continueInstruction;

        public ContinueReference(Continue continueInstruction)
        {
            this.continueInstruction = continueInstruction;
        }

        override public string Name => continueInstruction.continueName;

        override public object Value
        {
            get
            {
                return continueInstruction.next;
            }
            set
            {
                continueInstruction.next = value as IInstruction;
            }
        }

        override public Type Type => typeof(IInstruction);

        public override IOutputReference OutputPipe
        {
            get;
            set;
        }

#if UNITY_EDITOR
        private SerializedObject serializedObject = null;
        private SerializedObject SerializedObject
        {
            get
            {
                return null;
            }
        }

        override public SerializedProperty SerializedProperty
        {
            get
            {
                return null;
            }
        }

        override public void ApplySerializedPropertyModifications()
        {
        }
#endif
    }

    /*public class OutputMemberReference : IOutputReference
    {
        private BaseInstruction instruction;
        private string propertyName;

        public OutputMemberReference(BaseInstruction instruction, string propertyName, string label, Type type)
        {
            this.instruction = instruction;
            this.propertyName = propertyName;
            Name = label;
            Type = type;
        }

        override public string Name { get; }

        override public object Value
        {
            get
            {
                if (instruction.GetType().GetProperty(propertyName) != null)
                {
                    return instruction.GetType().GetProperty(propertyName).GetValue(instruction, null);
                }
                else
                {
                    return instruction.GetType().GetField(propertyName).GetValue(instruction);
                }
            }
        }

        override public Type Type { get; }

        public override IInstruction[] Dependencies => new IInstruction[] { instruction };
    }

    public class ThisReference : IOutputReference
    {
        private IInstruction instruction;

        public ThisReference(IInstruction instruction)
        {
            this.instruction = instruction;
        }

        override public string Name => "self";

        override public object Value
        {
            get
            {
                return instruction;
            }
        }

        override public Type Type => typeof(IInstruction);

        public override IInstruction[] Dependencies => new IInstruction[0];
    }*/
}

﻿using Presto_Script.Core;
using System;

namespace Presto_Script.Instruction.Function
{
    [Instruction("Function/Continue"), Serializable]
    public class Continue : BaseInstruction
    {
        public IInstruction next;

        [Input("Name")]
        public string continueName;

        public override void Action(ScriptExecuter exec)
        {
            base.Action(exec);
        }

        public override IOutputReference[] NextExecutableInstructions => new IOutputReference[] { new OutputMemberReference(this, continueName, continueName, typeof(IInstruction)) };
    }
}
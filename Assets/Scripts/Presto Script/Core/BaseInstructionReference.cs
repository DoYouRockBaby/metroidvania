﻿using System;
using UnityEditor;

namespace Presto_Script.Core
{
    public class InputMemberReference : IInputReference
    {
        private UnityEngine.Object instance;
        private string propertyName;

        public InputMemberReference(UnityEngine.Object instance, string propertyName, string label, Type type)
        {
            this.instance = instance;
            this.propertyName = propertyName;
            Name = label;
            Type = type;
        }

        override public string Name { get; }

        override public object Value
        {
            get
            {
                if (instance.GetType().GetProperty(propertyName) != null)
                {
                    return instance.GetType().GetProperty(propertyName).GetValue(instance);
                }
                else
                {
                    return instance.GetType().GetField(propertyName).GetValue(instance);
                }
            }
            set
            {
                if (instance.GetType().GetProperty(propertyName) != null)
                {
                    instance.GetType().GetProperty(propertyName).SetValue(instance, value);
                }
                else
                {
                    instance.GetType().GetField(propertyName).SetValue(instance, value);
                }
            }
        }

        override public Type Type { get; }

        public override IOutputReference OutputPipe
        {
            get;
            set;
        }

#if UNITY_EDITOR
        private SerializedObject serializedObject = null;
        private SerializedObject SerializedObject
        {
            get
            {
                if (serializedObject == null)
                {
                    serializedObject = new SerializedObject(instance);
                }

                return serializedObject;
            }
        }

        override public SerializedProperty SerializedProperty
        {
            get
            {
                return SerializedObject.FindProperty(propertyName);
            }
        }

        override public void ApplySerializedPropertyModifications()
        {
            SerializedObject.ApplyModifiedProperties();
        }
#endif
    }

    public class OutputMemberReference : IOutputReference
    {
        private BaseInstruction instruction;
        private string propertyName;

        public OutputMemberReference(BaseInstruction instruction, string propertyName, string label, Type type)
        {
            this.instruction = instruction;
            this.propertyName = propertyName;
            Name = label;
            Type = type;
        }

        override public string Name { get; }

        override public object Value
        {
            get
            {
                if (instruction.GetType().GetProperty(propertyName) != null)
                {
                    return instruction.GetType().GetProperty(propertyName).GetValue(instruction, null);
                }
                else
                {
                    return instruction.GetType().GetField(propertyName).GetValue(instruction);
                }
            }
        }

        override public Type Type { get; }

        public override IInstruction[] Dependencies => new IInstruction[] { instruction };
    }

    public class ThisReference : IOutputReference
    {
        private IInstruction instruction;

        public ThisReference(IInstruction instruction)
        {
            this.instruction = instruction;
        }

        override public string Name => "self";

        override public object Value
        {
            get
            {
                return instruction;
            }
        }

        override public Type Type => typeof(IInstruction);

        public override IInstruction[] Dependencies => new IInstruction[0];
    }
}

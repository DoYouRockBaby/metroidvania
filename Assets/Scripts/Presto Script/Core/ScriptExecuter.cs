﻿using Presto_Script.Instruction.Start;
using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Presto_Script.Core
{
    public class ScriptExecuter
    {
        public GameObject GameObject { get; }

        public delegate void TriggerDelegate(Collider2D collider);

        public event TriggerDelegate TriggerEntered;
        public event TriggerDelegate TriggerStayed;
        public event TriggerDelegate TriggerExited;

        public static ScriptExecuter Last { get; private set; }

        public ScriptExecuter(GameObject gameObject)
        {
            GameObject = gameObject;
        }

        public void Execute(Script script)
        {
            foreach (var instruction in script.instructions)
            {
                if(instruction == null)
                {
                    continue;
                }

                if (instruction.GetType().IsSubclassOf(typeof(Begin)) || instruction.GetType() == typeof(Begin))
                {
                    ExecuteInstruction(instruction);
                }
            }
        }

        public void ExecuteInstruction(IInstruction instruction)
        {
            if(instruction == null)
            {
                return;
            }

            Last = this;

            GrabInputs(instruction);
            instruction.Action(this);
            ExecuteNextInstructions(instruction);
        }

        public void ExecuteNextInstructions(IInstruction instruction)
        {
            foreach (var next in instruction.NextExecutableInstructions)
            {
                if (next.Value != null)
                {
                    ExecuteInstruction(next.Value as IInstruction);
                }
            }
        }

        private void GrabInputs(IInstruction instruction)
        {
            foreach(var input in instruction.Inputs)
            {
                if(input.OutputPipe != null)
                {
                    if(input.Type != input.OutputPipe.Type)
                    {
                        throw new TypesDontMatchException();
                    }

                    foreach(var dependency in input.OutputPipe.Dependencies)
                    {
                        GrabInputs(dependency);
                    }

                    input.Value = input.OutputPipe.Value;
                }
            }

            foreach (var input in instruction.NextInstructions)
            {
                if (input.OutputPipe != null)
                {
                    if (input.Type != input.OutputPipe.Type)
                    {
                        throw new TypesDontMatchException();
                    }

                    input.Value = input.OutputPipe.Value;
                }
            }
        }

        public void InvokeTriggerEntered(Collider2D collider)
        {
            TriggerEntered?.Invoke(collider);
        }

        public void InvokeTriggerStayed(Collider2D collider)
        {
            TriggerStayed?.Invoke(collider);
        }

        public void InvokeTriggerExited(Collider2D collider)
        {
            TriggerExited?.Invoke(collider);
        }
    }

    [Serializable]
    internal class TypesDontMatchException : Exception
    {
        public TypesDontMatchException()
        {
        }

        public TypesDontMatchException(string message) : base(message)
        {
        }

        public TypesDontMatchException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TypesDontMatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Presto_Script.Core
{
    public static class InstructionStore
    {
        public struct AvailableInstruction
        {
            public Type Type { get; set; }
            public string Label { get; set; }
            public bool InstantiableInEditor { get; set; }
        }

        static AvailableInstruction[] instructions = null;
        public static AvailableInstruction[] Instructions
        {
            get
            {
                if(instructions == null)
                {
                    var instructionList = new List<AvailableInstruction>();

                    var asmbly = Assembly.GetExecutingAssembly();
                    var typeList = asmbly.GetTypes().Where(x => x.GetCustomAttributes(typeof(InstructionAttribute), true).Any()).ToList();

                    foreach (var type in typeList)
                    {
                        var attr = type.GetCustomAttributes(typeof(InstructionAttribute), true)[0] as InstructionAttribute;
                        instructionList.Add(new AvailableInstruction()
                        {
                            Type = type,
                            Label = attr.Label,
                            InstantiableInEditor = attr.InstantiableInEditor,
                        });
                    }

                    instructions = instructionList.ToArray();
                }

                return instructions;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Presto_Script.Core
{
    public class BaseInstruction : IInstruction
    {
        //Position, used by the graph editor
        public Vector2 gridPosition = new Vector2();
        override public Vector2 GridPosition
        {
            get
            {
                return gridPosition;
            }
            set
            {
                gridPosition = value;
            }
        }

        private IOutputReference tthis = null;
        override public IOutputReference This
        {
            get
            {
                if(tthis == null)
                {
                    tthis = new ThisReference(this);
                }

                return tthis;
            }
        }

        private IInputReference[] inputs = null;
        override public IInputReference[] Inputs
        {
            get
            {
                if(inputs == null)
                {

                    var inputFields = GetType().GetFields().Where(x => x.GetCustomAttributes(typeof(InputAttribute), true).Any());
                    var inputs = new List<IInputReference>();

                    foreach (var field in inputFields)
                    {
                        var attribute = field.GetCustomAttributes(typeof(InputAttribute), true)[0] as InputAttribute;
                        inputs.Add(new InputMemberReference(this, field.Name, attribute.Label, field.FieldType));
                    }

                    this.inputs = inputs.ToArray();
                }

                return inputs;
            }
        }

        private IOutputReference[] outputs = null;
        override public IOutputReference[] Outputs
        {
            get
            {

                if (outputs == null)
                {
                    var outputFields = GetType().GetProperties().Where(x => x.GetCustomAttributes(typeof(OutputAttribute), true).Any());
                    var outputs = new List<IOutputReference>();

                    foreach (var field in outputFields)
                    {
                        var attribute = field.GetCustomAttributes(typeof(OutputAttribute), true)[0] as OutputAttribute;
                        outputs.Add(new OutputMemberReference(this, field.Name, attribute.Label, field.PropertyType));
                    }

                    this.outputs = outputs.ToArray();
                }

                return outputs;
            }
        }

        private IInputReference[] nextInstructions = null;
        override public IInputReference[] NextInstructions
        {
            get
            {
                if (nextInstructions == null)
                {

                    var nextInstructionFields = GetType().GetFields().Where(x => x.GetCustomAttributes(typeof(NextInstructionAttribute), true).Any());
                    var nextInstructions = new List<IInputReference>();

                    foreach (var field in nextInstructionFields)
                    {
                        var attribute = field.GetCustomAttributes(typeof(NextInstructionAttribute), true)[0] as NextInstructionAttribute;
                        nextInstructions.Add(new InputMemberReference(this, field.Name, attribute.Label, field.FieldType));
                    }

                    this.nextInstructions = nextInstructions.ToArray();
                }

                return nextInstructions;
            }
        }

        override public IOutputReference[] NextExecutableInstructions
        {
            get
            {
                var nextInstructionFields = GetType().GetFields().Where(x => x.GetCustomAttributes(typeof(NextInstructionAttribute), true).Any());
                var nextInstructions = new List<IOutputReference>();

                foreach (var field in nextInstructionFields)
                {
                    var attribute = field.GetCustomAttributes(typeof(NextInstructionAttribute), true)[0] as NextInstructionAttribute;
                    nextInstructions.Add(new OutputMemberReference(this, field.Name, attribute.Label, field.FieldType));
                }

                return nextInstructions.ToArray();
            }
        }

        override public string Name => Attribute.Label;
        override public bool CanBeReferenced => Attribute.CanBeReferenced;

        private InstructionAttribute attribute = null;
        private InstructionAttribute Attribute
        {
            get
            {
                if (attribute == null)
                {
                    attribute = GetType().GetCustomAttributes(typeof(InstructionAttribute), true)[0] as InstructionAttribute;
                }

                return attribute;
            }
        }

        //Action of the bloc, intend to be overrided
        override public void Action(ScriptExecuter exec)
        {

        }
    }
}

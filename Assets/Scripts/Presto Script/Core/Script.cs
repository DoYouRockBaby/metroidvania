﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Presto_Script.Core
{
    /*[CreateAssetMenu(fileName = "My Script", menuName = "Presto Script/Script")]
    public class Script : ScriptableObject
    {
        public List<IInstruction> Instructions = new List<IInstruction>();

        public void Reload()
        {
        }

        public void Save()
        {
        }
    }*/

    /*[CreateAssetMenu(fileName = "My Script", menuName = "Presto Script/Script")]
    public class Script : ScriptableObject
    {
        [Serializable]
        public struct SerializedInstruction
        {
            public string type;
            public string datas;
        }

        [Serializable]
        public struct SerializedPipes
        {
            public int outputInstruction;
            public int outputMember;
            public int inputInstruction;
            public int inputMember;
        }

        [Serializable]
        public struct SerializedInstructionPipes
        {
            public int outputInstruction;
            public int inputInstruction;
            public int nextInstructionMember;
        }

        [Serializable]
        public struct SerializedAssetReference
        {
            public int instruction;
            public int member;
            public string guid;
            public long localId;
        }

        public SerializedInstruction[] instructionDatas = new SerializedInstruction[0];
        public SerializedPipes[] pipeDatas = new SerializedPipes[0];
        public SerializedInstructionPipes[] pipeInstructionDatas = new SerializedInstructionPipes[0];
        public SerializedAssetReference[] assetReferenceDatas = new SerializedAssetReference[0];

        List<IInstruction> instructions = null;
        public List<IInstruction> Instructions
        {
            get
            {
                if (instructions != null)
                {
                    instructions.RemoveAll((e) => e == null);
                }

                if (instructions == null || instructions.Count == 0)
                {
                    instructions = new List<IInstruction>();

                    foreach (var instructionData in instructionDatas)
                    {
                        var type = Type.GetType(instructionData.type);
                        var instruction = CreateInstance(type) as IInstruction;
                        JsonUtility.FromJsonOverwrite(instructionData.datas, instruction);

                        if (instruction != null)
                        {
                            instructions.Add(instruction);
                        }
                    }

                    foreach (var assetReferenceData in assetReferenceDatas)
                    {
#if UNITY_EDITOR
                        var instruction = instructions[assetReferenceData.instruction];
                        var inputMember = instruction.Inputs[assetReferenceData.member];

                        var path = AssetDatabase.GUIDToAssetPath(assetReferenceData.guid);
                        var obj = AssetDatabase.LoadAssetAtPath(path, instruction.Inputs[assetReferenceData.member].Type);

                        inputMember.Value = obj;
#endif
                    }

                    foreach (var pipeData in pipeDatas)
                    {
                        var outputInstruction = instructions[pipeData.outputInstruction];
                        var inputInstruction = instructions[pipeData.inputInstruction];

                        var outputMember = outputInstruction.Outputs[pipeData.outputMember];
                        var inputMember = inputInstruction.Inputs[pipeData.inputMember];

                        inputMember.OutputPipe = outputMember;
                    }

                    foreach (var pipeData in pipeInstructionDatas)
                    {
                        var outputInstruction = instructions[pipeData.outputInstruction];
                        var inputInstruction = instructions[pipeData.inputInstruction];

                        var outputMember = outputInstruction.This;
                        var inputMember = inputInstruction.NextInstructions[pipeData.nextInstructionMember];

                        inputMember.OutputPipe = outputMember;
                    }
                }

                return instructions;
            }
        }

        public void Reload()
        {
            instructions = null;
        }

        public void Save()
        {
            var instructionDataList = new List<SerializedInstruction>();
            var pipeDataList = new List<SerializedPipes>();
            var pipeInstructionDataList = new List<SerializedInstructionPipes>();
            var assetReferenceDataList = new List<SerializedAssetReference>();

            foreach (var instruction in instructions)
            {
                instructionDataList.Add(new SerializedInstruction()
                {
                    datas = JsonUtility.ToJson(instruction),
                    type = instruction.GetType().AssemblyQualifiedName
                });

                foreach (var input in instruction.Inputs)
                {
                    if (input.OutputPipe != null)
                    {
                        var outputInstruction = instructions.Where((e) => e.Outputs.Contains(input.OutputPipe)).FirstOrDefault();

                        if (outputInstruction == null)
                        {
                            continue;
                        }

                        pipeDataList.Add(new SerializedPipes()
                        {
                            inputInstruction = instructions.IndexOf(instruction),
                            inputMember = Array.IndexOf(instruction.Inputs, input),
                            outputInstruction = instructions.IndexOf(outputInstruction),
                            outputMember = Array.IndexOf(outputInstruction.Outputs, input.OutputPipe),
                        });
                    }
                    else if (input.Type.IsSubclassOf(typeof(UnityEngine.Object)) && input.Value != null)
                    {
#if UNITY_EDITOR
                        string guid;
                        long localId;

                        if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(input.Value as UnityEngine.Object, out guid, out localId))
                        {
                            assetReferenceDataList.Add(new SerializedAssetReference()
                            {
                                instruction = instructions.IndexOf(instruction),
                                member = Array.IndexOf(instruction.Inputs, input),
                                guid = guid,
                                localId = localId
                            });
                        }
#endif
                    }
                }

                foreach (var nextInstruction in instruction.NextInstructions)
                {
                    if (nextInstruction.OutputPipe != null)
                    {
                        var outputInstruction = instructions.Where((e) => e.This == nextInstruction.OutputPipe).FirstOrDefault();

                        if (outputInstruction == null)
                        {
                            continue;
                        }

                        pipeInstructionDataList.Add(new SerializedInstructionPipes()
                        {
                            inputInstruction = instructions.IndexOf(instruction),
                            nextInstructionMember = Array.IndexOf(instruction.NextInstructions, nextInstruction),
                            outputInstruction = instructions.IndexOf(outputInstruction),
                        });
                    }
                }
            }

            instructionDatas = instructionDataList.ToArray();
            pipeDatas = pipeDataList.ToArray();
            pipeInstructionDatas = pipeInstructionDataList.ToArray();
            assetReferenceDatas = assetReferenceDataList.ToArray();
        }
    }*/

    [CreateAssetMenu(fileName = "My Script", menuName = "Presto Script/Script")]
    public class Script : ScriptableObject, ISerializationCallbackReceiver
    {
        [Serializable]
        public struct SerializedInstruction
        {
            public string type;
            public string datas;
        }

        [Serializable]
        public struct SerializedPipes
        {
            public int outputInstruction;
            public int outputMember;
            public int inputInstruction;
            public int inputMember;
        }

        [Serializable]
        public struct SerializedInstructionPipes
        {
            public int outputInstruction;
            public int inputInstruction;
            public int nextInstructionMember;
        }

        [Serializable]
        public struct SerializedAssetReference
        {
            public int instruction;
            public int member;
            public string guid;
            public long localId;
        }

        public SerializedInstruction[] instructionDatas = new SerializedInstruction[0];
        public SerializedPipes[] pipeDatas = new SerializedPipes[0];
        public SerializedInstructionPipes[] pipeInstructionDatas = new SerializedInstructionPipes[0];
        public SerializedAssetReference[] assetReferenceDatas = new SerializedAssetReference[0];

        public List<IInstruction> instructions = null;

        public void OnBeforeSerialize()
        {
            var instructionDataList = new List<SerializedInstruction>();
            var pipeDataList = new List<SerializedPipes>();
            var pipeInstructionDataList = new List<SerializedInstructionPipes>();
            var assetReferenceDataList = new List<SerializedAssetReference>();

            foreach (var instruction in instructions)
            {
                if(instruction == null)
                {
                    continue;
                }

                instructionDataList.Add(new SerializedInstruction()
                {
                    datas = JsonUtility.ToJson(instruction),
                    type = instruction.GetType().AssemblyQualifiedName
                });

                foreach (var input in instruction.Inputs)
                {
                    if (input.OutputPipe != null)
                    {
                        var outputInstruction = instructions.Where((e) => e.Outputs.Contains(input.OutputPipe)).FirstOrDefault();

                        if (outputInstruction == null)
                        {
                            continue;
                        }

                        pipeDataList.Add(new SerializedPipes()
                        {
                            inputInstruction = instructions.IndexOf(instruction),
                            inputMember = Array.IndexOf(instruction.Inputs, input),
                            outputInstruction = instructions.IndexOf(outputInstruction),
                            outputMember = Array.IndexOf(outputInstruction.Outputs, input.OutputPipe),
                        });
                    }
                    else if (input.Type.IsSubclassOf(typeof(UnityEngine.Object)) && input.Value != null)
                    {
#if UNITY_EDITOR
                        string guid;
                        long localId;

                        if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(input.Value as UnityEngine.Object, out guid, out localId))
                        {
                            assetReferenceDataList.Add(new SerializedAssetReference()
                            {
                                instruction = instructions.IndexOf(instruction),
                                member = Array.IndexOf(instruction.Inputs, input),
                                guid = guid,
                                localId = localId
                            });
                        }
#endif
                    }
                }

                foreach (var nextInstruction in instruction.NextInstructions)
                {
                    if (nextInstruction.OutputPipe != null)
                    {
                        var outputInstruction = instructions.Where((e) => e.This == nextInstruction.OutputPipe).FirstOrDefault();

                        if (outputInstruction == null)
                        {
                            continue;
                        }

                        pipeInstructionDataList.Add(new SerializedInstructionPipes()
                        {
                            inputInstruction = instructions.IndexOf(instruction),
                            nextInstructionMember = Array.IndexOf(instruction.NextInstructions, nextInstruction),
                            outputInstruction = instructions.IndexOf(outputInstruction),
                        });
                    }
                }
            }

            instructionDatas = instructionDataList.ToArray();
            pipeDatas = pipeDataList.ToArray();
            pipeInstructionDatas = pipeInstructionDataList.ToArray();
            assetReferenceDatas = assetReferenceDataList.ToArray();
        }

        public void OnEnable()
        {
            if (instructions != null)
            {
                instructions.RemoveAll((e) => e == null);
            }

            if (instructions == null || instructions.Count == 0)
            {
                instructions = new List<IInstruction>();

                foreach (var instructionData in instructionDatas)
                {
                    var type = Type.GetType(instructionData.type);
                    var instruction = CreateInstance(type) as IInstruction;
                    JsonUtility.FromJsonOverwrite(instructionData.datas, instruction);

                    instructions.Add(instruction);
                }

                foreach (var assetReferenceData in assetReferenceDatas)
                {
#if UNITY_EDITOR
                    var instruction = instructions[assetReferenceData.instruction];
                    var inputMember = instruction.Inputs[assetReferenceData.member];

                    var path = AssetDatabase.GUIDToAssetPath(assetReferenceData.guid);
                    var obj = AssetDatabase.LoadAssetAtPath(path, instruction.Inputs[assetReferenceData.member].Type);

                    inputMember.Value = obj;
#endif
                }

                foreach (var pipeData in pipeDatas)
                {
                    var outputInstruction = instructions[pipeData.outputInstruction];
                    var inputInstruction = instructions[pipeData.inputInstruction];

                    var outputMember = outputInstruction.Outputs[pipeData.outputMember];
                    var inputMember = inputInstruction.Inputs[pipeData.inputMember];

                    inputMember.OutputPipe = outputMember;
                }

                foreach (var pipeData in pipeInstructionDatas)
                {
                    if(pipeData.outputInstruction >= instructions.Count || pipeData.inputInstruction >= instructions.Count)
                    {
                        continue;
                    }

                    var outputInstruction = instructions[pipeData.outputInstruction];
                    var inputInstruction = instructions[pipeData.inputInstruction];

                    if (pipeData.nextInstructionMember >= inputInstruction.NextInstructions.Length)
                    {
                        continue;
                    }

                    var outputMember = outputInstruction.This;
                    var inputMember = inputInstruction.NextInstructions[pipeData.nextInstructionMember];

                    inputMember.OutputPipe = outputMember;
                }
            }
        }

        public void OnAfterDeserialize()
        {
        }
    }
}

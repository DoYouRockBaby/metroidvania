﻿using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Presto_Script.Core
{
    [Serializable]
    public abstract class IInstruction : ScriptableObject
    {
        abstract public string Name { get; }
        abstract public bool CanBeReferenced { get; }

        abstract public Vector2 GridPosition { get; set; }

        abstract public IOutputReference This { get; }
        abstract public IInputReference[] Inputs { get; }
        abstract public IOutputReference[] Outputs { get; }
        abstract public IInputReference[] NextInstructions { get; }
        abstract public IOutputReference[] NextExecutableInstructions { get; }

        abstract public void Action(ScriptExecuter exec);
    }
}

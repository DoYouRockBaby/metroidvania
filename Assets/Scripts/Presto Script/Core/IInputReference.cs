﻿using System;
using UnityEditor;

namespace Presto_Script.Core
{
    [Serializable]
    public abstract class IInputReference
    {
        abstract public string Name { get; }
        abstract public object Value { get; set; }
        abstract public Type Type { get; }
        abstract public IOutputReference OutputPipe { get; set; }

#if UNITY_EDITOR
        abstract public SerializedProperty SerializedProperty { get; }
        abstract public void ApplySerializedPropertyModifications();
#endif
    }
}

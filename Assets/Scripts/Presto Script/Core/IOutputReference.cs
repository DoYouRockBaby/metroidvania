﻿using System;

namespace Presto_Script.Core
{
    [Serializable]
    public abstract class IOutputReference
    {
        abstract public string Name { get; }
        abstract public object Value { get; }
        abstract public Type Type { get; }
        abstract public IInstruction[] Dependencies { get; }
    }
}

﻿using UnityEngine;

namespace Presto_Script.Core
{
    public class PrestoScript : MonoBehaviour
    {
        public Script script;
        private ScriptExecuter executer;

        private void Awake()
        {
            executer = new ScriptExecuter(gameObject);
        }

        private void Start()
        {
            executer.Execute(script);
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            executer.InvokeTriggerEntered(collider);
        }

        private void OnTriggerStay2D(Collider2D collider)
        {
            executer.InvokeTriggerStayed(collider);
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            executer.InvokeTriggerExited(collider);
        }
    }
}

﻿using System;

namespace Presto_Script.Core
{
    [Serializable]
    public class InputOutputPipe
    {
        public IInputReference input;
        public IOutputReference output;
    }
}

﻿using System;

namespace Presto_Script.Core
{
    public class InstructionAttribute : Attribute
    {
        public string Label { get; private set; }
        public bool CanBeReferenced { get; private set; }
        public bool InstantiableInEditor { get; private set; }
        public bool FunctionOnly { get; private set; }

        public InstructionAttribute(string label)
        {
            Label = label;
            CanBeReferenced = true;
            InstantiableInEditor = true;
            FunctionOnly = false;
        }

        public InstructionAttribute(string label, bool canBeReferenced)
        {
            Label = label;
            CanBeReferenced = canBeReferenced;
            InstantiableInEditor = true;
            FunctionOnly = false;
        }

        public InstructionAttribute(string label, bool canBeReferenced, bool instantiableInEditor)
        {
            Label = label;
            CanBeReferenced = canBeReferenced;
            InstantiableInEditor = instantiableInEditor;
            FunctionOnly = false;
        }

        public InstructionAttribute(string label, bool canBeReferenced, bool instantiableInEditor, bool functionOnly)
        {
            Label = label;
            CanBeReferenced = canBeReferenced;
            InstantiableInEditor = instantiableInEditor;
            FunctionOnly = functionOnly;
        }
    }

    //Attribute to describe input informations
    public class InputAttribute : Attribute
    {
        public string Label { get; private set; }

        public InputAttribute(string label)
        {
            Label = label;
        }
    }

    //Attribute to describe output informations
    public class OutputAttribute : Attribute
    {
        public string Label { get; private set; }

        public OutputAttribute(string label)
        {
            Label = label;
        }
    }

    //Attribute to describe next instruction informations
    public class NextInstructionAttribute : Attribute
    {
        public string Label { get; private set; }

        public NextInstructionAttribute(string label)
        {
            Label = label;
        }
    }
}

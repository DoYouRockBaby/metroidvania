﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UI
{
    public class InventoryPanel : MonoBehaviour
    {
        public GameObject container = null;
        private float _lastTimeScale = 1f;

        private void Update()
        {
            if (CrossPlatformInputManager.GetButtonDown("Inventory") && container != null)
            {
                container.SetActive(!container.activeInHierarchy);

                if (container.activeInHierarchy)
                {
                    _lastTimeScale = Time.timeScale;
                    //Time.timeScale = 0f;
                }
                else
                {
                    Time.timeScale = _lastTimeScale;
                }
            }
        }
    }
}
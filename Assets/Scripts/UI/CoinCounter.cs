﻿using Gameplay.Character.Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class CoinCounter : MonoBehaviour
    {
        Text text;

        private Inventory inventory = null;
        private Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = Object.FindObjectOfType<Inventory>();
                }

                return inventory;
            }
        }

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        private void Start()
        {
            if (Inventory != null)
            {
                text.text = Inventory.coin.ToString();
            }
        }

        private void Update()
        {
            if (Inventory != null)
            {
                text.text = Inventory.coin.ToString();
            }
        }
    }
}
﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(Dropdown))]
    public class ResolutionDropdown : MonoBehaviour
    {
        private Dropdown dropdown;

        private void Awake()
        {
            dropdown = GetComponent<Dropdown>();
        }

        private void Start()
        {
            dropdown.options = Screen.resolutions.Select(
                (e) => new Dropdown.OptionData()
                {
                    text = e.width + " x " + e.height + "(" + e.refreshRate + " Hz)"
                }).ToList();

            dropdown.value = Array.IndexOf(Screen.resolutions, Screen.currentResolution);
        }

        public void UpdateResolution()
        {
            var resolution = Screen.resolutions[dropdown.value];
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreenMode, resolution.refreshRate);
        }
    }
}

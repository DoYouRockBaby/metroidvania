﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(Toggle))]
    public class FullscreenToggle : MonoBehaviour
    {
        private Toggle toggle;

        private void Awake()
        {
            toggle = GetComponent<Toggle>();
        }

        private void Start()
        {
            toggle.isOn = Screen.fullScreen;
        }

        public void UpdateFullscreen()
        {
            Screen.fullScreen = toggle.isOn;
        }
    }
}

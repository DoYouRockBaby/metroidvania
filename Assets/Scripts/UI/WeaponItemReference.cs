﻿using Gameplay.Character;
using Gameplay.Items;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu(fileName = "reference.asset", menuName = "Item/UI/Weapon Reference")]
    public class WeaponItemReference : ItemReference
    {
        private Gameplay.Character.Player.Weapon weapon = null;
        private Gameplay.Character.Player.Weapon Weapon
        {
            get
            {
                if (weapon == null)
                {
                    weapon = FindObjectOfType<Gameplay.Character.Player.Weapon>();
                }

                return weapon;
            }
        }

        public override AbstractItem Item
        {
            get
            {
                return Weapon.weapon;
            }
            set
            {
                var old = Weapon.weapon;

                if (value is WeaponItem)
                {
                    Weapon.weapon = value as WeaponItem;
                }
                else if (value == null)
                {
                    Weapon.weapon = null;
                }

                if(old != Weapon.weapon)
                {
                    Weapon.InvokeChangedEvent(old, Weapon.weapon);
                }
            }
        }
    }
}
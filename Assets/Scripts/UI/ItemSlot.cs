﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class ItemSlot : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public ItemReference item;

        private Image image;
        private DragAndDropInfos DragAndDrop = null;

        public class DragAndDropInfos
        {
            public Image Image;
            public ItemReference Item;
            public Vector2 PositionOffset;
            public Vector3 OriginalImagePosition;
            public int OriginalLayer;
        }

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        private void Update()
        {
            if (item.Item != null)
            {
                image.color = Color.white;
                image.sprite = item.Item.image;
            }
            else
            {
                image.color = Color.clear;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            var cam = eventData.enterEventCamera;
            var clickScreenPosition = cam.WorldToScreenPoint(image.rectTransform.position);

            DragAndDrop = new DragAndDropInfos()
            {
                Image = image,
                Item = item,
                PositionOffset = new Vector2(clickScreenPosition.x, clickScreenPosition.y) - eventData.position,
                OriginalImagePosition = image.rectTransform.position,
                OriginalLayer = image.gameObject.layer
            };

            image.raycastTarget = false;
            //e.image.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        public void OnDrag(PointerEventData eventData)
        {
            var cam = eventData.enterEventCamera;
            var pos2d = eventData.position + DragAndDrop.PositionOffset;
            var pos3d = cam.ScreenToWorldPoint(new Vector3(pos2d.x, pos2d.y, DragAndDrop.Image.rectTransform.position.z - cam.transform.position.z));
            DragAndDrop.Image.rectTransform.position = pos3d;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            var raycast = eventData.pointerCurrentRaycast;
            image.rectTransform.position = DragAndDrop.OriginalImagePosition;

            var targetSlot = raycast.gameObject.GetComponent<ItemSlot>();
            if (targetSlot != null)
            {
                var temp = targetSlot.item.Item;
                targetSlot.item.Item = item.Item;

                //If the change has realy be done, we remove the object in the origin slot
                if (targetSlot.item.Item == item.Item)
                {
                    item.Item = temp;
                }
            }

            image.raycastTarget = true;
            //image.gameObject.layer = DragAndDrop.OriginalLayer;
        }
    }
}
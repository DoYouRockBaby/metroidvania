﻿using Gameplay.Items;
using UnityEngine;

namespace UI
{
    public abstract class ItemReference : ScriptableObject
    {
        public abstract AbstractItem Item { get; set; }
    }
}
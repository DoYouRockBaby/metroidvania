﻿using Gameplay.Character.Player;
using Gameplay.Items;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu(fileName = "reference.asset", menuName = "Item/UI/Inventory Item Reference")]
    public class InventoryItemReference : ItemReference
    {
        public int index = 0;

        private Inventory inventory = null;
        private Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = FindObjectOfType<Inventory>();
                }

                return inventory;
            }
        }


        public override AbstractItem Item
        {
            get
            {
                if (index >= Inventory.items.Count)
                {
                    return null;
                }
                else
                {
                    return Inventory.items[index];
                }
            }
            set
            {
                while (index >= Inventory.items.Count)
                {
                    Inventory.items.Add(null);
                }

                Inventory.items[index] = value;
            }
        }
    }
}
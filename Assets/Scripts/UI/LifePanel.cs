﻿using Gameplay.Character.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LifePanel : MonoBehaviour
    {
        public Vector2 padding = new Vector2(10.0f, 10.0f);
        public Vector2 margin = new Vector2(10.0f, 10.0f);
        public int maxDivisionPerLine = 10;
        public int indicatorDivisions = 4;
        public GameObject emptyIndicator;
        public GameObject fullIndicator;

        private int lastMaxLife = 0;
        private int lastLife = 0;
        private List<GameObject> _fullIndicators = new List<GameObject>();
        private List<GameObject> _emptyIndicators = new List<GameObject>();

        private Health player = null;
        private Health Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
                }

                return player;
            }
        }

        private void FixedUpdate()
        {
            if (Player == null)
            {
                return;
            }

            if (Player.maxLife != lastMaxLife)
            {
                lastMaxLife = Player.maxLife;
                UpdateIndicator(lastMaxLife, emptyIndicator, _emptyIndicators, indicatorDivisions);
            }

            if (Player.life != lastLife)
            {
                lastLife = Player.life;
                UpdateIndicator(lastLife, fullIndicator, _fullIndicators, indicatorDivisions);
            }
        }

        private void UpdateIndicator(int newValue, GameObject indicatorReference, List<GameObject> indicatorList, int indicatorDivisions)
        {
            //remove previous indicators
            foreach (var indicator in indicatorList)
            {
                Destroy(indicator);
            }
            indicatorList.Clear();

            //add new indicators
            var nIterations = (newValue + indicatorDivisions - newValue % indicatorDivisions) / indicatorDivisions;
            for (int i = 0; i < nIterations; i++)
            {
                var newIndicator = Instantiate(indicatorReference, transform);
                var rectComponent = newIndicator.GetComponent<RectTransform>();
                newIndicator.transform.localPosition = new Vector3(
                    padding.x + (rectComponent.sizeDelta.x * rectComponent.localScale.x + margin.x) * (i % maxDivisionPerLine),
                    -padding.y - (rectComponent.sizeDelta.y * rectComponent.localScale.y + margin.y) * (i / maxDivisionPerLine)
                    , 0.0f);

                //Divide the indicator if needed
                var remainingDivisions = newValue - i * indicatorDivisions;
                if (remainingDivisions < indicatorDivisions)
                {
                    var imageComponent = newIndicator.GetComponent<Image>();
                    imageComponent.fillAmount = remainingDivisions / (float)indicatorDivisions;
                }

                indicatorList.Add(newIndicator);
            }
        }
    }
}
﻿using Gameplay.Character.Player;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(Animator))]
    public class CoinChangedAnimation : MonoBehaviour
    {
        private Animator animator;

        private int lastValue = 0;

        private Inventory inventory = null;
        private Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = GameObject.FindObjectOfType<Inventory>();
                }

                return inventory;
            }
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            lastValue = Inventory.coin;
        }

        private void Update()
        {
            if (lastValue != Inventory.coin)
            {
                animator.SetTrigger("Changed");
            }

            lastValue = Inventory.coin;
        }
    }
}
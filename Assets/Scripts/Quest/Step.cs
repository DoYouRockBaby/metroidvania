﻿using System;

namespace Presto_Script.Quest
{
    [Serializable]
    public struct QuestStep
    {
        public string label;
        public Condition[] conditions;
    }
}

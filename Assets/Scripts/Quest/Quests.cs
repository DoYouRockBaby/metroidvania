﻿using Presto_Script.Quest;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Quest
{
    public class Quests : MonoBehaviour
    {
        public List<QuestState> quests = new List<QuestState>();
    }
}

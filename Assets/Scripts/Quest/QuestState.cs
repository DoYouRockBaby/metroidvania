﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Presto_Script.Quest
{
    [Serializable]
    public struct QuestState : ISerializationCallbackReceiver
    {
        public enum Status
        {
            NotStarted,
            Processing,
            Over
        }

        [Serializable]
        public struct ConditionSerializedEntry
        {
            public string key;
            public string type;
            public string datas;
        }

        public Quest quest;
        public Status status;
        public int step;

        public ConditionSerializedEntry[] serializedEntries;

        private Dictionary<string, object> questConditionsStorage;

        public void OnBeforeSerialize()
        {
            var serializedEntryDatas = new List<ConditionSerializedEntry>();

            foreach (var entry in questConditionsStorage)
            {
                serializedEntryDatas.Add(new ConditionSerializedEntry()
                {
                    key = entry.Key,
                    type = entry.Value.GetType().AssemblyQualifiedName,
                    datas = JsonUtility.ToJson(entry.Value)
                });
            }
        }

        public void OnAfterDeserialize()
        {
            questConditionsStorage = new Dictionary<string, object>();

            foreach (var serialized in serializedEntries)
            {
                var type = Type.GetType(serialized.type);
                var instance = JsonUtility.FromJson(serialized.datas, type);

                questConditionsStorage.Add(serialized.key, instance);
            }
        }
    }
}

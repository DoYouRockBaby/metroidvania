﻿using UnityEngine;

namespace Presto_Script.Quest
{
    [CreateAssetMenu(fileName = "My Quest", menuName = "Presto Script/Quest")]
    public class Quest : ScriptableObject
    {
        public new string name;
        public string description;
        public QuestStep[] steps;
    }
}

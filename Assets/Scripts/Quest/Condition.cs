﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presto_Script.Quest
{
    public abstract class Condition
    {
        public abstract string Description { get; }

        public abstract void OnEnabled();
        public abstract void OnStarted();
    }
}

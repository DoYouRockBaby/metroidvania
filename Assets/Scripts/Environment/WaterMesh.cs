﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

namespace Environment
{
    [RequireComponent(typeof(WaterPhysic))]
    public class WaterMesh : MonoBehaviour
    {
        public enum Mode
        {
            Both,
            Forward,
            Back
        }

        public Mode mode;
        public float startDepth = 10f;
        public float endDepth = 10f;
        public float totalDepth = 100f;
        public Material material;
        public float planeTopDistance = 1f;
        public float textureTiling = 1f;

        public MeshFilter waterMeshFilter;
        public MeshRenderer waterMeshRenderer;

        private WaterPhysic waterPhysic;

        private void Awake()
        {
            waterPhysic = GetComponent<WaterPhysic>();
        }

        private void Start()
        {
            if(waterMeshFilter == null && waterMeshRenderer == null)
            {
                var waterMeshObject = new GameObject("Water Mesh");
                waterMeshObject.transform.parent = transform;
                waterMeshObject.transform.localPosition = Vector3.zero;
                waterMeshObject.transform.localRotation = Quaternion.identity;
                waterMeshObject.transform.localScale = Vector3.one;
                waterMeshFilter = waterMeshObject.AddComponent<MeshFilter>();
                waterMeshRenderer = waterMeshObject.AddComponent<MeshRenderer>();
            }
            else if(waterMeshFilter != null && waterMeshRenderer == null)
            {
                waterMeshRenderer = waterMeshFilter.GetComponent<MeshRenderer>();

                if(waterMeshRenderer == null)
                {
                    waterMeshRenderer = waterMeshFilter.gameObject.AddComponent<MeshRenderer>();
                }
            }
            else if (waterMeshRenderer != null && waterMeshFilter == null)
            {
                waterMeshFilter = waterMeshRenderer.GetComponent<MeshFilter>();

                if (waterMeshFilter == null)
                {
                    waterMeshFilter = waterMeshRenderer.gameObject.AddComponent<MeshFilter>();
                }
            }

            UpdateWaterMesh();
        }

        private void Update()
        {
            if(waterPhysic.Activated)
            {
                UpdateWaterMesh();
            }
        }

        public void UpdateWaterMesh()
        {
            if(waterPhysic.WaterPoints.Count == 0)
            {
                return;
            }

            //Build edge vertices
            var centralEdge = BuildLateralEdge(0f, false);
            var start1Edge = BuildLateralEdge(-startDepth);
            var start2Edge = BuildLateralEdge(startDepth);
            var central1Edge = BuildLateralEdge(endDepth - totalDepth);
            var central2Edge = BuildLateralEdge(totalDepth - endDepth);
            var end1Edge = BuildLateralEdge(-totalDepth);
            var end2Edge = BuildLateralEdge(totalDepth);

            //Mesh informations
            var vertices = new List<Vector3>();
            var uvs = new List<Vector2>();
            var colors = new List<Color>();
            var triangles = new List<int>();

            //Build vertices
            PushEdge(centralEdge, 0f, Color.white, ref vertices, ref uvs, ref colors);
            PushEdge(start1Edge, -startDepth, Color.white, ref vertices, ref uvs, ref colors);
            PushEdge(start2Edge, startDepth, Color.white, ref vertices, ref uvs, ref colors);
            PushEdge(central1Edge, endDepth - totalDepth, Color.white, ref vertices, ref uvs, ref colors);
            PushEdge(central2Edge, totalDepth - endDepth, Color.white, ref vertices, ref uvs, ref colors);
            PushEdge(end1Edge, -totalDepth, new Color(1f, 1f, 1f, 0f), ref vertices, ref uvs, ref colors);
            PushEdge(end2Edge, totalDepth, new Color(1f, 1f, 1f, 0f), ref vertices, ref uvs, ref colors);

            //Build faces
            triangles.AddRange(BuildEdgeFaces(centralEdge, 0, start1Edge, centralEdge.Count));
            triangles.AddRange(BuildEdgeFaces(centralEdge, 0, start2Edge, centralEdge.Count + start1Edge.Count));
            triangles.AddRange(BuildEdgeFaces(start1Edge, centralEdge.Count, central1Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count));
            triangles.AddRange(BuildEdgeFaces(start2Edge, centralEdge.Count + start1Edge.Count, central2Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count + central1Edge.Count));
            triangles.AddRange(BuildEdgeFaces(central1Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count, end1Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count + central1Edge.Count + central2Edge.Count));
            triangles.AddRange(BuildEdgeFaces(central2Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count + central1Edge.Count, end2Edge, centralEdge.Count + start1Edge.Count + start2Edge.Count + central1Edge.Count + central2Edge.Count + end1Edge.Count));

            //Build and setup mesh
            waterMeshFilter.mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                triangles = triangles.ToArray(),
                uv = uvs.ToArray(),
                colors = colors.ToArray()
            };
            waterMeshRenderer.material = material;
        }

        public void PushEdge(List<Vector3> edge, float depth, Color color, ref List<Vector3> vertices, ref List<Vector2> uvs, ref List<Color> colors)
        {
            foreach (var vertice in edge)
            {
                vertices.Add(vertice);
                uvs.Add(new Vector2((vertice.x - transform.position.x) / waterPhysic.segmentSize / textureTiling, depth / waterPhysic.segmentSize / textureTiling));
                colors.Add(color);
            }
        }

        public List<Vector3> BuildLateralEdge(float depth, bool followBaseHeight = true)
        {
            var vertices = new List<Vector3>();

            foreach (var waterPoint in waterPhysic.WaterPoints)
            {
                vertices.Add(new Vector3(waterPoint.Position.x - transform.position.x, (followBaseHeight ? waterPoint.BaseHeight : waterPoint.Position.y) - transform.position.y) + Vector3.forward * depth);
            }

            return vertices;
        }

        public List<int> BuildEdgeFaces(List<Vector3> fromEdge, int fromOffset, List<Vector3> toEdge, int toOffset)
        {
            if(fromEdge.Count != toEdge.Count)
            {
                throw new System.Exception("The two list must have the same size.");
            }

            var triangles = new List<int>();
            for (var i = 0; i < fromEdge.Count - 1; i++)
            {
                triangles.Add(i + fromOffset);
                triangles.Add(i + fromOffset + 1);
                triangles.Add(i + toOffset);

                triangles.Add(i + toOffset);
                triangles.Add(i + fromOffset + 1);
                triangles.Add(i + toOffset + 1);

                triangles.Add(i + fromOffset);
                triangles.Add(i + toOffset);
                triangles.Add(i + fromOffset + 1);

                triangles.Add(i + toOffset);
                triangles.Add(i + toOffset + 1);
                triangles.Add(i + fromOffset + 1);
            }

            return triangles;
        }

        /*public void UpdateWaterMesh()
        {
            var vertices = new List<Vector3>();
            var faces = new List<int>();
            var uvs = new List<Vector2>();
            var colors = new List<Color>();

            //Keep only top vertices
            for (var i = 0; i < waterPhysic.WaterPoints.Count - 1; i++)
            {
                var pos1 = waterPhysic.WaterPoints[i].Position;
                var pos2 = waterPhysic.WaterPoints[i + 1].Position;

                //Calculate total uv
                var uvx = textureTiling / Mathf.Abs(pos1.x - pos2.x);
                var uvy = textureTiling / Mathf.Abs(pos1.y - pos2.y);

                //Inner part
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));

                uvs.Add(new Vector2(0, 0));
                uvs.Add(new Vector2(0, uvy * (startDepth / totalDepth)));
                uvs.Add(new Vector2(uvx, uvy * (startDepth / totalDepth)));
                uvs.Add(new Vector2(uvy, 0));

                colors.Add(Color.white);
                colors.Add(Color.white);
                colors.Add(Color.white);
                colors.Add(Color.white);

                //Central part
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));

                uvs.Add(new Vector2(0, uvy * (startDepth / totalDepth)));
                uvs.Add(new Vector2(0, uvy * (totalDepth - endDepth) / totalDepth));
                uvs.Add(new Vector2(uvx, uvy * (startDepth - endDepth) / totalDepth));
                uvs.Add(new Vector2(uvy, uvy * (startDepth / totalDepth)));

                colors.Add(Color.white);
                colors.Add(Color.white);
                colors.Add(Color.white);
                colors.Add(Color.white);

                //Outer part
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));
                vertices.Add(pos1 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Forward) ? Vector3.back : Vector3.zero));
                vertices.Add(pos2 + depth * ((mode == Mode.Both || mode == Mode.Back) ? Vector3.forward : Vector3.zero));

                uvs.Add(new Vector2(0, uvy * (totalDepth - endDepth) / totalDepth));
                uvs.Add(new Vector2(0, uvy));
                uvs.Add(new Vector2(uvx, uvy));
                uvs.Add(new Vector2(uvy, uvy * (totalDepth - endDepth) / totalDepth));

                colors.Add(new Color(1f, 1f, 1f, 0f));
                colors.Add(new Color(1f, 1f, 1f, 0f));
                colors.Add(new Color(1f, 1f, 1f, 0f));
                colors.Add(new Color(1f, 1f, 1f, 0f));
            }

            for (var i = 0; i < vertices.Count / 2; i++)
            {
                faces.Add((i * 4) % vertices.Count);
                faces.Add((i * 4 + 1) % vertices.Count);
                faces.Add((i * 4 + 3) % vertices.Count);
                faces.Add((i * 4 + 1) % vertices.Count);
                faces.Add((i * 4 + 3) % vertices.Count);
                faces.Add((i * 4 + 2) % vertices.Count);

                faces.Add((i * 4) % vertices.Count);
                faces.Add((i * 4 + 3) % vertices.Count);
                faces.Add((i * 4 + 1) % vertices.Count);
                faces.Add((i * 4 + 1) % vertices.Count);
                faces.Add((i * 4 + 2) % vertices.Count);
                faces.Add((i * 4 + 3) % vertices.Count);
            }

            waterMeshFilter.mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                triangles = faces.ToArray(),
                uv = uvs.ToArray(),
                colors = colors.ToArray()
            };

            waterMeshRenderer.material = material;
        }*/
    }
}

﻿using UnityEngine;

namespace Environment
{
    public class InvokeOnCollisionMovement : MonoBehaviour
    {
        public enum CollisionType
        {
            Both,
            Borders,
            Inside
        }

        public new GameObject gameObject;
        public LayerMask mask;
        public Vector2 offset = Vector3.zero;
        public float frequency = .15f;
        public float minVelocity = .2f;
        public float maxVelocity = 10f;
        public CollisionType collisionType;
        public bool attach;

        Vector2 lastPosition = Vector3.zero;
        float lastTime = float.MinValue;
        private new Collider2D collider;

        private void Awake()
        {
            collider = GetComponent<Collider2D>();
        }

        private void OnTriggerStay2D(Collider2D collider)
        {
            if (mask.value == 1 << collider.gameObject.layer)
            {
                var controller = collider.GetComponent<Physic.Controller2D>();
                if (controller != null && Mathf.Abs(controller.velocity.x) > minVelocity)
                {
                    if(collisionType == CollisionType.Both)
                    {
                        if (Time.time > lastTime + frequency * maxVelocity / Mathf.Abs(controller.velocity.x))
                        {
                            var gameObject = Instantiate(this.gameObject, collider.transform.position + Vector3.right * offset.x + Vector3.up * offset.y, this.gameObject.transform.rotation);

                            if (attach)
                            {
                                gameObject.transform.parent = collider.transform;
                            }

                            lastTime = Time.time;
                        }
                    }
                    else if (collisionType == CollisionType.Borders &&
                        (!this.collider.bounds.Contains(new Vector3(collider.bounds.max.x, collider.bounds.max.y, this.collider.bounds.center.z)) ||
                        !this.collider.bounds.Contains(new Vector3(collider.bounds.min.x, collider.bounds.min.y, this.collider.bounds.center.z))))
                    {
                        if (Time.time > lastTime + frequency * maxVelocity / Mathf.Abs(controller.velocity.x))
                        {
                            var gameObject = Instantiate(this.gameObject, collider.transform.position + Vector3.right * offset.x + Vector3.up * offset.y, this.gameObject.transform.rotation);

                            if (attach)
                            {
                                gameObject.transform.parent = collider.transform;
                            }

                            lastTime = Time.time;
                        }
                    }
                    if (collisionType == CollisionType.Inside &&
                        this.collider.bounds.Contains(new Vector3(collider.bounds.max.x, collider.bounds.max.y, this.collider.bounds.center.z)) &&
                        this.collider.bounds.Contains(new Vector3(collider.bounds.min.x, collider.bounds.min.y, this.collider.bounds.center.z)))
                    {
                        if (Time.time > lastTime + frequency * maxVelocity / Mathf.Abs(controller.velocity.x))
                        {
                            var gameObject = Instantiate(this.gameObject, collider.transform.position + Vector3.right * offset.x + Vector3.up * offset.y, this.gameObject.transform.rotation);

                            if (attach)
                            {
                                gameObject.transform.parent = collider.transform;
                            }

                            lastTime = Time.time;
                        }
                    }
                }
            }
        }

        void OnColliderStay2D(Collider2D collider)
        {
            //OnTriggerStay2D(collider);
        }

        private void OnCollisionStay2D(UnityEngine.Collision2D collision)
        {
            OnTriggerStay2D(collision.collider);
        }
    }
}

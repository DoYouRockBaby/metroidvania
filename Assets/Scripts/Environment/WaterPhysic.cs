﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.U2D;

namespace Environment
{
    [RequireComponent(typeof(SpriteShapeController))]
    public class WaterPhysic : MonoBehaviour
    {
        public float segmentSize = .5f;
        public bool loadOnlyIfVisible = true;
        public float stiffness = 1.5f;
        public float dampening = 1.5f;
        [Range(0f, 30f)]
        public float spread = 15;
        public Vector2 speedInfluence = new Vector2(.5f, 1f);
        public LayerMask mask;

        public bool Activated
        {
            get
            {
                return waterPoints.Count > 0;
            }
        }

        public class WaterPoint
        {
            SpriteShapeController _spriteShapeController = null;

            public WaterPoint(int index, SpriteShapeController spriteShapeController, float baseHeight)
            {
                Index = index;
                _spriteShapeController = spriteShapeController;
                Velocity = 0.0f;
                Acceleration = 0.0f;
                BaseHeight = baseHeight;
            }

            public int Index { get; set; }

            public float Height
            {
                get
                {
                    return Position.y;
                }
                set
                {
                    var pos = Position;
                    Position = new Vector3(pos.x, value);
                }
            }

            public Vector2 Position
            {
                get
                {
                    var vec3 = _spriteShapeController.spline.GetPosition(Index);
                    vec3 = _spriteShapeController.transform.TransformPoint(vec3);
                    return new Vector2(vec3.x, vec3.y);
                }
                set
                {
                    var wvec = new Vector3(value.x, value.y, 0.0f);
                    wvec = _spriteShapeController.transform.InverseTransformPoint(wvec);

                    var vec3 = _spriteShapeController.spline.GetPosition(Index);
                    _spriteShapeController.spline.SetPosition(Index, new Vector3(wvec.x, wvec.y, vec3.z));
                }
            }

            public bool Static { get; set; }

            public float Velocity { get; set; }

            public float Acceleration { get; set; }

            public float BaseHeight { get; set; }
        }

        public IList<WaterPoint> WaterPoints
        {
            get
            {
                return waterPoints;
            }
        }

        private SpriteShapeController spriteShape;
        private List<WaterPoint> waterPoints = new List<WaterPoint>();


        private void Awake()
        {
            spriteShape = GetComponent<SpriteShapeController>();

            if (!loadOnlyIfVisible)
            {
                BuildWaterPoints();
            }
        }

        private void Update()
        {
            if(!Activated)
            {
                return;
            }

            //Update water point position
            foreach (var waterPoint in waterPoints)
            {
                if(!waterPoint.Static)
                {
                    float diff = waterPoint.Position.y - waterPoint.BaseHeight;
                    float acceleration = -stiffness * diff - dampening * waterPoint.Velocity;

                    waterPoint.Height = waterPoint.Position.y + waterPoint.Velocity * Time.deltaTime;
                    waterPoint.Velocity += acceleration * Time.deltaTime;
                }
            }

            //Propagate velocity
            float[] leftDeltas = new float[waterPoints.Count];
            float[] rightDeltas = new float[waterPoints.Count];

            for (int j = 0; j < 8; j++)
            {
                for (int i = 0; i < waterPoints.Count; i++)
                {
                    if (!waterPoints[i].Static)
                    {
                        if (i > 0)
                        {
                            leftDeltas[i] = spread * (waterPoints[i].Height - waterPoints[i - 1].Height);
                            waterPoints[i - 1].Velocity += leftDeltas[i];
                        }
                        if (i < waterPoints.Count - 1)
                        {
                            rightDeltas[i] = spread * (waterPoints[i].Height - waterPoints[i + 1].Height);
                            waterPoints[i + 1].Velocity += rightDeltas[i];
                        }
                    }
                }

                for (int i = 0; i < waterPoints.Count; i++)
                {
                    if (i > 0)
                        waterPoints[i - 1].Height += leftDeltas[i] * Time.deltaTime;
                    if (i < waterPoints.Count - 1)
                        waterPoints[i + 1].Height += rightDeltas[i] * Time.deltaTime;
                }
            }
        }

        private void OnBecameVisible()
        {
            if(loadOnlyIfVisible)
            {
                BuildWaterPoints();
            }
        }

        private void OnBecameInvisible()
        {
            if (loadOnlyIfVisible)
            {
                ReleaseWaterPoints();
            }
        }

        private void OnTriggerStay2D(Collider2D collider)
        {
            var controller = collider.GetComponent<Physic.Controller2D>();
            if (controller != null)
            {
                Splash(collider.bounds, Mathf.Abs(controller.velocity.x * speedInfluence.x));
            }
        }

        private void OnColliderStay2D(Collider2D collider)
        {
            OnTriggerStay2D(collider);
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            OnTriggerStay2D(collision.collider);
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            var controller = collider.GetComponent<Physic.Controller2D>();
            if (controller != null)
            {
                Splash(collider.bounds, controller.velocity.y * speedInfluence.y);
            }
        }

        private void OnColliderEnter2D(Collider2D collider)
        {
            OnTriggerEnter2D(collider);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnTriggerEnter2D(collision.collider);
        }

        public void Splash(Bounds bounds, float velocity)
        {
            foreach(var waterPoint in WaterPoints)
            {
                if(bounds.Contains(new Vector3(waterPoint.Position.x, waterPoint.Position.y, bounds.center.z)))
                {
                    waterPoint.Velocity = velocity;
                }
            }
        }

        public void Splash(Vector2 position, float velocity)
        {
            var waterPoint = FindNearestPoint(position);

            if(waterPoint != null)
            {
                waterPoint.Velocity = velocity;
            }
        }

        private WaterPoint FindNearestPoint(Vector2 position)
        {
            var nearestPointDistance = float.MaxValue;
            WaterPoint nearestPoint = null;
            foreach (var waterPoint in waterPoints)
            {
                float distance;
                if((distance = Vector2.Distance(waterPoint.Position, position)) < nearestPointDistance)
                {
                    nearestPoint = waterPoint;
                    nearestPointDistance = distance;
                }
            }

            return nearestPoint;
        }

        private void BuildWaterPoints()
        {
            //The first water surface point extremity will be the highest point 
            var waterPoint1 = FindHighestPoint();
            //The second water surface point extremity is the first point's highest neightbour
            var waterPoint2 = spriteShape.spline.GetPosition(waterPoint1 - 1).y >= spriteShape.spline.GetPosition(waterPoint1 + 1).y ?
                waterPoint1 - 1 : waterPoint1 + 1;

            var waterSurfaceBeginIndex = Mathf.Min(waterPoint1, waterPoint2);
            var waterSurfaceEndIndex = Mathf.Max(waterPoint1, waterPoint2);

            //Get extremity positions
            var beginPosition = spriteShape.spline.GetPosition(waterSurfaceBeginIndex);
            var endPosition = spriteShape.spline.GetPosition(waterSurfaceEndIndex);

            //Calculate dimensions
            var segmentLength = Vector2.Distance(new Vector2(beginPosition.x, beginPosition.y), new Vector2(endPosition.x, endPosition.y));
            var segmentNumber = (int)(segmentLength / segmentSize);
            var realSegmentSize = segmentLength / segmentNumber;

            //Insert the water points
            waterPoints.Clear();
            var segmentGap = (endPosition - beginPosition).normalized * realSegmentSize;
            var lastPosition = beginPosition;
            for (var i = 0; i < (segmentNumber - 1); i++)
            {
                lastPosition = lastPosition + segmentGap;
                var index = waterSurfaceBeginIndex + 1 + i;
                spriteShape.spline.InsertPointAt(index, lastPosition);
                spriteShape.spline.SetTangentMode(index, ShapeTangentMode.Continuous);

                var point = new WaterPoint(index, spriteShape, transform.TransformPoint(lastPosition).y);

                point.Static = Physics2D.OverlapPoint(point.Position, mask);

                waterPoints.Add(point);
            }
        }

        private void ReleaseWaterPoints()
        {
            var indexes = waterPoints.Select(e => e.Index).OrderByDescending(e => e);
            foreach (var index in indexes)
            {
                spriteShape.spline.RemovePointAt(index);
            }

            waterPoints.Clear();
        }

        int FindHighestPoint()
        {
            var index = -1;
            var maxValue = float.MinValue;
            for (var i = 0; i < spriteShape.spline.GetPointCount(); i++)
            {
                var point = spriteShape.spline.GetPosition(i);
                if (point.y > maxValue)
                {
                    index = i;
                    maxValue = point.y;
                }
            }

            return index;
        }
    }
}

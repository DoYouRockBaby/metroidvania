﻿using UnityEngine;

namespace Environment
{
    public class InvokeOnCollisionEnter : MonoBehaviour
    {
        public new GameObject gameObject;
        public LayerMask mask;
        public Vector2 offset = Vector3.zero;

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (mask.value == 1 << collider.gameObject.layer)
            {
                Instantiate(gameObject, collider.transform.position + Vector3.right * offset.x + Vector3.up * offset.y, gameObject.transform.rotation);
            }
        }

        void OnColliderEnter2D(Collider2D collider)
        {
            OnTriggerEnter2D(collider);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnTriggerEnter2D(collision.collider);
        }
    }
}

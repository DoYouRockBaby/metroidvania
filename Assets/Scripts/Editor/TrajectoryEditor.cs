﻿using Assets.Scripts.Utility;
using Gameplay.Environment;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Trajectory))]
public class TrajectoryEditor : Editor
{
    protected enum EditMode
    {
        Position,
        Rotation,
        Scale,
        Interpolation,
        Delete
    }

    protected Trajectory _trajectory = null;
    protected EditMode _currentEditMode = EditMode.Position;

    /*public override void OnInspectorGUI()
    {
        // get the chosen game object
        _trajectory = target as Trajectory;

        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("loop"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("nodes"), true);
        serializedObject.ApplyModifiedProperties();
    }*/

    public void OnSceneGUI()
    {
        // get the chosen game object
        _trajectory = target as Trajectory;

        if (_trajectory == null)
            return;

        InitializeNodesIfNeeded();
        UpdateEditMode();
        DrawTrajectory();

        if (GUI.changed)
            EditorUtility.SetDirty(target);

        //SceneView.RepaintAll();
    }

    protected void InitializeNodesIfNeeded()
    {
        if (NodeCount() <= 1)
        {
            _trajectory.nodes = new Trajectory.Node[]
            {
                new Trajectory.Node(),
                new Trajectory.Node()
                {
                    position = new Vector3(1f, 0f, 0f)
                }
            };
        }
    }

    protected int NodeCount()
    {
        return _trajectory.nodes.Length;
    }

    protected Trajectory.Node GetNode(int index)
    {
        return _trajectory.nodes[index];
    }

    protected void AddNodeAfter(int index)
    {
        var position = Vector3.Lerp(_trajectory.transform.TransformPoint(GetNode(index).position), _trajectory.transform.TransformPoint(GetNode((index + 1) % NodeCount()).position), 0.5f);

        Trajectory.Node[] newArray = new Trajectory.Node[_trajectory.nodes.Length + 1];
        for (int i = 0; i < _trajectory.nodes.Length + 1; i++)
        {
            if (i <= index)
            {
                newArray[i] = _trajectory.nodes[i];
            }
            else if (i > index + 1)
            {
                newArray[i] = _trajectory.nodes[i - 1];
            }
            else
            {
                newArray[i] = new Trajectory.Node()
                {
                    position = _trajectory.transform.InverseTransformPoint(position),
                    rotation = GetNode(index).rotation,
                    scale = GetNode(index).scale,
                };
            }
        }

        _trajectory.nodes = newArray;
    }

    protected void RemoveNode(int index)
    {
        Trajectory.Node[] newArray = new Trajectory.Node[_trajectory.nodes.Length - 1];
        for (int i = 0; i < _trajectory.nodes.Length; i++)
        {
            if (i < index)
            {
                newArray[i] = _trajectory.nodes[i];
            }
            else if (i > index)
            {
                newArray[i - 1] = _trajectory.nodes[i];
            }
        }

        _trajectory.nodes = newArray;
    }

    protected void UpdateEditMode()
    {
        Event e = Event.current;
        if (e.type == EventType.KeyDown)
        {
            if(e.keyCode == KeyCode.LeftAlt || e.keyCode == KeyCode.RightAlt)
            {
                switch(_currentEditMode)
                {
                    case EditMode.Position :
                        _currentEditMode = EditMode.Rotation;
                        break;
                    case EditMode.Rotation:
                        _currentEditMode = EditMode.Scale;
                        break;
                    case EditMode.Scale:
                        _currentEditMode = EditMode.Position;
                        break;
                }
            }

            if(e.keyCode == KeyCode.C)
            {
                _currentEditMode = EditMode.Delete;
            }
            else if (e.keyCode == KeyCode.X)
            {
                _currentEditMode = EditMode.Interpolation;
            }
        }
        else if(e.type == EventType.KeyUp)
        {
            if (e.keyCode == KeyCode.C || e.keyCode == KeyCode.X)
            {
                _currentEditMode = EditMode.Position;
            }
        }
    }

    protected void DrawTrajectory()
    {
        for (int i = 0; i < NodeCount(); i++)
        {
            //Draw controls
            switch(_currentEditMode)
            {
                case EditMode.Position:
                    DrawPositionControl(i);
                    break;
                case EditMode.Rotation:
                    DrawRotationControl(i);
                    break;
                case EditMode.Scale:
                    DrawScaleControl(i);
                    break;
                case EditMode.Delete:
                    DrawDeleteControl(i);
                    break;
                case EditMode.Interpolation:
                    DrawInterpolationControl(i);
                    break;
            }

            DrawTangentControl(i);
            DrawLine(i);
        }
    }

    protected void DrawPositionControl(int index)
    {
        var position = Handles.FreeMoveHandle(
            index,
            _trajectory.transform.TransformPoint(GetNode(index).position),
            Quaternion.Euler(0.0f, 0.0f, 0.0f), 1.0f,
            Vector3.zero,
            Handles.SphereHandleCap);

        if (index != 0)
        {
            GetNode(index).position = _trajectory.transform.InverseTransformPoint(position);
        }
    }

    protected void DrawRotationControl(int index)
    {
        var rotation = Handles.RotationHandle(
            Quaternion.Euler(GetNode(index).rotation.x, GetNode(index).rotation.y, GetNode(index).rotation.z),
            _trajectory.transform.TransformPoint(GetNode(index).position));

        Handles.ArrowHandleCap(
            index,
            _trajectory.transform.TransformPoint(GetNode(index).position),
            rotation,
            1.0f,
            EventType.Ignore
        );

        GetNode(index).rotation = rotation.eulerAngles;
    }

    protected void DrawScaleControl(int index)
    {
        var scale = Handles.ScaleHandle(
            GetNode(index).scale,
            _trajectory.transform.TransformPoint(GetNode(index).position),
            Quaternion.Euler(0.0f, 0.0f, 0.0f), 1.0f);

        GetNode(index).scale = scale;
    }

    protected void DrawDeleteControl(int index)
    {
        Handles.color = Color.red;

        var delete = Handles.Button(
            _trajectory.transform.TransformPoint(GetNode(index).position),
            Quaternion.Euler(0.0f, 0.0f, 0.0f),
            1.0f,
            0.5f,
            Handles.SphereHandleCap);

        if (delete)
        {
            RemoveNode(index);
        }

        Handles.color = Color.white;
    }

    protected void DrawInterpolationControl(int index)
    {
        Handles.color = Color.yellow;

        var interpolation = Handles.Button(
            _trajectory.transform.TransformPoint(GetNode(index).position),
            Quaternion.Euler(0.0f, 0.0f, 0.0f),
            1.0f,
            0.5f,
            Handles.SphereHandleCap);

        if (interpolation)
        {
            switch (GetNode(index).interpolation)
            {
                case MathUtility.Interpolation.Linear:
                    GetNode(index).interpolation = MathUtility.Interpolation.BezierSimple;
                    break;
                case MathUtility.Interpolation.BezierSimple:
                    GetNode(index).interpolation = MathUtility.Interpolation.BezierFree;
                    break;
                case MathUtility.Interpolation.BezierFree:
                    GetNode(index).interpolation = MathUtility.Interpolation.Linear;
                    break;
            }

            if (GetNode(index).interpolation == MathUtility.Interpolation.BezierFree)
            {
                //set default tangent
                GetNode(index).tangent1 = (GetNode((index - 1 + NodeCount()) % NodeCount()).position - GetNode((index + 1) % NodeCount()).position).normalized * -3.0f;
                GetNode(index).tangent2 = (GetNode((index - 1 + NodeCount()) % NodeCount()).position - GetNode((index + 1) % NodeCount()).position).normalized * 3.0f;
            }
            else if (GetNode(index).interpolation == MathUtility.Interpolation.BezierSimple)
            {
                //set default tangent
                GetNode(index).tangent1 = (GetNode((index - 1 + NodeCount()) % NodeCount()).position - GetNode((index + 1) % NodeCount()).position).normalized * -3.0f;
            }
        }

        Handles.color = Color.white;
    }

    protected void DrawTangentControl(int index)
    {
        var nodePosition = _trajectory.transform.TransformPoint(GetNode(index).position);
        if (GetNode(index).interpolation == MathUtility.Interpolation.BezierSimple)
        {
            var tangent1 = Handles.FreeMoveHandle(
                index + NodeCount(),
                _trajectory.transform.TransformPoint(GetNode(index).tangent1),
                Quaternion.Euler(0.0f, 0.0f, 0.0f), 0.25f,
                Vector3.zero,
                Handles.SphereHandleCap);

            GetNode(index).tangent1 = _trajectory.transform.InverseTransformPoint(tangent1);
        }
        else if (GetNode(index).interpolation == MathUtility.Interpolation.BezierFree)
        {
            var tangent1 = Handles.FreeMoveHandle(
                index + NodeCount(),
                _trajectory.transform.TransformPoint(GetNode(index).tangent1),
                Quaternion.Euler(0.0f, 0.0f, 0.0f), 0.25f,
                Vector3.zero,
                Handles.SphereHandleCap);

            GetNode(index).tangent1 = _trajectory.transform.InverseTransformPoint(tangent1);

            var tangent2 = Handles.FreeMoveHandle(
                index + NodeCount() * 2,
                _trajectory.transform.TransformPoint(GetNode(index).tangent2),
                Quaternion.Euler(0.0f, 0.0f, 0.0f), 0.25f,
                Vector3.zero,
                Handles.SphereHandleCap);

            GetNode(index).tangent2 = _trajectory.transform.InverseTransformPoint(tangent2);
        }
    }

    protected void DrawLine(int index)
    {
        var nodePosition = _trajectory.transform.TransformPoint(GetNode(index).position);
        var nextIndex = (index + 1) % NodeCount();
        if (GetNode(index).interpolation == MathUtility.Interpolation.Linear && GetNode(nextIndex).interpolation == MathUtility.Interpolation.Linear)
        {
            Handles.DrawLine(_trajectory.transform.TransformPoint(GetNode(index).position), _trajectory.transform.TransformPoint(GetNode((index + 1) % NodeCount()).position));

            //Draw add buttons at the middle
            Handles.color = Color.green;

            var add = Handles.Button(
                Vector3.Lerp(_trajectory.transform.TransformPoint(GetNode(index).position), _trajectory.transform.TransformPoint(GetNode((index + 1) % NodeCount()).position), 0.5f),
                Quaternion.Euler(0.0f, 0.0f, 0.0f),
                0.5f,
                0.25f,
                Handles.SphereHandleCap);

            if (add)
            {
                AddNodeAfter(index);
            }

            Handles.color = Color.white;
        }
        else
        {
            Vector3 tangent1 = Vector3.zero;
            switch (GetNode(index).interpolation)
            {
                case MathUtility.Interpolation.Linear:
                    tangent1 = (_trajectory.transform.TransformPoint(GetNode(nextIndex).position) + _trajectory.transform.TransformPoint(GetNode(index).position)) / 2f;
                    break;
                case MathUtility.Interpolation.BezierSimple:
                    tangent1 = _trajectory.transform.TransformPoint(GetNode(index).tangent1);
                    break;
                case MathUtility.Interpolation.BezierFree:
                    tangent1 = _trajectory.transform.TransformPoint(GetNode(index).tangent1);
                    break;
            }

            Vector3 tangent2 = Vector3.zero;
            switch (GetNode(nextIndex).interpolation)
            {
                case MathUtility.Interpolation.Linear:
                    tangent2 = (_trajectory.transform.TransformPoint(GetNode(nextIndex).position) + _trajectory.transform.TransformPoint(GetNode(index).position)) / 2f;
                    break;
                case MathUtility.Interpolation.BezierSimple:
                    tangent2 = _trajectory.transform.TransformPoint(2f * GetNode(nextIndex).position - GetNode(nextIndex).tangent1);
                    break;
                case MathUtility.Interpolation.BezierFree:
                    tangent2 = _trajectory.transform.TransformPoint(GetNode(nextIndex).tangent2);
                    break;
            }

            Handles.DrawBezier(nodePosition, _trajectory.transform.TransformPoint(GetNode((index + 1) % NodeCount()).position), tangent1, tangent2, Color.white, null, 1.0f);

            //Draw add buttons at the middle
            Handles.color = Color.green;

            var add = Handles.Button(
                Vector3.Lerp(_trajectory.transform.TransformPoint(GetNode(index).position), _trajectory.transform.TransformPoint(GetNode((index + 1) % NodeCount()).position), 0.5f),
                Quaternion.Euler(0.0f, 0.0f, 0.0f),
                0.5f,
                0.25f,
                Handles.SphereHandleCap);

            if (add)
            {
                AddNodeAfter(index);
            }

            Handles.color = Color.white;
        }
    }

    protected Vector3 GetMousePosition()
    {
        Vector2 mousePos = Event.current.mousePosition;
        mousePos.y = Camera.current.pixelHeight - mousePos.y;
        return Camera.current.ScreenPointToRay(mousePos).origin;
    }
}
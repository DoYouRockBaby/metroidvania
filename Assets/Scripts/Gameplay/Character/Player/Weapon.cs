﻿using Gameplay.Items;
using System;
using UnityEngine;

namespace Gameplay.Character.Player
{
    public class Weapon : MonoBehaviour
    {
        public WeaponItem weapon = null;

        public class ChangedEventArgs : EventArgs
        {
            public WeaponItem oldWeapon;
            public WeaponItem newWeapon;
        }

        public delegate void ChangedEventHandler(Weapon sender, ChangedEventArgs e);

        public event ChangedEventHandler Changed;

        public void InvokeChangedEvent(WeaponItem oldWeapon, WeaponItem newWeapon)
        {
            if(Changed != null)
            {
                Changed.Invoke(this, new ChangedEventArgs()
                {
                    oldWeapon = oldWeapon,
                    newWeapon = newWeapon
                });
            }
        }
    }
}
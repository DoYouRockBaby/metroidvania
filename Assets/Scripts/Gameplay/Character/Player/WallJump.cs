﻿using Physic;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D), typeof(Gravity), typeof(WallSlide)), RequireComponent(typeof(Jump))]
    public class WallJump : MonoBehaviour
    {
        public float speed = 20f;

        private Physic.Controller2D controller;
        private Gravity gravity;
        private WallSlide wallSlide;
        private Jump jump;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            gravity = GetComponent<Gravity>();
            wallSlide = GetComponent<WallSlide>();
            jump = GetComponent<Jump>();
        }

        private void Update()
        {
            if (wallSlide.wallSliding)
            {
                //Reset air jump
                jump.ResetAirJump();

                if (CrossPlatformInputManager.GetButtonDown("Jump"))
                {
                    var wallDirection = (controller.collisions.right) ? -1 : 1;

                    controller.velocity = new Vector2(
                        speed * wallDirection,
                        Mathf.Sqrt(2f * jump.height * -gravity.gravity));

                    transform.Rotate(Vector3.up, 180f);
                    controller.facingRight = !controller.facingRight;

                    //Invoke jump event
                    jump.InvokeJumpedEvent();

                    //Disable wall slide to prevent movement reset by wall gravity
                    wallSlide.enabled = false;

                    //And reset it after a small timelaps
                    StartCoroutine(RestoreStateAfterTime());
                }
            }
        }

        IEnumerator RestoreStateAfterTime()
        {
            yield return new WaitForSeconds(.1f);

            wallSlide.enabled = true;
        }
    }
}
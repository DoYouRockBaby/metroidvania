﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class Run : MonoBehaviour
    {
        public float speed = 12f;
        public float groundDamping = 20f; // how fast do we change direction? higher means faster
        public float inAirDamping = 5f;

        private Physic.Controller2D controller;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
        }

        private void Update()
        {
            //Detect the run orientation and rotate the entity
            int normalizedHorizontalSpeed;

            if (CrossPlatformInputManager.GetAxis("Horizontal") > 0.2f)
            {
                normalizedHorizontalSpeed = 1;

                if (!controller.facingRight)
                {
                    transform.Rotate(Vector3.up, 180f);
                    controller.facingRight = true;
                }
            }
            else if (CrossPlatformInputManager.GetAxis("Horizontal") < -0.2f)
            {
                normalizedHorizontalSpeed = -1;

                if (controller.facingRight)
                {
                    transform.Rotate(Vector3.up, 180f);
                    controller.facingRight = false;
                }
            }
            else
            {
                normalizedHorizontalSpeed = 0;
            }

            //Calculate run velocity
            var smoothedMovementFactor = controller.collisions.below ? groundDamping : inAirDamping; // how fast do we change direction?
            controller.velocity.x = Mathf.Lerp(controller.velocity.x, normalizedHorizontalSpeed * speed * Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal")), Time.deltaTime * smoothedMovementFactor);
        }
    }
}

﻿using Physic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D), typeof(Collider2D))]
    public class Float : MonoBehaviour
    {
        public float speed = 10f;
        public float damping = 1f;
        public LayerMask waterMask;
        public float buoyancy = 60f;

        [HideInInspector]
        public bool floating;

        private Physic.Controller2D controller;
        private new Collider2D collider;
        private Run run;
        private Jump jump;
        private Gravity gravity;
        private WallSlide wallSlide;
        private WallJump wallJump;
        private Dash dash;
        private Attack attack;
        private Animator animator;

        private Collider2D waterCollider;
        public bool wasInsideCollider = false;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            collider = GetComponent<Collider2D>();
            run = GetComponent<Run>();
            jump = GetComponent<Jump>();
            gravity = GetComponent<Gravity>();
            wallSlide = GetComponent<WallSlide>();
            wallJump = GetComponent<WallJump>();
            dash = GetComponent<Dash>();
            attack = GetComponent<Attack>();
            animator = GetComponent<Animator>();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (waterMask.value == 1 << collider.gameObject.layer)
            {
                floating = true;
                waterCollider = collider;
            }
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (waterMask.value == 1 << collider.gameObject.layer)
            {
                floating = false;
                waterCollider = null;
            }
        }

        private void Update()
        {
            if (run != null) run.enabled = !floating;
            if (run != null) run.enabled = !floating;
            if (gravity != null) gravity.enabled = !floating;
            if (wallSlide != null) wallSlide.enabled = !floating;
            if (wallJump != null) wallJump.enabled = !floating;
            if (dash != null) dash.enabled = !floating;
            if (attack != null) attack.enabled = !floating;
            if (animator != null) animator.enabled = !floating;

            if (floating)
            {
                //Reset air jump
                jump?.ResetAirJump();

                //Detect the run orientation and rotate the entity
                int normalizedHorizontalSpeed;

                if (CrossPlatformInputManager.GetAxis("Horizontal") > 0.2f)
                {
                    normalizedHorizontalSpeed = 1;

                    if (!controller.facingRight)
                    {
                        transform.Rotate(Vector3.up, 180f);
                        controller.facingRight = true;
                    }
                }
                else if (CrossPlatformInputManager.GetAxis("Horizontal") < -0.2f)
                {
                    normalizedHorizontalSpeed = -1;

                    if (controller.facingRight)
                    {
                        transform.Rotate(Vector3.up, 180f);
                        controller.facingRight = false;
                    }
                }
                else
                {
                    normalizedHorizontalSpeed = 0;
                }

                //Calculate run velocity
                controller.velocity.x = Mathf.Lerp(controller.velocity.x, normalizedHorizontalSpeed * speed * Mathf.Abs(CrossPlatformInputManager.GetAxis("Horizontal")), Time.deltaTime * damping);

                //Velocity Y
                if (waterCollider.OverlapPoint(collider.bounds.center))
                {
                    controller.velocity.y = controller.velocity.y + Time.deltaTime * buoyancy;
                    wasInsideCollider = wasInsideCollider || waterCollider.OverlapPoint(collider.bounds.center + Vector3.down * .1f);
                }
                else
                {
                    if(wasInsideCollider)
                    {
                        controller.velocity.y = 0f;
                    }

                    wasInsideCollider = false;
                }

                if (jump != null) jump.enabled = !wasInsideCollider;
            }

            if (animator != null)
            {
                animator.SetBool("Floating", floating);
            }
        }
    }
}

﻿using UnityEngine;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class WeaponDisplay : MonoBehaviour
    {
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;

        private Weapon weapon = null;
        private Weapon Weapon
        {
            get
            {
                if (weapon == null)
                {
                    weapon = Object.FindObjectOfType<Weapon>();
                }

                return weapon;
            }
        }

        private void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
        }

        private void Start()
        {
            if (Weapon != null)
            {
                meshFilter.mesh = Weapon.weapon.mesh;
                meshRenderer.material = Weapon.weapon.material;
            }
            else
            {
                meshFilter.mesh = null;
                meshRenderer.material = null;
            }

            Weapon.Changed += WeaponChanged;
        }

        private void OnDestroy()
        {
            Weapon.Changed -= WeaponChanged;
        }

        private void WeaponChanged(Weapon sender, Weapon.ChangedEventArgs e)
        {
            if (e.newWeapon != null)
            {
                meshFilter.mesh = e.newWeapon.mesh;
                meshRenderer.material = e.newWeapon.material;
            }
            else
            {
                meshFilter.mesh = null;
                meshRenderer.material = null;
            }
        }
    }
}
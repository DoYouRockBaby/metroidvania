﻿using Physic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class Dive : MonoBehaviour
    {
        public LayerMask waterMask;
        public float speed = 10f;
        public bool isDiving = false;
        public float damping = 1.5f;
        public float rotationSpeed = 5;

        private Physic.Controller2D controller;
        private new Collider2D collider;
        private Run run;
        private Jump jump;
        private Gravity gravity;
        private WallSlide wallSlide;
        private WallJump wallJump;
        private Dash dash;
        private Attack attack;
        private Animator animator;
        private Float floatt;

        private Collider2D waterCollider;
        private bool originalFacingRight = true;
        private Quaternion originalRotation;
        private float currentVelocity = 0f;
        private bool hasDived = false;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            collider = GetComponent<Collider2D>();
            run = GetComponent<Run>();
            jump = GetComponent<Jump>();
            gravity = GetComponent<Gravity>();
            wallSlide = GetComponent<WallSlide>();
            wallJump = GetComponent<WallJump>();
            dash = GetComponent<Dash>();
            attack = GetComponent<Attack>();
            animator = GetComponent<Animator>();
            floatt = GetComponent<Float>();
        }

        private void Update()
        {
            if (run != null) run.enabled = !isDiving;
            if (jump != null) jump.enabled = !isDiving;
            if (gravity != null) gravity.enabled = !isDiving;
            if (wallSlide != null) wallSlide.enabled = !isDiving;
            if (wallJump != null) wallJump.enabled = !isDiving;
            if (dash != null) dash.enabled = !isDiving;
            if (attack != null) attack.enabled = !isDiving;
            if (animator != null) animator.enabled = !isDiving;
            if (floatt != null) floatt.enabled = !isDiving;

            if (isDiving)
            {
                var newVelocity = new Vector2(
                    CrossPlatformInputManager.GetAxis("Horizontal"),
                    CrossPlatformInputManager.GetAxis("Vertical"));
                newVelocity = newVelocity.normalized * speed;

                controller.velocity.x = Mathf.Lerp(
                    controller.velocity.x,
                    newVelocity.x,
                    Time.deltaTime * damping);

                controller.velocity.y = Mathf.Lerp(
                    controller.velocity.y,
                    newVelocity.y,
                    Time.deltaTime * damping);

                /*var targetAngle = Vector2.Angle(Vector2.right, controller.velocity);
                transform.rotation = Quaternion.Euler(
                    Mathf.SmoothDampAngle(transform.rotation.eulerAngles.x, targetAngle, ref currentVelocity, rotationSpeed),
                    transform.rotation.eulerAngles.y,
                    transform.rotation.eulerAngles.z);*/

                //If aldready dived and touch the surface, we cancel the dive
                if (hasDived && 
                    (!waterCollider.OverlapPoint(collider.bounds.min) ||
                    !waterCollider.OverlapPoint(collider.bounds.max)))
                {
                    hasDived = false;
                    isDiving = false;
                }
            }
            else if(floatt != null && !hasDived && floatt.floating && CrossPlatformInputManager.GetAxis("Vertical") < -0.5)
            {
                isDiving = true;
            }

            if (animator != null)
            {
                animator.SetBool("Diving", isDiving);
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (waterMask.value == 1 << collider.gameObject.layer)
            {
                isDiving = true;

                /*var controller2D = collider.GetComponent<Physic.Controller2D>();
                if(controller2D != null)
                {
                    originalFacingRight = controller2D.facingRight;
                }
                originalRotation = collider.transform.localRotation;*/

                waterCollider = collider;
            }
        }

        private void OnTriggerStay2D(Collider2D collider)
        {
            if (waterMask.value == 1 << collider.gameObject.layer)
            {
                if (collider.OverlapPoint(this.collider.bounds.min) &&
                collider.OverlapPoint(this.collider.bounds.max))
                {
                    hasDived = true;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (waterMask.value == 1 << collider.gameObject.layer)
            {
                isDiving = false;
                hasDived = false;

                /*transform.localRotation = originalRotation;
                if (controller.facingRight != originalFacingRight)
                {
                    transform.Rotate(Vector3.up, 180f);
                }*/

                waterCollider = null;
            }
        }
    }
}

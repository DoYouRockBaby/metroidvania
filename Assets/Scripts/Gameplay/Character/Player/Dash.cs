﻿using Physic;
using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class Dash : MonoBehaviour
    {
        public new bool enabled = true;
        public float distance = 5f;
        public float speed = 30f;

        public event EventHandler Dashed;

        private Physic.Controller2D controller;
        private Run run;
        private Jump jump;
        private Gravity gravity;
        private WallSlide wallSlide;

        private bool canDash = false;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            run = GetComponent<Run>();
            jump = GetComponent<Jump>();
            gravity = GetComponent<Gravity>();
            wallSlide = GetComponent<WallSlide>();
        }

        private void Update()
        {
            if(wallSlide.wallSliding || controller.collisions.below)
            {
                canDash = true;
            }

            if (canDash && CrossPlatformInputManager.GetButtonDown("Dash"))
            {
                var direction = controller.facingRight ? 1 : -1;
                controller.velocity = new Vector2(direction * speed, 0f);

                if(Dashed != null)
                {
                    Dashed.Invoke(this, new EventArgs());
                }

                //Disable every abilities to prevent movement conflicts
                if (run != null) run.enabled = false;
                if (jump != null) jump.enabled = false;
                if (gravity != null) gravity.enabled = false;
                if (wallSlide != null) wallSlide.enabled = false;

                canDash = false;

                //Allow the enemy to move again after the attack
                StartCoroutine(RestoreStateAfterTime());
            }
        }

        IEnumerator RestoreStateAfterTime()
        {
            yield return new WaitForSeconds(distance / speed);

            controller.velocity.x = 0f;
            if (run != null) run.enabled = true;
            if (jump != null) jump.enabled = true;
            if (gravity != null) gravity.enabled = true;
            if (wallSlide != null) wallSlide.enabled = true;
        }
    }
}
﻿using Gameplay.Character.Player;
using Physic;
using UnityEngine;

namespace Assets.Scripts.Character.Player
{
    public class JumpTrigger : MonoBehaviour
    {
        public LayerMask mask;

        private Physic.Controller2D player = null;
        private Physic.Controller2D Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Physic.Controller2D>();
                }

                return player;
            }
        }

        private Jump playerJump = null;
        private Jump PlayerJump
        {
            get
            {
                if (playerJump == null)
                {
                    playerJump = GameObject.FindGameObjectWithTag("Player").GetComponent<Jump>();
                }

                return playerJump;
            }
        }

        private Gravity playerGravity = null;
        private Gravity PlayerGravity
        {
            get
            {
                if (playerGravity == null)
                {
                    playerGravity = GameObject.FindGameObjectWithTag("Player").GetComponent<Gravity>();
                }

                return playerGravity;
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (mask.value == 1 << collider.gameObject.layer)
            {
                Player.velocity.y = Mathf.Sqrt(2f * PlayerJump.height * -PlayerGravity.gravity);
            }
        }

        void OnColliderEnter2D(Collider2D collider)
        {
            OnTriggerEnter2D(collider);
        }

        private void OnCollisionEnter2D(UnityEngine.Collision2D collision)
        {
            OnTriggerEnter2D(collision.collider);
        }
    }
}

﻿using Gameplay.Items;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Character.Player
{
    public class Inventory : MonoBehaviour
    {
        public int coin = 45;
        public List<AbstractItem> items = new List<AbstractItem>();
    }
}
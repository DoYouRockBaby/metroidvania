﻿using Physic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class WallSlide : MonoBehaviour
    {
        public float minAngle = -20f;
        public float maxAngle = 20f;
        public float fallSpeed = -3;

        [HideInInspector]
        public bool wallSliding = false;

        private Physic.Controller2D controller;
        private Gravity gravity;
        private Run run;
        private Jump jump;
        private Animator animator;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            gravity = GetComponent<Gravity>();
            run = GetComponent<Run>();
            jump = GetComponent<Jump>();
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            wallSliding = !controller.collisions.below && (controller.collisions.right || controller.collisions.left)
                && minAngle <= controller.collisions.horizontalCollisionAngle
                && maxAngle >= controller.collisions.horizontalCollisionAngle;
            var wallDirection = (controller.collisions.right) ? -1 : 1;

            if (wallDirection * CrossPlatformInputManager.GetAxis("Horizontal") > 0)
            {
                wallSliding = false;
            }

            if(gravity != null) gravity.enabled = !wallSliding;
            if (run != null) run.enabled = !wallSliding;
            if (jump != null) jump.enabled = !wallSliding;

            if (wallSliding)
            {
                //Slide along the wall
                controller.velocity.y = fallSpeed;

                //Orientation of the character
                if (wallDirection > 0)
                {
                    if (controller.facingRight)
                    {
                        transform.Rotate(Vector3.up, 180f);
                        controller.facingRight = false;
                    }
                }
                else
                {
                    if (!controller.facingRight)
                    {
                        transform.Rotate(Vector3.up, 180f);
                        controller.facingRight = true;
                    }
                }
            }

            if (animator != null)
            {
                animator.SetBool("WallSliding", wallSliding);
            }
        }
    }
}
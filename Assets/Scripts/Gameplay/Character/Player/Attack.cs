﻿using Gameplay.Items;
using Physic;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Animator), typeof(Physic.Controller2D), typeof(Weapon))]
    public class Attack : MonoBehaviour
    {
        private Animator animator;
        private Physic.Controller2D controller;
        private Weapon weapon;
        private Run run;
        private Jump jump;
        private Gravity gravity;
        private WallSlide wallSlide;
        private Dash dash;

        private WeaponItem lastWeapon = null;
        private WeaponItem.AttackProperty lastAttack = null;
        private float lastAttackTime = float.MinValue;
        private float lastDammagesTime = float.MinValue;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            controller = GetComponent<Physic.Controller2D>();
            weapon = GetComponent<Weapon>();
            run = GetComponent<Run>();
            jump = GetComponent<Jump>();
            gravity = GetComponent<Gravity>();
            wallSlide = GetComponent<WallSlide>();
            dash = GetComponent<Dash>();
        }

        private void Update()
        {
            /*var playerDistance = Vector2.Distance(Player.transform.position, transform.position);
            if (playerDistance <= distance)
            {
                //Attack
                animator.SetTrigger("Attack");
                controller.velocity.x = 0f;

                //Disable other components
                run.enabled = false;
                enabled = false;

                //Allow the enemy to move again after the attack
                StartCoroutine(RestoreStateAfterTime());
            }
            */
            //Down attack
            if (CrossPlatformInputManager.GetButtonDown("Attack") && CrossPlatformInputManager.GetAxis("Vertical") <= -0.5f)
            {
                StartAttack(weapon.weapon.downAttack, true);
            }

            //Normal attack
            if (CrossPlatformInputManager.GetButtonDown("Attack") && CrossPlatformInputManager.GetAxis("Vertical") > -0.5f)
            {
                //First attack
                if (lastAttack == null || Time.time > (lastAttackTime + lastAttack.animationTime + weapon.weapon.timeBeforeNextAttackChain))
                {
                    StartAttack(weapon.weapon.attacks[0]);

                    controller.velocity.x = Mathf.Sign(controller.velocity.x) * (Mathf.Min(controller.velocity.x, weapon.weapon.attackMaxHorizontalSpeed));
                }
                //Chain attack
                else if (lastAttack != weapon.weapon.attacks[weapon.weapon.attacks.Length - 1] && Time.time <= (lastAttackTime + lastAttack.animationTime + weapon.weapon.timeToChainAttacks))
                {
                    StartAttack(NextAttack());
                }
            }
        }

        WeaponItem.AttackProperty NextAttack()
        {
            if (lastAttack == null)
            {
                return weapon.weapon.attacks[0];
            }

            for (var i = 0; i < weapon.weapon.attacks.Length - 1; i++)
            {
                if (weapon.weapon.attacks[i] == lastAttack)
                {
                    return weapon.weapon.attacks[i + 1];
                }
            }

            return weapon.weapon.attacks[0];
        }

        void StartAttack(WeaponItem.AttackProperty attack, bool downAttack = false)
        {
            lastAttack = attack;
            lastAttackTime = Time.time;

            animator.SetTrigger(attack.animationName);

            /*hitbox.offset = attack.hitBox.position;
            hitbox.size = attack.hitBox.size;
            if (hitbox.GetComponent<Weapon>() != null)
            {
                hitbox.GetComponent<Weapon>().isDownAttack = downAttack;
            }

            Invoke("AttackHit", attack.timeBeforeHit);
            Invoke("CancelAttack", attack.animationTime);*/

            this.enabled = false;
            if (run != null) run.enabled = false;
            if (jump != null) jump.enabled = false;
            if (gravity != null) gravity.enabled = false;
            if (wallSlide != null) wallSlide.enabled = false;
            if (dash != null) dash.enabled = false;

            StartCoroutine(RestoreStateAfterTime());
        }

        IEnumerator RestoreStateAfterTime()
        {
            yield return new WaitForSeconds(lastAttack.animationTime);

            this.enabled = true;
            if (run != null) run.enabled = true;
            if (jump != null) jump.enabled = true;
            if (gravity != null) gravity.enabled = true;
            if (wallSlide != null) wallSlide.enabled = true;
            if (dash != null) dash.enabled = true;
        }
    }
}
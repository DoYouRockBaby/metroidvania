﻿using System;
using UnityEngine;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Animator))]
    public class Health : MonoBehaviour
    {
        public int maxLife = 12;
        public int life = 8;
        public float hurtTime = 1f;
        public float dieTime = 1f;

        public event EventHandler Died;
        public static event EventHandler SomeoneDied;

        private Animator animator;

        public int Life
        {
            get
            {
                return life;
            }
            set
            {
                life = Math.Max(value, 0);

                if (animator != null)
                {
                    animator.SetTrigger("Hurt");
                }

                if (animator != null && life <= 0)
                {
                    animator.SetTrigger("Die");

                    foreach (var component in animator.GetComponents<MonoBehaviour>())
                    {
                        component.StopAllCoroutines();
                        component.enabled = false;
                    }

                    Died?.Invoke(this, new EventArgs());
                    SomeoneDied?.Invoke(this, new EventArgs());

                    Destroy(animator.gameObject, dieTime);
                }
            }
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }
    }
}

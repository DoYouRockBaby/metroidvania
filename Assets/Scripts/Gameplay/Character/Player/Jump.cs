﻿using Physic;
using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Gameplay.Character.Player
{
    [RequireComponent(typeof(Physic.Controller2D), typeof(Gravity))]
    public class Jump : MonoBehaviour
    {
        public float height = 6f;
        public uint maxAirJump = 1;

        public event EventHandler Jumped;

        private Physic.Controller2D controller;
        private Gravity gravity;
        private Animator animator;

        private uint airJumpCount = 0;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            gravity = GetComponent<Gravity>();
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            //Reset jump counter if were touching the ground
            if (controller.collisions.below)
            {
                ResetAirJump();
            }

            //Limit jump chain
            if (controller.collisions.below || airJumpCount < maxAirJump)
            {
                if (CrossPlatformInputManager.GetButtonDown("Jump"))
                {
                    controller.velocity.y = Mathf.Sqrt(2f * height * -gravity.gravity);

                    if (!controller.collisions.below)
                    {
                        airJumpCount++;
                    }

                    if (animator != null)
                    {
                        animator.SetTrigger("Jump");
                    }

                    InvokeJumpedEvent();
                }
            }
        }

        public void ResetAirJump()
        {
            airJumpCount = 0;
        }

        public void InvokeJumpedEvent()
        {
            if (Jumped != null)
            {
                Jumped.Invoke(this, new EventArgs());
            }
        }
    }
}

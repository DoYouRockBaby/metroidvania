﻿using System.Collections;
using UnityEngine;

namespace Gameplay.Character.Enemies
{
    [RequireComponent(typeof(Animator), typeof(Physic.Controller2D), typeof(FollowPlayer))]
    public class PhysicAttack : MonoBehaviour
    {
        public float distance = 4f;
        public float animationTime = 0.5f;
        public float timeBeforeNextAttack = 0.6f;

        private Animator animator;
        private Physic.Controller2D controller;
        private FollowPlayer followPlayer;

        private Transform player = null;
        private Transform Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").transform;
                }

                return player;
            }
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
            controller = GetComponent<Physic.Controller2D>();
            followPlayer = GetComponent<FollowPlayer>();
        }

        private void Update()
        {
            var playerDistance = Vector2.Distance(Player.transform.position, transform.position);
            if (playerDistance <= distance)
            {
                //Attack
                animator.SetTrigger("Attack");
                controller.velocity.x = 0f;

                //Disable other components
                followPlayer.enabled = false;
                enabled = false;

                //Allow the enemy to move again after the attack
                StartCoroutine(RestoreStateAfterTime());
            }
        }

        IEnumerator RestoreStateAfterTime()
        {
            yield return new WaitForSeconds(animationTime + timeBeforeNextAttack);

            followPlayer.enabled = true;
            enabled = true;
        }
    }
}
﻿using UnityEngine;

namespace Gameplay.Character.Enemies
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class WallBounce : MonoBehaviour
    {
        public float speed = 5.0f;
        public Vector2 initialDirection = new Vector2(1f, 1f);

        private Physic.Controller2D controller;

        private Vector2 currentDirection = Vector2.zero;

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
            currentDirection = initialDirection.normalized;
        }

        private void Update()
        {
            if (controller.collisions.below)
            {
                currentDirection.y = Mathf.Abs(currentDirection.y);
            }

            if (controller.collisions.above)
            {
                currentDirection.y = -Mathf.Abs(currentDirection.y);
            }

            if (controller.collisions.left)
            {
                currentDirection.x = Mathf.Abs(currentDirection.x);

                if (transform.localScale.x < 0f)
                {
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                }
            }

            if (controller.collisions.right)
            {
                currentDirection.x = -Mathf.Abs(currentDirection.x);

                if (transform.localScale.x > 0f)
                {
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                }
            }

            controller.velocity = currentDirection * speed;
        }
    }
}
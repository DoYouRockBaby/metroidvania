﻿using UnityEngine;

namespace Gameplay.Character.Enemies
{
    [RequireComponent(typeof(Physic.Controller2D))]
    public class FollowPlayer : MonoBehaviour
    {
        public float speed = 5f;
        public float visionDistance = 15f;
        public float groundDamping = 20f; // how fast do we change direction? higher means faster
        public float inAirDamping = 5f;

        private Physic.Controller2D controller;

        private Transform player = null;
        private Transform Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").transform;
                }

                return player;
            }
        }

        private void Awake()
        {
            controller = GetComponent<Physic.Controller2D>();
        }

        private void Update()
        {
            //Find and follow the player
            var distance = Vector2.Distance(Player.position, transform.position);
            var normalizedHorizontalSpeed = (distance > visionDistance) ? 0 : ((Player.position.x < transform.position.x) ? -1 : 1);

            if (normalizedHorizontalSpeed > 0)
            {
                if (!controller.facingRight)
                {
                    transform.Rotate(Vector3.up, 180f);
                    controller.facingRight = true;
                }
            }
            else if (normalizedHorizontalSpeed < -0)
            {
                if (controller.facingRight)
                {
                    transform.Rotate(Vector3.up, 180f);
                    controller.facingRight = false;
                }
            }

            //Calculate run velocity
            var smoothedMovementFactor = controller.collisions.below ? groundDamping : inAirDamping; // how fast do we change direction?
            controller.velocity.x = Mathf.Lerp(controller.velocity.x, normalizedHorizontalSpeed * speed, Time.deltaTime * smoothedMovementFactor);
        }
    }
}
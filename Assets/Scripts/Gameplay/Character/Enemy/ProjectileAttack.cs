﻿using System.Collections;
using UnityEngine;

namespace Gameplay.Character.Enemies
{
    [RequireComponent(typeof(Animator), typeof(Physic.Controller2D), typeof(FollowPlayer))]
    public class ProjectileAttack : MonoBehaviour
    {
        public float distance = 10f;
        public float animationTime = 0.5f;
        public float timeBeforeNextAttack = 0.6f;
        public float initialVelocity = 10f;
        public GameObject projectile;
        public GameObject spawn;

        private Animator animator;
        private Physic.Controller2D controller;
        private FollowPlayer followPlayer;

        private Transform player = null;
        private Transform Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").transform;
                }

                return player;
            }
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
            controller = GetComponent<Physic.Controller2D>();
            followPlayer = GetComponent<FollowPlayer>();
        }

        private void Update()
        {
            var playerDistance = Vector2.Distance(Player.transform.position, transform.position);
            if (playerDistance <= distance)
            {
                //Play animation
                animator.SetTrigger("Attack");
                controller.velocity.x = 0f;

                //Disable other components
                followPlayer.enabled = false;
                enabled = false;

                //Launch projectile
                StartCoroutine(InvokeProjectile());

                //Allow the enemy to move again after the attack
                StartCoroutine(RestoreStateAfterTime());
            }
        }

        IEnumerator InvokeProjectile()
        {
            yield return new WaitForSeconds(animationTime);

            var projectileGameObject = GameObject.Instantiate(projectile, null, true);
            var instantiatedProjectile = projectileGameObject.GetComponent<Physic.Controller2D>();
            instantiatedProjectile.transform.position = spawn.transform.position;
            instantiatedProjectile.velocity = (Player.transform.position - transform.position).normalized * initialVelocity;

            projectileGameObject.SetActive(true);
        }

        IEnumerator RestoreStateAfterTime()
        {
            yield return new WaitForSeconds(animationTime + timeBeforeNextAttack);

            followPlayer.enabled = true;
            enabled = true;
        }
    }
}
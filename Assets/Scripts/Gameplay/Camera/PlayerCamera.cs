﻿using UnityEngine;

namespace Gameplay.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class PlayerCamera : MonoBehaviour
    {
        public float defaultSize = 6.5f;
        public float smoothTime = 0.2f;
        public float maxSpeed = 100.0f;

        [HideInInspector]
        public Rect visibleZone = new Rect(0.0f, 0.0f, 0.0f, 0.0f);

        private new UnityEngine.Camera camera = null;
        private Vector3 currentVelocity = Vector3.zero;

        private Transform player = null;
        private Transform Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player").transform;
                }

                return player;
            }
        }

        private void Awake()
        {
            camera = GetComponent<UnityEngine.Camera>();
        }

        private void Start()
        {
            transform.position = CalculateTargetPosition();
        }

        private void Update()
        {
            transform.position = Vector3.SmoothDamp(
                transform.position,
                CalculateTargetPosition(),
                ref currentVelocity,
                smoothTime,
                maxSpeed);
        }

        Vector3 CalculateTargetPosition()
        {
            var oldPosition = new Vector2(transform.position.x, transform.position.y);

            //take player as center for new position
            var newPosition = new Vector2(Player.position.x, Player.position.y);

            //ajust new position to fit visible zone
            if (visibleZone.width * visibleZone.height > 0)
            {
                //Calculate the correct view size
                //TODO

                //Calculate viewport dimensions
                var camHalfWidth = camera.orthographicSize * camera.aspect;
                var camHalfHeight = camHalfWidth / camera.aspect;

                //Ajust the camera position
                if (newPosition.x - camHalfWidth < visibleZone.x)
                {
                    newPosition.x = visibleZone.x + camHalfWidth;
                }

                if (newPosition.x + camHalfWidth > visibleZone.x + visibleZone.width)
                {
                    newPosition.x = visibleZone.x + visibleZone.width - camHalfWidth;
                }

                if (newPosition.y - camHalfHeight < visibleZone.y)
                {
                    newPosition.y = visibleZone.y + camHalfHeight;
                }

                if (newPosition.y + camHalfHeight > visibleZone.y + visibleZone.height)
                {
                    newPosition.y = visibleZone.y + visibleZone.height - camHalfHeight;
                }
            }

            return new Vector3(newPosition.x, newPosition.y, transform.position.z);
        }
    }
}
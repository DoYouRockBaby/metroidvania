﻿using UnityEngine;

namespace Gameplay.Camera
{
    [RequireComponent(typeof(Collider2D))]
    public class CameraZone : MonoBehaviour
    {
        private new PlayerCamera camera = null;
        private PlayerCamera Camera
        {
            get
            {
                if (camera == null)
                {
                    camera = FindObjectOfType<PlayerCamera>();
                }

                return camera;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.tag == "Player")
            {
                //Camera.visibleZone = CalculateVisibleZone(ev.thisCollider);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                Camera.visibleZone = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
            }
        }

        protected Rect CalculateVisibleZone(Collider2D collider)
        {
            var position = new Vector2(collider.bounds.min.x, collider.bounds.min.y);
            var size = new Vector2(collider.bounds.size.x, collider.bounds.size.y);

            position = collider.transform.TransformPoint(position);
            size = collider.transform.TransformVector(size);

            return new Rect(position, size);
        }
    }
}
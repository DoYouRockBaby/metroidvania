﻿using UnityEngine;

namespace Gameplay.Miscelanous
{
    public class PlayerAttract : MonoBehaviour
    {
        public float time = 1f;
        public float distance = 1f;

        private float elapsedAttractionTime = 0f;

        private Collider2D playerCollider = null;
        private Collider2D PlayerCollider
        {
            get
            {
                if (playerCollider == null)
                {
                    var player = GameObject.FindGameObjectWithTag("Player");
                    playerCollider = player.GetComponent<Collider2D>();
                }

                return playerCollider;
            }
        }

        private void Update()
        {
            if (Vector2.Distance(PlayerCollider.bounds.center, new Vector2(transform.position.x, transform.position.y)) <= distance)
            {
                if (elapsedAttractionTime < time)
                {
                    elapsedAttractionTime += Time.deltaTime;

                    var targetPos = new Vector2(PlayerCollider.bounds.center.x, PlayerCollider.bounds.center.y);
                    var currentPos = new Vector2(transform.position.x, transform.position.y);
                    var newPos = Vector2.Lerp(currentPos, targetPos, elapsedAttractionTime / time);

                    transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(PlayerCollider.bounds.center.x, PlayerCollider.bounds.center.y, transform.position.z);
                }
            }
            else
            {
                elapsedAttractionTime = 0f;
            }
        }
    }
}

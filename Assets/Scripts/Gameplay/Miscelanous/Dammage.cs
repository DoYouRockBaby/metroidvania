﻿using Gameplay.Character.Player;
using UnityEngine;

namespace Gameplay.Miscelanous
{
    [RequireComponent(typeof(Collider2D))]
    public class Dammage : MonoBehaviour
    {
        public LayerMask mask;
        public int dammages = 2;
        public float force = 100f;

        private new Collider2D collider;

        private void Awake()
        {
            collider = GetComponent<Collider2D>();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (mask.value == 1 << collider.gameObject.layer)
            {
                var health = collider.GetComponent<Health>();
                if (health != null)
                {
                    var controller = collider.GetComponent<Physic.Controller2D>();
                    if (controller != null)
                    {
                        controller.velocity = (collider.bounds.center - this.collider.bounds.center).normalized * force;
                    }

                    health.Life = health.Life - dammages;
                }
            }
        }

        void OnColliderEnter2D(Collider2D collider)
        {
            OnTriggerEnter2D(collider);
        }

        private void OnCollisionEnter2D(UnityEngine.Collision2D collision)
        {
            OnTriggerEnter2D(collision.collider);
        }
    }

    public class BaseMonoBehaviour<T>
    {
    }
}
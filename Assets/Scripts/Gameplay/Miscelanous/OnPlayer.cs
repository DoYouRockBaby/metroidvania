﻿using UnityEngine;

namespace Assets.Scripts.Gameplay.Miscelanous
{
    public class OnPlayer : MonoBehaviour
    {
        private Collider2D playerCollider = null;
        private Collider2D PlayerCollider
        {
            get
            {
                if (playerCollider == null)
                {
                    var player = GameObject.FindGameObjectWithTag("Player");
                    playerCollider = player.GetComponent<Collider2D>();
                }

                return playerCollider;
            }
        }

        private void Update()
        {
            transform.position = new Vector3(PlayerCollider.bounds.center.x, PlayerCollider.bounds.center.y, transform.position.z);
        }
    }
}

﻿using UnityEngine;

namespace Gameplay.Miscelanous
{
    [RequireComponent(typeof(Collider2D))]
    public class SelfDestroy : MonoBehaviour
    {
        public LayerMask mask;

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (mask.value == 1 << collider.gameObject.layer)
            {
                Destroy(gameObject);
            }
        }

        void OnColliderEnter2D(Collider2D collider)
        {
            OnTriggerEnter2D(collider);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnTriggerEnter2D(collision.collider);
        }
    }
}
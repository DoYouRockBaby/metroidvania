﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gameplay.Environment
{
    public class Platform : MonoBehaviour
    {
        public LayerMask passengerMask;

        private new Collider2D collider;
        private Trajectory trajectory;

        private struct RaycastOrigins
        {
            public Vector2 topLeft, topRight;
            public Vector2 bottomLeft, bottomRight;
        }

        private int horizontalRayCount;
        private int verticalRayCount;
        private float horizontalRaySpacing;
        private float verticalRaySpacing;
        private RaycastOrigins raycastOrigins;
        private List<Collider2D> collisionColliders = new List<Collider2D>();

        private struct Passenger
        {
            public Transform transform;
            public Transform previousParent;
            public bool enteredByPlatformRaycast;
        }

        private List<Passenger> passengers = new List<Passenger>();

        private const float skinWidth = .015f;
        private const float dstBetweenRays = .25f;

        private void Awake()
        {
            collider = GetComponent<Collider2D>();
            trajectory = GetComponent<Trajectory>();
            trajectory.Moved += TrajectoryMoved;

            CalculateRaySpacing();
        }

        private void OnDestroy()
        {
            trajectory.Moved -= TrajectoryMoved;
        }

        private void TrajectoryMoved(Vector3 from, Vector3 to)
        {
            UpdateRaycastOrigins();

            var moveAmount = to - from;

            var previousCollisionColliders = collisionColliders;
            collisionColliders = new List<Collider2D>();

            /*if (moveAmount.x != 0)
            {
                HorizontalCollisions(moveAmount, Mathf.Sign(moveAmount.x));
            }

            if (moveAmount.y != 0)
            {
                VerticalCollisions(moveAmount, Mathf.Sign(moveAmount.y));
            }*/

            if (moveAmount.y > 0)
            {
                VerticalCollisions(moveAmount, Mathf.Sign(moveAmount.y));
            }

            NotifyCollisions(previousCollisionColliders);
        }

        protected void CalculateRaySpacing()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);

            float boundsWidth = bounds.size.x;
            float boundsHeight = bounds.size.y;

            horizontalRayCount = Mathf.RoundToInt(boundsHeight / dstBetweenRays);
            verticalRayCount = Mathf.RoundToInt(boundsWidth / dstBetweenRays);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }

        protected void UpdateRaycastOrigins()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);

            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        void HorizontalCollisions(Vector2 moveAmount, float directionX)
        {
            float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);

                if (hit)
                {
                    collisionColliders.Add(hit.collider);
                }
            }
        }

        void VerticalCollisions(Vector2 moveAmount, float directionY)
        {
            float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

            for (int i = 0; i < verticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

                if (hit)
                {
                    collisionColliders.Add(hit.collider);
                }
            }
        }

        private void OnColliderEnter2D(Collider2D collider)
        {
            AddPassenger(collider.transform, false);
        }

        private void OnColliderExit2D(Collider2D collider)
        {
            RemovePassenger(collider.transform, false);
        }

        private void AddPassenger(Transform passenger, bool isPlatformRaycasted)
        {
            if (passengerMask.value == 1 << passenger.gameObject.layer && passengers.Where(e => e.transform == passenger).Count() == 0)
            {
                if (transform.parent != transform)
                {
                    passengers.Add(new Passenger()
                    {
                        transform = transform,
                        previousParent = transform.parent,
                        enteredByPlatformRaycast = isPlatformRaycasted
                    });

                    transform.parent = transform;
                }
            }
        }

        private void RemovePassenger(Transform passenger, bool isPlatformRaycast)
        {
            if (passengerMask.value == 1 << collider.gameObject.layer && passengers.Where(e => e.transform == passenger).Count() > 0)
            {
                var passengerInfos = passengers.Where(e => e.transform == passenger).First();
                if(passengerInfos.enteredByPlatformRaycast == isPlatformRaycast)
                {
                    collider.transform.parent = passengerInfos.previousParent;
                    passengers.RemoveAll(e => e.transform == passenger);
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnColliderEnter2D(collision.collider);
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            OnColliderExit2D(collision.collider);
        }

        void NotifyCollisions(List<Collider2D> previousCollisionColliders)
        {
            collisionColliders = collisionColliders.Distinct().ToList();

            foreach (var collider in collisionColliders)
            {
                if (!previousCollisionColliders.Contains(collider))
                {
                    AddPassenger(collider.transform, true);
                    OnColliderEnter2D(collider);
                }
            }

            foreach (var collider in previousCollisionColliders)
            {
                if (!collisionColliders.Contains(collider))
                {
                    RemovePassenger(collider.transform, true);
                }
            }
        }
    }
}
﻿using Assets.Scripts.Utility;
using System;
using UnityEngine;

namespace Gameplay.Environment
{
    public class Trajectory : MonoBehaviour
    {
        [Serializable]
        public class Node : MathUtility.InterpolationPoint
        {
            public Vector3 rotation;
            public Vector3 scale = new Vector3(1.0f, 1.0f, 1.0f);
            public float time = 1.0f;
            public string animation;
        }

        public new bool enabled = true;
        public bool loop = true;
        public float speedScale = 1.0f;
        public Node[] nodes = new Node[0];

        public delegate void MovedHandler(Vector3 from, Vector3 to);
        public event MovedHandler Moved;

        private Matrix4x4 localToWorld;
        private int currentStep;
        private float elapsedStepTime;

        private void Start()
        {
                localToWorld = transform.localToWorldMatrix;
        }

        private void Update()
        {
            if (nodes.Length == 0 || !enabled)
            {
                return;
            }

            //Update current step
            elapsedStepTime += Time.deltaTime * speedScale;
            if (elapsedStepTime > nodes[currentStep].time)
            {
                elapsedStepTime -= nodes[currentStep].time;
                currentStep++;

                if (loop)
                {
                    if (currentStep >= nodes.Length)
                    {
                        currentStep = 0;
                    }
                }
                else
                {
                    if (currentStep >= nodes.Length - 1)
                    {
                        enabled = false;
                        return;
                    }
                }

                //Play animation
                /*if (nodes[_step].animation != "")
                {
                    _animator.Play(Animator.StringToHash(nodes[_step].animation));
                }*/
            }

            //Find nodes
            var originNode = nodes[currentStep];
            var targetNode = nodes[(currentStep + 1) % nodes.Length];

            //Interpolate position
            var transformedStart = originNode.Transform(localToWorld);
            var transformedEnd = targetNode.Transform(localToWorld);

            var originalPosition = transform.position;
            transform.position = MathUtility.Interpolate(transformedStart, transformedEnd, elapsedStepTime / originNode.time);

            if(originalPosition != transform.position)
            {
                Moved?.Invoke(originalPosition, transform.position);
            }

            //Interpolate rotation and scale
            var eulerRotation = Vector3.Lerp(originNode.rotation, targetNode.rotation, elapsedStepTime / originNode.time) + localToWorld.rotation.eulerAngles;
            transform.localRotation = Quaternion.Euler(eulerRotation.x, eulerRotation.y, eulerRotation.z);
            transform.localScale = localToWorld.MultiplyVector(Vector3.Lerp(originNode.scale, targetNode.scale, elapsedStepTime / originNode.time));
        }
    }
}

﻿using UnityEngine;

namespace Gameplay.Environment
{
    [RequireComponent(typeof(Animator))]
    public class PlayerTrigger : MonoBehaviour
    {
        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if(collider.tag == "Player")
            {
                animator.SetBool("PlayerTrigger", true);
            }
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            if (collider.tag == "Player")
            {
                animator.SetBool("PlayerTrigger", false);
            }
        }
    }
}

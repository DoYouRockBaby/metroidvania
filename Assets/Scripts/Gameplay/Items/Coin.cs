﻿using Gameplay.Character.Player;
using UnityEngine;

namespace Gameplay.Items
{
    public class Coin : MonoBehaviour
    {
        public int amount;

        private Inventory inventory = null;
        private Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = Object.FindObjectOfType<Inventory>();
                }

                return inventory;
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag == "Player")
            {
                Inventory.coin += amount;
                Object.Destroy(gameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            
        }
    }
}
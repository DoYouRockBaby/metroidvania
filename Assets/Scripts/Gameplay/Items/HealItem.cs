﻿using Gameplay.Character.Player;
using System;
using UnityEngine;

namespace Gameplay.Items
{
    [CreateAssetMenu(fileName = "item.asset", menuName = "Item/Heal Item")]
    public class HealItem : AbstractItem
    {
        public int heal = 4;

        public override void Use()
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
            {
                var character = player.GetComponent<Health>();
                var inventory = player.GetComponent<Inventory>();

                character.life = Math.Min(character.life + heal, character.maxLife);
                inventory.items[inventory.items.IndexOf(this)] = null;
            }
        }
    }
}
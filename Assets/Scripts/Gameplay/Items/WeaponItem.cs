﻿using Gameplay.Character.Player;
using System;
using UnityEngine;

namespace Gameplay.Items
{
    [CreateAssetMenu(fileName = "item.asset", menuName = "Item/Weapon")]
    public class WeaponItem : AbstractItem
    {
        [Serializable]
        public class AttackProperty
        {
            public float timeBeforeHit = 0.1f;
            public float animationTime = 0.5f;
            public string animationName = "";
            public Rect hitBox;
        }

        public float attackMaxHorizontalSpeed = 3f;
        public float timeToChainAttacks = 0.1f;
        public float timeBeforeNextAttackChain = 1f;
        public AttackProperty[] attacks = new AttackProperty[0];
        public AttackProperty downAttack = null;

        public override void Use()
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
            {
                var inventory = player.GetComponent<Inventory>();
                var weapon = player.GetComponent<Character.Player.Weapon>();

                var old = weapon.weapon;
                weapon.weapon = this;
                weapon.InvokeChangedEvent(old, this);

                inventory.items[inventory.items.IndexOf(this)] = null;
            }
        }
    }
}
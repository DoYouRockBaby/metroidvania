﻿using Gameplay.Character.Player;
using System;
using UnityEngine;

namespace Gameplay.Items
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Item : MonoBehaviour
    {
        public AbstractItem item;

        public event EventHandler Picked;
        public static event EventHandler SomethingPicked;

        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;

        private Inventory inventory = null;
        private Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = UnityEngine.Object.FindObjectOfType<Inventory>();
                }

                return inventory;
            }
        }

        private void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
        }

        private void Update()
        {
            if (item != null)
            {
                meshFilter.mesh = item.mesh;
                meshRenderer.material = item.material;
            }
            else
            {
                meshFilter.mesh = null;
                meshRenderer.material = null;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Picked?.Invoke(this, new EventArgs());
            SomethingPicked?.Invoke(this, new EventArgs());
            Inventory.items.Add(item);

            UnityEngine.Object.Destroy(gameObject);
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            
        }
    }
}
﻿using UnityEngine;

namespace Gameplay.Items
{
    public abstract class AbstractItem : ScriptableObject
    {
        public string itemName;
        public Sprite image;
        public Mesh mesh;
        public Material material;

        abstract public void Use();
    }
}